import numpy

class patch:
    def __init__(self, X=[0, 0, 0, 0], Y=[0, 0, 0, 0], Z=[0, 0, 0, 0]) -> None:
        self.X = X
        self.Y = Y
        self.Z = Z

    def getPoint(self, u, v):
        N = self.__N(u, v)

        x = sum([self.X[i] * N[i] for i in range(4)])
        y = sum([self.Y[i] * N[i] for i in range(4)])
        z = sum([self.Z[i] * N[i] for i in range(4)])

        return x, y, z

    def getDirU(self, u, v):
        dNdU = self.__dNdU(u, v)
        dNdV = self.__dNdV(u, v)

        dXdU = sum([self.X[i] * dNdU[i] for i in range(4)])
        dYdU = sum([self.Y[i] * dNdU[i] for i in range(4)])
        dZdU = sum([self.Z[i] * dNdU[i] for i in range(4)])

        dU = numpy.sqrt(dXdU ** 2 + dYdU ** 2)

        return dXdU/dU, dYdU/dU, dZdU/dU

    def getDirV(self, u, v):
        dNdU = self.__dNdU(u, v)
        dNdV = self.__dNdV(u, v)

        dXdV = sum([self.X[i] * dNdV[i] for i in range(4)])
        dYdV = sum([self.Y[i] * dNdV[i] for i in range(4)])
        dZdV = sum([self.Z[i] * dNdV[i] for i in range(4)])

        dV = numpy.sqrt(dXdV ** 2 + dYdV ** 2)

        return dXdV/dV, dYdV/dV, dZdV/dV

    #
    def getTesellationSquare(self, nU):
        u = numpy.linspace(0.1, 1.0, nU) ** 2
        v = numpy.linspace(0.1, 1.0, nU) ** 2

        u -= u[0]
        u /= u[nU - 1]
        u = 2 * u - 1
        
        v -= v[0]
        v /= v[nU - 1]
        v = 2 * v - 1

        pnt = []
        for uL in u:
            for vL in v:
                xL, yL, _ = self.getPoint(uL, vL)
                pnt.append([xL, yL])
        
        return pnt

    def getTesellationLinear(self, nU):
        u = numpy.linspace(-1, 1, nU)
        v = numpy.linspace(-1, 1, nU)

        pnt = []
        for uL in u:
            for vL in v:
                xL, yL, _ = self.getPoint(uL, vL)
                pnt.append([xL, yL])

        # Add a diagonal of points
        for i in range(1, nU):
            if((i + 1) % 2 == 0):
                xA, yA, _ = self.getPoint(u[i],   -1)
                xB, yB, _ = self.getPoint(-1,   u[i])
                pnt.append([(xA + xB)/2, (yA + yB)/2])

        return pnt

    def getTesellationDiagnl(self, nU):
        u = numpy.linspace(-1.0, 1.0, nU)
        
        xC, yC = self.getCorners()[:2]
        xUp = numpy.linspace(xC[0], xC[1], nU)
        yUp = numpy.linspace(yC[0], yC[1], nU)
        xDn = numpy.linspace(xC[0], xC[3], nU)
        yDn = numpy.linspace(yC[0], yC[3], nU)

        dL = numpy.sqrt((xUp[1] - xUp[0]) ** 2 + (yUp[1] - yUp[0]) ** 2)

        pnt = [[xC[0], yC[0]]]
        for i in range(1, nU):
            dY = yUp[i] - yDn[i]
            nV = max([2, int(numpy.round(dY/dL)) + 1])
            xV = numpy.linspace(xUp[i], xDn[i], nV)
            yV = numpy.linspace(yUp[i], yDn[i], nV)
            for j in range(nV):
                pnt.append([xV[j], yV[j]])
        
        return pnt

    def projectPoint(self, x0, y0):
        u = 0.0
        v = 0.0

        dV = 1.0
        nT = 0
        while(dV > 1e-6 and nT < 100):
            nT += 1

            N = self.__N(u, v)
            dNdU = self.__dNdU(u, v)
            dNdV = self.__dNdV(u, v)
            
            R = numpy.zeros(2)
            J = numpy.zeros([2, 2])

            R[0] = sum([N[i] * self.X[i] for i in range(4)]) - x0
            R[1] = sum([N[i] * self.Y[i] for i in range(4)]) - y0

            J[0][0] = sum([dNdU[i] * self.X[i] for i in range(4)])
            J[0][1] = sum([dNdV[i] * self.X[i] for i in range(4)])
            J[1][0] = sum([dNdU[i] * self.Y[i] for i in range(4)])
            J[1][1] = sum([dNdV[i] * self.Y[i] for i in range(4)])

            dU = numpy.linalg.solve(J, R)

            u -= dU[0]
            v -= dU[1]

            dV = numpy.linalg.norm(R)

        return u, v

    #
    def __N(self, u, v):
        N = [0.0 for i in range(4)]
        N[0] = 0.25 * (1 - u) * (1 - v)
        N[1] = 0.25 * (1 + u) * (1 - v)
        N[2] = 0.25 * (1 + u) * (1 + v)
        N[3] = 0.25 * (1 - u) * (1 + v)

        return N

    def __dNdU(self, u, v):
        dNdXi = [0.0 for i in range(4)]
        dNdXi[0] = -0.25 * (1 - v)
        dNdXi[1] =  0.25 * (1 - v)
        dNdXi[2] =  0.25 * (1 + v)
        dNdXi[3] = -0.25 * (1 + v)

        return dNdXi

    def __dNdV(self, u, v):
        dNdEta = [0.0 for i in range(4)]
        dNdEta[0] = -0.25 * (1 - u)
        dNdEta[1] = -0.25 * (1 + u)
        dNdEta[2] =  0.25 * (1 + u)
        dNdEta[3] =  0.25 * (1 - u)

        return dNdEta
    
    #
    def getCorners(self):
        return self.X, self.Y, self.Z
    
    def getDX(self):
        xCrn = self.getCorners()[0]
        return max(xCrn) - min(xCrn)
        
    def getDY(self):
        yCrn = self.getCorners()[1]
        return max(yCrn) - min(yCrn)
        
    def getDZ(self):
        zCrn = self.getCorners()[2]
        return max(zCrn) - min(zCrn)