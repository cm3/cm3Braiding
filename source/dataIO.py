import os
import pickle
import csv

def exportDictionaryToCSV(fileName, dctList, delimiter=';'):
    fFile = open(fileName + ".csv", "w")
    keyList = []
    strOut = ''
    for key in dctList[0]:
        strOut += str(key) + delimiter
        keyList.append(key)
    fFile.write(strOut[:-1] + "\n")

    strOut = ''
    for dct in dctList:
        for key in keyList:
            strOut += str(dct[key]) + delimiter
    fFile.write(strOut[:-1] + "\n")

def importCSVToVariables(fileName, delimiter=';'):
    variables = []
    with open(fileName) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=delimiter)
        line_count = 0
        for row in csv_reader:
            if line_count > 0:
                for val in row:
                    variables.append(float(val))
            line_count += 1
    return tuple(variables)

def exportFile(name, obj):
    try:
        pickle.dump(obj, open(name, 'wb'))
    except:
        print("Error while opening file %s" % name, flush=True)

def importFile(name):
    obj = None
    try:
        obj = pickle.load(open(name, 'rb'))
    except:
        print("Error while opening file %s" % name, flush=True)
    
    return obj
