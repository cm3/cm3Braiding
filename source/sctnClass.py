import numpy
import tool
# import matplotlib.pyplot as pyplot

class sctn:
    def __init__(self, dW, dH, dE, nP):
        self.dW = dW
        self.dH = dH
        self.dE = dE

        if(nP % 2 == 0):
            self.nP = nP + 1
        else:
            self.nP = nP

        self.pDn = [[], []]
        self.pMn = [[], []]
        self.pUp = [[], []]

        self.dTheta = numpy.pi/(self.nP - 1)

        self.genDnSkin()
        self.genMnSkin()
        self.genUpSkin()

    def getZCoord(self, xi, xiMin=-1, xiMax=1.0):
        dH = 1e-6
        if(xi < xiMin):
            xi1 = xiMin
            xi2 = xiMin + dH
            z1  = 0.5 * self.dH * numpy.cos(numpy.pi * xi1 / 2) ** self.dE
            z2  = 0.5 * self.dH * numpy.cos(numpy.pi * xi2 / 2) ** self.dE
            z = tool.extrap1([xi1, xi2], [z1, z2], [xi])[0]
        elif(xi > xiMax):
            xi1 = xiMax - dH
            xi2 = xiMax
            z1  = 0.5 * self.dH * numpy.cos(numpy.pi * xi1 / 2) ** self.dE
            z2  = 0.5 * self.dH * numpy.cos(numpy.pi * xi2 / 2) ** self.dE
            z = tool.extrap1([xi1, xi2], [z1, z2], [xi])[0]
        else:
            z = 0.5 * self.dH * numpy.cos(numpy.pi * xi / 2) ** self.dE


        return z

    def getDeriv(self, xi):
        return - 0.5 * self.dE * self.dH * numpy.pi * numpy.sin(numpy.pi * xi) * numpy.cos(numpy.pi * xi)**(self.dE - 1)

    def genDnSkin(self):
        for i in range(self.nP):
            xLcl = 0.5 * numpy.cos(numpy.pi + self.dTheta * float(i))
            yLcl = 0.5 * numpy.cos(numpy.pi * xLcl) ** self.dE
    
            self.pDn[0].append( xLcl * self.dW)
            self.pDn[1].append(-yLcl * self.dH)

    def genMnSkin(self):
        for i in range(self.nP):
            xLcl = 0.5 * numpy.cos(numpy.pi + self.dTheta * float(i))
    
            self.pMn[0].append(xLcl * self.dW)
            self.pMn[1].append(0.0)

    def genUpSkin(self):
        for i in range(self.nP):
            xLcl = 0.5 * numpy.cos(numpy.pi + self.dTheta * float(i))
            yLcl = 0.5 * numpy.cos(numpy.pi * xLcl) ** self.dE
    
            self.pUp[0].append(xLcl * self.dW)
            self.pUp[1].append(yLcl * self.dH)

    #
    def getNrmUp(self, u):
        xLcl = u - 0.5
        yLcl = 0.5 * abs(numpy.cos(numpy.pi * xLcl)) ** self.dE

        return yLcl * self.dH

    def getNrmDn(self, u):
        xLcl = u - 0.5
        yLcl = 0.5 * abs(numpy.cos(numpy.pi * xLcl)) ** self.dE

        return - yLcl * self.dH

    def getArea(self):
        xMn = self.pMn[0]
        zUp = self.pUp[1]
        zDn = self.pDn[1]

        dX = [0 for _ in range(self.nP - 1)]
        zM = [0 for _ in range(self.nP - 1)]
        for i in range(self.nP - 1):
            dX[i] = xMn[i+1] - xMn[i]
            zM[i] =(zUp[i+1] + zUp[i])/2

        area = 0
        for i in range(self.nP - 1):
            area += 2 * zM[i] * dX[i]

        return area

    def plot(self):
        pyplot.figure()
        pyplot.plot(self.pDn[0], self.pDn[1], 'b.-', label='Lower Skin')
        pyplot.plot(self.pMn[0], self.pMn[1], 'r.-', label='Mean Skin')
        pyplot.plot(self.pUp[0], self.pUp[1], 'g.-', label='Upper Skin')
        pyplot.legend()
        pyplot.show()
