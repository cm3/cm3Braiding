import sctnClass as sctn
import surfClass as surf
import surfOffClass as srfO
import bezrClass as bezr
import stripeClass as strp
import bzrPairClass as bzpr

import bilnClass as biln
import dataIO
import tool

import matplotlib.pyplot as plt

from shapely import Polygon
import numpy
import gmsh
import time
import sys

class braid:
    def __init__(self, wAxial, hAxial, sAxial, lAxial, wBias, hBias, dExp, vGap, theta, nPA, nPB, iIdx):
        self.theta = theta * numpy.pi / 180

        self.wAxial = wAxial
        self.hAxial = hAxial
        self.lAxial = lAxial
        self.sAxial = sAxial

        self.wBias = wBias
        self.hBias = hBias
        self.sBias = 2 * self.sAxial * numpy.tan(numpy.pi/2 - self.theta)

        self.dExp = dExp

        if(nPA % 2 == 0):
            nPA += 1
        if(nPB % 2 == 0):
            nPB += 1

        self.nPA = nPA
        self.nPB = nPB

        self.vGap = vGap

        self.sctnAxial = sctn.sctn(wAxial, hAxial, dExp, nPA)
        self.sctnBias  = sctn.sctn(wBias,  hBias,  dExp, nPB)

        self.surfAxial = []
        self.surfBiasR = []
        self.surfBiasL = []

        self.volAxial = None
        self.volBiasR = None
        self.volBiasL = None

        self.xCtrSurf = []
        self.yCtrSurf = []
        self.zCtrSurf = []

        self.xCtrCurve = []
        self.yCtrCurve = []
        self.zCtrCurve = []

        self.xMn = []
        self.yMn = []
        self.zMn = []

        self.towVolume = []

        self.iIdx = iIdx

    #
    def pntsSctnAxial(self, nP):
        self.sctnAxial = sctn.sctn(self.wAxial, self.hAxial, self.dExp, nP)

    def pntsSctnBias(self, nP):
        self.sctnBias = sctn.sctn(self.wBias, self.hBias, self.dExp, nP)

    #
    def generate(self, sType, nPair=1, bSurfOut=False, bGenVol=True):
        self.nPairs = nPair

        gmsh.option.setNumber('Geometry.ToleranceBoolean', 1e-8)

        if(sType.casefold() == str("diamond").casefold()):
            print("Model Info: Pattern Diamond", flush=True)
            sepBias = self.sBias
            offBias = 0.0

            #
            polyAxial, polyBiasR, polyBiasL, polyBox = self.generateProjectedPolygons(offBias, sepBias)
            self.exportSVG(polyAxial, polyBiasR, polyBiasL, polyBox, path="./svg/", nameMod=str(self.iIdx))
            self.coverFactor = self.getCoverFactor(polyAxial, polyBiasR, polyBiasL, polyBox)

            if(bGenVol):
                # Export input data
                dictExport = {}
                dictExport["wAxial"] = self.wAxial
                dictExport["hAxial"] = self.hAxial
                dictExport["sAxial"] = self.sAxial
                dictExport["lAxial"] = self.lAxial
                dictExport["vGap"]   = self.vGap
                dictExport["wBias"]  = self.wBias
                dictExport["hBias"]  = self.hBias
                dictExport["nSctn"]  = self.dExp
                dictExport["theta"]  = self.theta*180/numpy.pi
                dictExport["cFct"]   = self.coverFactor
                dataIO.exportDictionaryToCSV("csv/model" + self.iIdx, [dictExport])

                #
                if(self.coverFactor < 1 - 1e-10):
                    bOK = self.genGeneral(sepBias, offBias, bSurfOut=bSurfOut)
                else:
                    print("Error: Geometry generation not possible", flush=True)
                    print('%%%% EXIT OK %%%%')
                    sys.exit(0)
            else:
                bOK = True

        elif(sType.casefold() == str("regular").casefold()):
            print("Model Info: Pattern Regular", flush=True)
            sepBias = self.sBias / 2
            offBias = -self.sBias / 4

            #
            polyAxial, polyBiasR, polyBiasL, polyBox = self.generateProjectedPolygons(offBias, sepBias)
            self.exportSVG(polyAxial, polyBiasR, polyBiasL, polyBox, path="./svg/", nameMod=str(self.iIdx))
            self.coverFactor = self.getCoverFactor(polyAxial, polyBiasR, polyBiasL, polyBox)

            if(bGenVol):
                # Export input data
                dictExport = {}
                dictExport["wAxial"] = self.wAxial
                dictExport["hAxial"] = self.hAxial
                dictExport["sAxial"] = self.sAxial
                dictExport["lAxial"] = self.lAxial
                dictExport["vGap"]   = self.vGap
                dictExport["wBias"]  = self.wBias
                dictExport["hBias"]  = self.hBias
                dictExport["nSctn"]  = self.dExp
                dictExport["theta"]  = self.theta*180/numpy.pi
                dictExport["cFct"]   = self.coverFactor
                dataIO.exportDictionaryToCSV("csv/model" + self.iIdx, [dictExport])

                #
                if(self.coverFactor < 1 - 1e-10):
                    bOK = self.genGeneral(sepBias, offBias, bSurfOut=bSurfOut)
                else:
                    print("Error: Geometry generation not possible", flush=True)
                    print('%%%% EXIT OK %%%%')
                    sys.exit(0)
            else:
                bOK = True

        else:
            print("braid::generate: Pattern not recognized", flush=True)
            raise("braid::generate: Pattern not recognized")

        return bOK

    def expCoverFactor(self, sType, nPair=1):
        self.nPairs = nPair

        if(sType.casefold() == str("diamond").casefold()):
            print("Model Info: Pattern Diamond", flush=True)
            sepBias = self.sBias
            offBias = 0.0

            #
            polyAxial, polyBiasR, polyBiasL, polyBox = self.generateProjectedPolygons(offBias, sepBias)
            self.exportSVG(polyAxial, polyBiasR, polyBiasL, polyBox, path="./svg/", nameMod=str(self.iIdx))
            cf = self.getCoverFactor(polyAxial, polyBiasR, polyBiasL, polyBox)


        elif(sType.casefold() == str("regular").casefold()):
            print("Model Info: Pattern Regular", flush=True)
            sepBias = self.sBias / 2
            offBias = -self.sBias / 4

            #
            polyAxial, polyBiasR, polyBiasL, polyBox = self.generateProjectedPolygons(offBias, sepBias)
            self.exportSVG(polyAxial, polyBiasR, polyBiasL, polyBox, path="./svg/", nameMod=str(self.iIdx))
            cf = self.getCoverFactor(polyAxial, polyBiasR, polyBiasL, polyBox)

        else:
            cf = numpy.nan

        return cf

    # Generate: General function
    def genGeneral(self, sepBias, offBias, bSurfOut=False):
        timeStart = time.time()

        self.actualSepBias = sepBias
        self.actualOffBias = offBias

        # Generate initial central surfaces for the axial tow, centered at (0.0, 0.0, 0.0)
        surfAxial = surf.surf(self.nPA, 2)
        for i in range(self.nPA):
            for j in range(2):
                surfAxial.X[i][j] = self.sctnAxial.pMn[0][i]
                surfAxial.Y[i][j] = self.lAxial*(-0.5 + float(j))

        # Generate the initial offset surfaces
        xCrnO, yCrnO = self.genOffsetCorners()

        srfOffUp = srfO.surfOff()
        srfOffUp.setFn(fnV=self.sctnAxial.getZCoord, fnU=self.sctnBias.getZCoord)
        srfOffUp.setCorners(xCrnO, yCrnO)
        srfOffUp.vGap = self.vGap

        srfOffDn = srfO.surfOff()
        srfOffDn.setFn(fnV=self.sctnAxial.getZCoord, fnU=self.sctnBias.getZCoord, dir=-1)
        srfOffDn.setCorners(xCrnO, yCrnO)
        srfOffDn.move(dX=self.sAxial, dY=self.sAxial / numpy.tan(self.theta))
        srfOffDn.vGap = self.vGap

        srfOffUp.setTrim(uMin=0.025, uMax=0.975)
        srfOffDn.setTrim(uMin=0.025, uMax=0.975)

        #
        lAxial = self.genBzrStrpAxial()

        #
        surfRMd, surfLMd, surfOffUpR, surfOffDnR = self.genBezierBias(srfOffUp, srfOffDn, offBias, bVerb=True, bSurfOut=bSurfOut, nIterMax=250)

        surfOffUpL = surfOffUpR.getLeftFromRight(dY=-offBias)
        surfOffDnL = surfOffDnR.getLeftFromRight(dY=-offBias)

        surfOffUpR.resetTanCurve()
        surfOffDnR.resetTanCurve()
        surfOffUpL.resetTanCurve()
        surfOffDnL.resetTanCurve()

        if(bSurfOut):
            surfOffUpR.export2VTK("UpRefR")
            surfOffUpL.export2VTK("UpRefL")
            surfOffDnR.export2VTK("DnRefR")
            surfOffDnL.export2VTK("DnRefL")

        sBzrR = self.replBezierList(surfOffUpR, surfRMd, surfOffDnR, dX=self.sAxial, dY=  self.sAxial * numpy.tan(numpy.pi/2 - self.theta))
        sBzrL = self.replBezierList(surfOffUpL, surfLMd, surfOffDnL, dX=self.sAxial, dY= -self.sAxial * numpy.tan(numpy.pi/2 - self.theta))

        #
        lBzrR = [strp.stripe(sBzrR)]
        lBzrL = [strp.stripe(sBzrL)]

        for i in range(self.nPairs + 1):
            dDir = 1
            for j in range(2):
                lBzrT = strp.stripe(sBzrR)
                lBzrT.move(dY=dDir * sepBias * (i + 1))
                lBzrR.append(lBzrT)
                dDir *= -1

        for i in range(self.nPairs + 1):
            dDir = 1
            for j in range(2):
                lBzrT = strp.stripe(sBzrL)
                lBzrT.move(dY=dDir * sepBias * (i + 1))
                lBzrL.append(lBzrT)
                dDir *= -1

        #
        if(bSurfOut):
            iIdx = 0
            for lBzr in lBzrR:
                for bzr in lBzr.lSurf:
                    iIdx += 1
                    bzr.export2VTK("R%03i" % (iIdx))

            iIdx = 0
            for lBzr in lBzrL:
                for bzr in lBzr.lSurf:
                    iIdx += 1
                    bzr.export2VTK("L%03i" % (iIdx))

            iIdx = 0
            for lBzr in lAxial:
                for bzr in lBzr.lSurf:
                    iIdx += 1
                    bzr.export2VTK("A%03i" % (iIdx))

        # Bezier stripes save
        self.strAxial = lAxial
        self.strBiasR = lBzrR
        self.strBiasL = lBzrL

        #
        print("Model Info: Generate time:", time.time() - timeStart, flush=True)

    def addToGMSH(self, nP0):
        #
        sepBias = self.actualSepBias
        offBias = self.actualOffBias

        # Box dimensions
        x0 = -self.sAxial
        y0 = -offBias - sepBias/2
        z0 = -(self.hAxial + 2*self.hBias + 3.0 * self.vGap)/2.0

        dx = 2 * self.sAxial
        dy = sepBias
        dz = self.hAxial + 2 * self.hBias + 3.0 * self.vGap

        #
        print("Model Info: Generate Tow Volumes: Axial", flush=True)
        towVolumeAxial = []
        idx = 0
        for lBzr in self.strAxial:
            idx += 1
            print("Model Info:     Tow n.", idx, flush=True)
            towVol = self.getVolGMSH(lBzr, 2, self.sctnAxial)
            towVolumeAxial += towVol

        #
        print("Model Info: Generate Tow Volumes: Right Bias", flush=True)
        towVolumeBiasR = []
        idx = 0
        for lBzr in self.strBiasR:
            idx += 1
            print("Model Info:     Tow n.", idx, flush=True)
            towVol = self.getVolGMSH(lBzr, nP0, self.sctnBias)
            towVolumeBiasR += towVol

        print("Model Info: Generate Tow Volumes: Left Bias", flush=True)
        towVolumeBiasL = []
        idx = 0
        for lBzr in self.strBiasL:
            idx += 1
            print("Model Info:     Tow n.", idx, flush=True)
            towVol = self.getVolGMSH(lBzr, nP0, self.sctnBias)
            towVolumeBiasL += towVol

        # Save box dimensions
        print("Model Info: Generate Matrix Volume", flush=True)
        self.box = {"xMin": x0,
                    "xMax": x0 + dx,
                    "yMin": y0,
                    "yMax": y0 + dy,
                    "zMin": z0,
                    "zMax": z0 + dz}

        boxVolume = gmsh.model.occ.addBox(x0, y0, z0, dx, dy, dz)
        boxVolume = [[(3, boxVolume)], [x0, x0 + dx, x0 + dx, x0], [y0, y0, y0 + dy, y0 + dy]]
        gmsh.model.occ.synchronize()

        #
        print("Model Info: Boolean Operations", flush=True)
        self.cutVol, self.towType = self.getFragmentedVolume(towVolumeAxial, towVolumeBiasR, towVolumeBiasL, boxVolume)

    #
    def meshSize(self, sizeMin, sizeMax):
        sizeLength = max([min([sizeMax,  self.dXMin / 3]), sizeMin])
        return sizeLength

    def getSurfacesForRefinement(self):
        volRefine = []
        srfRfnAxl = []
        srfRfnSpr = []

        for i in range(len(self.towType)):
            if(self.towType[i] != 1):
                vol = self.cutVol[i]
                volBndBox = gmsh.model.occ.getBoundingBox(3, vol)
                voldX = volBndBox[3] - volBndBox[0]
                voldY = volBndBox[4] - volBndBox[1]
                for strAxial in self.strAxial:
                    xCrnAxial = strAxial.getCorners()[0]

                    if(abs(min(xCrnAxial) - volBndBox[0]) < voldX * 1e-3 or abs(max(xCrnAxial) - volBndBox[3]) < voldX * 1e-3):
                        volRefine.append(vol)
                        surfs = gmsh.model.getAdjacencies(3, vol)[1]
                        surfZ = []
                        surfI = []
                        for surf in surfs:
                            srfBndBox = gmsh.model.getBoundingBox(2, surf)
                            srfdX = srfBndBox[3] - srfBndBox[0]
                            srfdY = srfBndBox[4] - srfBndBox[1]
                            srfMZ =(srfBndBox[2] + srfBndBox[5])/2
                            if(srfdX > voldX * 1e-3 and srfdY > voldY * 1e-3):
                                surfZ.append(abs(srfMZ))
                                surfI.append(surf)
                        iMin = numpy.argmin(surfZ)
                        srfRfnAxl.append(surfI[iMin])
                    elif(abs(max(xCrnAxial) - volBndBox[0]) < voldX * 1e-3 or abs(min(xCrnAxial) - volBndBox[3]) < voldX * 1e-3):
                        volRefine.append(vol)
                        surfs = gmsh.model.getAdjacencies(3, vol)[1]
                        surfZ = []
                        surfI = []
                        for surf in surfs:
                            srfBndBox = gmsh.model.getBoundingBox(2, surf)
                            srfdX = srfBndBox[3] - srfBndBox[0]
                            srfdY = srfBndBox[4] - srfBndBox[1]
                            srfMZ =(srfBndBox[2] + srfBndBox[5])/2
                            if(srfdX > voldX * 1e-3 and srfdY > voldY * 1e-3):
                                surfZ.append(abs(srfMZ))
                                surfI.append(surf)
                        iMax = numpy.argmax(surfZ)
                        srfRfnSpr.append(surfI[iMax])

        return srfRfnAxl, srfRfnSpr

    def getPointsForRefinement(self):
        idx = [[0, 3], [1, 2]]

        pInt = []
        for strR in self.strBiasR:
            xR, yR = strR.getCorners()[:2]
            for strL in self.strBiasL:
                xL, yL = strL.getCorners()[:2]

                for i0, j0 in idx:
                    for i1, j1 in idx:
                        xLcl, yLcl = tool.getLineIntersection(xR[i0], xR[j0], xL[i1], xL[j1], yR[i0], yR[j0], yL[i1], yL[j1])

                        bInsideAxial = False
                        for strA in self.strAxial:
                            if(strA.isInside(xLcl, yLcl)):
                                bInsideAxial = True
                                break

                        if(not bInsideAxial):
                            if(xLcl >= min(xR) and xLcl <= max(xR)):
                                #if(xLcl >= self.box["xMin"] and xLcl <= self.box["xMax"]):
                                #    if(yLcl >= self.box["yMin"] and yLcl <= self.box["yMax"]):
                                        uR, vR = strR.getLocalCoords(xLcl, yLcl)
                                        uL, vL = strL.getLocalCoords(xLcl, yLcl)

                                        uR = max([uR, -1.0 + 1e-3])
                                        uR = min([uR,  1.0 - 1e-3])
                                        vR = max([vR, -1.0 + 1e-3])
                                        vR = min([vR,  1.0 - 1e-3])
                                        uL = max([uL, -1.0 + 1e-3])
                                        uL = min([uL,  1.0 - 1e-3])
                                        vL = max([vL, -1.0 + 1e-3])
                                        vL = min([vL,  1.0 - 1e-3])

                                        xLclR, yLclR = strR.getXY(uR, vR)
                                        xLclL, yLclL = strL.getXY(uL, vL)

                                        xM = (xLclR + xLclL) / 2.0
                                        yM = (yLclR + yLclL) / 2.0

                                        zM = strR.getZ(xM, yM) - strL.getZ(xM, yM)
                                        if(abs(zM) < self.vGap * 1.01):
                                            pInt.append([xM, yM, zM])

        return pInt

    #
    def getVolGMSH(self, lBzr, nP0, sctn):
        #
        nS    = sctn.nP
        nB    = len(lBzr)
        xSct0 = sctn.pMn[0]
        ySctA = sctn.pDn[1]
        ySctB = sctn.pUp[1]
        uSct0 = [xSct0[i] - xSct0[0]  for i in range(nS)]
        uSct0 = [uSct0[i]/uSct0[nS-1] for i in range(nS)]

        #
        u = xSct0.copy()
        u /= u[-1] * (1 + 1e-10)

        #
        xCtrSurf = []
        yCtrSurf = []
        zCtrSurf = []

        xCrnS, yCrnS = lBzr.getCorners()[:2]

        if(nB > 1):
            #
            N0 = biln.biln().N(0, -1)
            N1 = biln.biln().N(0,  1)
            xM = [numpy.dot(N0, xCrnS), numpy.dot(N1, xCrnS)]
            yM = [numpy.dot(N0, yCrnS), numpy.dot(N1, yCrnS)]

            dL = numpy.sqrt((xM[0] - xM[1]) ** 2 + (yM[0] - yM[1]) ** 2)

            xI = [0 for _ in range(nB+1)]
            yI = [0 for _ in range(nB+1)]
            nI = 0
            for i in range(nB):
                nI += 1
                xCrnB, yCrnB = lBzr.lSurf[i].getCorners()[:2]
                xI[i+1], yI[i+1] = tool.getLineIntersection(xCrnB[2], xCrnB[3], xM[0], xM[1], yCrnB[2], yCrnB[3], yM[0], yM[1])

            vI = [0 for _ in range(nB+1)]
            for i in range(1, nB+1):
                vI[i] = 2 * numpy.sqrt((xM[0] - xI[i]) ** 2 + (yM[0] - yI[i]) ** 2) / dL - 1
            vI[0] = -1

            for i in range(1, nB):
                if(i % 2 == 0):
                    vI[i] += 1e-6
                else:
                    vI[i] -= 1e-6

            nOff = 6
            nBzr = 4

            fBlndOff  = 1 - 1/(numpy.exp(numpy.linspace(-3, 3, nOff)) + 1)
            fBlndOff -= fBlndOff[ 0]
            fBlndOff /= fBlndOff[-1]

            fBlndBzr  = numpy.linspace(0.0, 1.0, nBzr)

            vT = [-1]
            for i in range(nB):
                if(isinstance(lBzr.lSurf[i], bezr.bezr)):
                    for j in range(1, nBzr):
                        vT.append((1 - fBlndBzr[j]) * vI[i] + fBlndBzr[j] * vI[i + 1])
                else:
                    for j in range(1, nOff):
                        vT.append((1 - fBlndOff[j]) * vI[i] + fBlndOff[j] * vI[i + 1])
            
            nV = []
            for i in range(1, len(vT)):
                dV = vT[i] - vT[i-1]
                nP2 = min([max([int(nP0 * dV), 20]), nP0])
                nV.append(nP0)

            vI = vT.copy()
            nB = len(vI) - 1

        else:
            vI   = [-1, 1]
            nV   = [2]

        self.dXMin = max(xCrnS) - min(xCrnS)
        xCrnP = []
        yCrnP = []
        for i in range(nB):
            xCtrSurf.append([])
            yCtrSurf.append([])
            zCtrSurf.append([])

            N0 = biln.biln().N(0.0, vI[i])
            N1 = biln.biln().N(0.0, vI[i+1])
            xCrnL = []
            xCrnL.append(numpy.dot(N0, xCrnS))
            xCrnL.append(numpy.dot(N1, xCrnS))

            self.dXMin = min([self.dXMin, max(xCrnL) - min(xCrnL)])
            for j in range(nS):
                xCtrSurf[i].append([])
                yCtrSurf[i].append([])
                zCtrSurf[i].append([])

                for k in range(nV[i]):
                    uLcl = u[j]
                    vLcl = numpy.linspace(vI[i], vI[i+1], nV[i])[k]

                    N = biln.biln().N(uLcl, vLcl)
                    xLcl = numpy.dot(N, xCrnS)
                    yLcl = numpy.dot(N, yCrnS)
                    zLcl = lBzr.getZ(xLcl, yLcl)
                    xCtrSurf[i][j].append(xLcl )
                    yCtrSurf[i][j].append(yLcl)
                    zCtrSurf[i][j].append(zLcl)

            #
            NA = biln.biln().N(-1.0, -1.0)
            NB = biln.biln().N( 1.0, -1.0)
            NC = biln.biln().N( 1.0,  1.0)
            ND = biln.biln().N(-1.0,  1.0)
            xCrnP.append([])
            yCrnP.append([])
            xCrnP[-1].append(numpy.dot(NA, xCrnS))
            xCrnP[-1].append(numpy.dot(NB, xCrnS))
            xCrnP[-1].append(numpy.dot(NC, xCrnS))
            xCrnP[-1].append(numpy.dot(ND, xCrnS))
            yCrnP[-1].append(numpy.dot(NA, yCrnS))
            yCrnP[-1].append(numpy.dot(NB, yCrnS))
            yCrnP[-1].append(numpy.dot(NC, yCrnS))
            yCrnP[-1].append(numpy.dot(ND, yCrnS))

        for i in range(nB - 1):
            for j in range(nS):
                xM = (xCtrSurf[i][j][-1] + xCtrSurf[i+1][j][0])/2
                yM = (yCtrSurf[i][j][-1] + yCtrSurf[i+1][j][0])/2
                zM = (zCtrSurf[i][j][-1] + zCtrSurf[i+1][j][0])/2

                xCtrSurf[i]  [j][-1] = xM
                xCtrSurf[i+1][j][0]  = xM
                yCtrSurf[i]  [j][-1] = yM
                yCtrSurf[i+1][j][0]  = yM
                zCtrSurf[i]  [j][-1] = zM
                zCtrSurf[i+1][j][0]  = zM

        # Extreme curve loops
        crvLoopExt = [] 
        for i in range(nB):
            gPntA = []
            gPntB = []
            for j in range(nS):
                xLcl = xCtrSurf[i][j][0]
                yLcl = yCtrSurf[i][j][0]
                zLcl = zCtrSurf[i][j][0]
                gPntA.append(gmsh.model.occ.addPoint(xLcl, yLcl, zLcl + ySctA[j]))
                gPntB.append(gmsh.model.occ.addPoint(xLcl, yLcl, zLcl + ySctB[j]))
            gPntB.reverse()
            gSplA = gmsh.model.occ.addBSpline(gPntA)
            gSplB = gmsh.model.occ.addBSpline(gPntB)
            crvLoopExt.append(gmsh.model.occ.addCurveLoop([gSplA, gSplB]))
            gmsh.model.occ.synchronize()
        
        gPntA = []
        gPntB = []
        for j in range(nS):
            xLcl = xCtrSurf[-1][j][-1]
            yLcl = yCtrSurf[-1][j][-1]
            zLcl = zCtrSurf[-1][j][-1]
            gPntA.append(gmsh.model.occ.addPoint(xLcl, yLcl, zLcl + ySctA[j]))
            gPntB.append(gmsh.model.occ.addPoint(xLcl, yLcl, zLcl + ySctB[j]))
        gPntB.reverse()
        gSplA = gmsh.model.occ.addBSpline(gPntA)
        gSplB = gmsh.model.occ.addBSpline(gPntB)
        crvLoopExt.append(gmsh.model.occ.addCurveLoop([gSplA, gSplB]))
        gmsh.model.occ.synchronize()
        
        #
        towVolume = []
        for i in range(nB):
            gTow = []

            for k in range(nV[i]):
                if(k == 0):
                    gTow.append(crvLoopExt[i])
                elif(k == nV[i]-1):
                    gTow.append(crvLoopExt[i + 1])
                else:
                    gPntA = []
                    gPntB = []

                    for j in range(nS):
                        xLcl = xCtrSurf[i][j][k]
                        yLcl = yCtrSurf[i][j][k]
                        zLcl = zCtrSurf[i][j][k]

                        gPntA.append(gmsh.model.occ.addPoint(xLcl, yLcl, zLcl + ySctA[j]))
                        gPntB.append(gmsh.model.occ.addPoint(xLcl, yLcl, zLcl + ySctB[j]))
                    gPntB.reverse()
                    gSplA = gmsh.model.occ.addBSpline(gPntA)
                    gSplB = gmsh.model.occ.addBSpline(gPntB)
                    gTow.append(gmsh.model.occ.addCurveLoop([gSplA, gSplB]))
            gmsh.model.occ.synchronize()

            towVolume.append([gmsh.model.occ.addThruSections(gTow)[0], xCrnP[i], yCrnP[i]])
            gmsh.model.occ.synchronize()

        return towVolume

    def setVolPhysicalGroups(self):
        #-------------------------------------------
        vols = gmsh.model.occ.getEntities(3)
        nVol = len(vols)
        self.cutVol = []
        for i in range(nVol):
            self.cutVol.append(vols[i][1])
        self.cutVol.sort()

        #-------------------------------------------
        self.towAxial = []
        self.towBiasR = []
        self.towBiasL = []
        self.matrixVl = []
        for i in range(len(self.cutVol)):
            if(i < len(self.towType)):
                if(self.towType[i] == 1):
                    self.towAxial.append(self.cutVol[i])
                elif(self.towType[i] == 2):
                    self.towBiasR.append(self.cutVol[i])
                elif(self.towType[i] == 3):
                    self.towBiasL.append(self.cutVol[i])
            else:
                self.matrixVl.append(self.cutVol[i])

        gmsh.model.addPhysicalGroup(3, self.towAxial, 1001)
        gmsh.model.setPhysicalName (3, 1001, "Axial Tow volume")

        gmsh.model.addPhysicalGroup(3, self.towBiasR, 1002)
        gmsh.model.setPhysicalName (3, 1002, "Bias Right Tow volume")

        gmsh.model.addPhysicalGroup(3, self.towBiasL, 1003)
        gmsh.model.setPhysicalName (3, 1003, "Bias Left Tow volume")

        gmsh.model.addPhysicalGroup(3, self.matrixVl, 1004)
        gmsh.model.setPhysicalName (3, 1004, "Matrix volume")

        gmsh.model.occ.synchronize()

    def getFragmentedVolume(self, towVolumeAxial, towVolumeBiasR, towVolumeBiasL, boxVolume):
        towVolume = []
        towType   = []
        print("Model Info:    Volumes Intersection", flush=True)
        for towVolLcl, _, _ in towVolumeAxial:
            iIdx, _ = gmsh.model.occ.intersect([towVolLcl], boxVolume[0], removeObject=True, removeTool=False)
            if(len(iIdx) > 0):
                towVolume.append(iIdx[0])
                towType.append(1)
        gmsh.model.occ.synchronize()

        for towVolLcl, _, _ in towVolumeBiasR:
            iIdx, _ = gmsh.model.occ.intersect([towVolLcl], boxVolume[0], removeObject=True, removeTool=False)
            if(len(iIdx) > 0):
                towVolume.append(iIdx[0])
                towType.append(2)
        gmsh.model.occ.synchronize()

        for towVolLcl, _, _ in towVolumeBiasL:
            iIdx, _ = gmsh.model.occ.intersect([towVolLcl], boxVolume[0], removeObject=True, removeTool=False)
            if(len(iIdx) > 0):
                towVolume.append(iIdx[0])
                towType.append(3)
        gmsh.model.occ.synchronize()

        print("Model Info:    Volumes Fragment", flush=True)
        cutVol, _ = gmsh.model.occ.fragment(boxVolume[0], towVolume)
        gmsh.model.occ.synchronize()

        cutVol = [x[1] for x in cutVol]

        return cutVol, towType

    def getTowType(self):
        cutVol = gmsh.model.occ.getEntities(3)

        bndBoxVol = []
        for j in range(len(cutVol)):
            tag = cutVol[j][1]
            xMin, yMin, zMin, xMax, yMax, zMax = gmsh.model.occ.getBoundingBox(3, tag)
            bndBoxVol.append((xMax - xMin) * (yMax - yMin) * (zMax - zMin))
        bndBoxMax = numpy.argmax(bndBoxVol)

        self.towType = [0 for _ in range(len(cutVol) - 1)]
        for j in range(len(cutVol) - 1):
            tag = cutVol[j][1]
            xMin, yMin, zMin, xMax, yMax, zMax = gmsh.model.occ.getBoundingBox(3, tag)
            xVol = (xMin + xMax)/2
            yVol = (yMin + yMax)/2
            zVol = (zMin + zMax)/2
            zVol = zVol if(abs(zVol) > 1e-6) else 0

            dZ = [1e6 for _ in range(3)]
            for i in range(len(self.strAxial)):
                if(self.strAxial[i].isInside(xVol, yVol)):
                    dZ[0] = numpy.abs(zVol - self.strAxial[i].getZ(xVol, yVol))
                    break

            for i in range(len(self.strBiasR)):
                if(self.strBiasR[i].isInside(xVol, yVol)):
                    dZ[1] = numpy.abs(zVol - self.strBiasR[i].getZ(xVol, yVol))
                    break

            for i in range(len(self.strBiasL)):
                if(self.strBiasL[i].isInside(xVol, yVol)):
                    dZ[2] = numpy.abs(zVol - self.strBiasL[i].getZ(xVol, yVol))
                    break

            iMin = numpy.argmin(dZ)

            self.towType[j] = iMin + 1

        print(self.towType)


    def setSrfPhysicalGroups(self):
        eps = 1e-3

        xMin = self.box["xMin"]
        xMax = self.box["xMax"]
        yMin = self.box["yMin"]
        yMax = self.box["yMax"]
        zMin = self.box["zMin"]
        zMax = self.box["zMax"]

        #
        allSurfs = gmsh.model.getEntities(dim = 2)
        srfDict = {'100':[], '101':[], '110':[], '111':[], '120':[], '121':[], '300':[]}
        for surf in allSurfs:
            bndBox = gmsh.model.getBoundingBox(surf[0], surf[1])
            xM = (bndBox[0] + bndBox[3])/2
            yM = (bndBox[1] + bndBox[4])/2
            zM = (bndBox[2] + bndBox[5])/2
            dX =  bndBox[3] - bndBox[0]
            dY =  bndBox[4] - bndBox[1]
            dZ =  bndBox[5] - bndBox[2]

            if(dX < 1e-3):
                if(abs(xM - xMin) < eps):
                    srfDict["100"].append(surf[1])
                elif(abs(xM - xMax) < eps):
                    srfDict["101"].append(surf[1])
            elif(dY < 1e-3):
                if(abs(yM - yMin) < eps):
                    srfDict["110"].append(surf[1])
                elif(abs(yM - yMax) < eps):
                    srfDict["111"].append(surf[1])
            elif(dZ < 1e-3):
                if(abs(zM - zMin) < eps):
                    srfDict["120"].append(surf[1])
                elif(abs(zM - zMax) < eps):
                    srfDict["121"].append(surf[1])

        #
        for key in srfDict:
            gmsh.model.addPhysicalGroup(2, srfDict[key], int(key))

        #
        gmsh.model.setPhysicalName(2, 100, "Boundary surface X=-L/2")
        gmsh.model.setPhysicalName(2, 101, "Boundary surface X= L/2")
        gmsh.model.setPhysicalName(2, 110, "Boundary surface Y=-L/2")
        gmsh.model.setPhysicalName(2, 111, "Boundary surface Y= L/2")
        gmsh.model.setPhysicalName(2, 120, "Boundary surface Z=-T/2")
        gmsh.model.setPhysicalName(2, 121, "Boundary surface Z= T/2")
        gmsh.model.setPhysicalName(2, 300, "Other surfaces")
        gmsh.model.occ.synchronize()

    #
    def correctPatch(self, xCrn0, yCrn0):
        dX0 = xCrn0[3] - xCrn0[0]
        dY0 = yCrn0[3] - yCrn0[0]
        dL0 = numpy.sqrt(dX0 ** 2 + dY0 ** 2)
        dX0 /= dL0
        dY0 /= dL0

        dXr = -dY0
        dYr =  dX0

        #
        xM = [0 for _ in range(4)]
        xM[0] = (xCrn0[0] + xCrn0[1]) / 2.0
        xM[1] = (xCrn0[0] + xCrn0[1]) / 2.0
        xM[2] = (xCrn0[2] + xCrn0[3]) / 2.0
        xM[3] = (xCrn0[2] + xCrn0[3]) / 2.0

        yM = [0 for _ in range(4)]
        yM[0] = (yCrn0[0] + yCrn0[1]) / 2.0
        yM[1] = (yCrn0[0] + yCrn0[1]) / 2.0
        yM[2] = (yCrn0[2] + yCrn0[3]) / 2.0
        yM[3] = (yCrn0[2] + yCrn0[3]) / 2.0

        xCrn = [0 for _ in range(4)]
        yCrn = [0 for _ in range(4)]
        for i in range(4):
            A = numpy.zeros([2, 2])
            b = numpy.zeros(2)

            A[0][0] =  dX0
            A[0][1] = -dXr
            A[1][0] =  dY0
            A[1][1] = -dYr

            b[0] = xM[i] - xCrn0[i]
            b[1] = yM[i] - yCrn0[i]

            dL = numpy.linalg.solve(A, b)

            xCrn[i] = xCrn0[i] + dX0 * dL[0]
            yCrn[i] = yCrn0[i] + dY0 * dL[0]

        xRdc = [0 for _ in range(4)]
        yRdc = [0 for _ in range(4)]

        tol = 1e-10
        xi  = [tol - 1, 1 - tol, 1 - tol, tol - 1]
        eta = [tol - 1, tol - 1, 1 - tol, 1 - tol]

        for i in range(4):
            N = biln.biln().N(xi[i], eta[i])
            xRdc[i] = numpy.dot(N, xCrn)
            yRdc[i] = numpy.dot(N, yCrn)

        return xRdc, yRdc

    # Generate:
    def genOffsetSurf(self):
        #
        xCtrSurf = []
        yCtrSurf = []
        zCtrSurf = []

        if(self.nPairs % 2 == 0):
            zDir = 1
        else:
            zDir = -1

        x0  = - self.nPairs * self.sAxial
        for k in range(2 * self.nPairs + 1):
            #
            xCtrSurf.append([])
            yCtrSurf.append([])
            zCtrSurf.append([])

            for i in range(self.sctnBias.nP):
                xCtrSurf[k].append([])
                yCtrSurf[k].append([])
                zCtrSurf[k].append([])

                for j in range(self.sctnAxial.nP):
                    xCtrSurf[k][i].append(self.sctnAxial.pMn[0][j] + x0 + k * self.sAxial)
                    yCtrSurf[k][i].append(self.sctnBias.pMn[0][i] / numpy.sin(self.theta) + xCtrSurf[k][i][j]*numpy.tan(numpy.pi/2 - self.theta))
                    zCtrSurf[k][i].append(zDir * (self.sctnAxial.pUp[1][j] + self.sctnBias.pUp[1][i] + self.vGap))
            zDir *= -1

        return xCtrSurf, yCtrSurf, zCtrSurf

    def genOffsetCorners(self):
        #
        xA = [-self.wAxial/2, self.wAxial/2, self.wAxial/2, -self.wAxial/2]
        yA = [-self.lAxial,  -self.lAxial,   self.lAxial,    self.lAxial]

        xB = [-self.wBias/2, self.wBias/2, self.wBias/2, -self.wBias/2]
        yB = [-self.lAxial, -self.lAxial,  self.lAxial,   self.lAxial]

        for i in range(4):
            xR = xB[i] * numpy.cos(-self.theta) - yB[i] * numpy.sin(-self.theta)
            yR = xB[i] * numpy.sin(-self.theta) + yB[i] * numpy.cos(-self.theta)
            xB[i] = xR
            yB[i] = yR

        #
        polyA = Polygon([[xA[i], yA[i]] for i in range(4)])
        polyB = Polygon([[xB[i], yB[i]] for i in range(4)])

        polyC = polyA.intersection(polyB)
        xC = list(polyC.exterior.xy[0])
        yC = list(polyC.exterior.xy[1])

        xC = [xC[i] for i in range(0, 4)]
        yC = [yC[i] for i in range(0, 4)]
        xC.reverse()
        yC.reverse()

        def rotate(lst):
            lst.append(lst[0])
            lst = [lst[i] for i in range(1, len(lst))]
            return lst

        xC = rotate(xC)
        yC = rotate(yC)

        return xC, yC

    # Central Patch
    def genBezierCentral(self, srfOffUp, srfOffDn, nVOut, nU, dOff=0.5):
        surfOut = bezr.bezr()

        dA = min([max([0.5, dOff]), 0.95])

        xUp, yUp = srfOffUp.getCorners()
        xDn, yDn = srfOffDn.getCorners()

        xLnUp = numpy.linspace(xUp[3], xUp[2], nU)
        yLnUp = numpy.linspace(yUp[3], yUp[2], nU)

        xLnDn = numpy.linspace(xDn[0], xDn[1], nU)
        yLnDn = numpy.linspace(yDn[0], yDn[1], nU)

        xCtrl = []
        yCtrl = []
        zCtrl = []
        dB = (1 - dA)

        fDst0 = numpy.linspace(1 - dA - dB, 1 - dA + dB, nVOut)
        fDst1 = numpy.linspace(    dA - dB,     dA + dB, nVOut)

        fDst0[0]       = 0
        fDst0[nVOut-1] = 1.0
        fDst1[0]       = 0
        fDst1[nVOut-1] = 1.0

        dZ = srfOffUp.getCoord(0.5, 0.5)[2] - srfOffUp.getCoord(1.0, 1.0)[2]

        vL = numpy.linspace(0, 1, nU)
        zLnUp = [srfOffUp.getCoord(0.0, v)[2] + dZ / 2 for v in vL]
        zLnDn = [srfOffDn.getCoord(0.0, v)[2] - dZ / 2 for v in vL]

        #
        for u in range(nU):
            xA = xLnUp[u]
            xB = xLnDn[u]
            yA = yLnUp[u]
            yB = yLnDn[u]
            zA = zLnUp[u]
            zB = zLnDn[u]

            fDist = ((nU - 1 - u) * fDst0 + u * fDst1) / (nU - 1)
            xCtrlList = xA + (xB - xA) * fDist
            yCtrlList = yA + (yB - yA) * fDist
            zCtrlList = zA + (zB - zA) * fDist

            xCtrl.append(list(xCtrlList))
            yCtrl.append(list(yCtrlList))
            zCtrl.append(list(zCtrlList))

        surfOut.setControlPoints3D(xCtrl, yCtrl, zCtrl)

        return surfOut

    def genBezierConnect(self, surfA, surfB, dDst):
        nU = surfA.nU

        xCtrl = [[] for u in range(nU)]
        yCtrl = [[] for u in range(nU)]
        zCtrl = [[] for u in range(nU)]

        dDstA = 1 - dDst
        dDstC = dDst

        for u in range(nU):
            xCtrA, yCtrA, zCtrA = surfA.getCtrlPt(u=u)
            xCtrC, yCtrC, zCtrC = surfB.getCtrlPt(u=u)

            xA, yA, zA = surfA.getPoint2D(dDstA, xCtrA, yCtrA, zCtrA)
            xC, yC, zC = surfB.getPoint2D(dDstC, xCtrC, yCtrC, zCtrC)

            xCtrl[u] = [xA, xC]
            yCtrl[u] = [yA, yC]
            zCtrl[u] = [zA, zC]

        surfOut = bezr.bezr()
        surfOut.setControlPoints3D(xCtrl, yCtrl, zCtrl)

        return surfOut

    #
    def genBzrStrpAxial(self):
        bAxial0 = bezr.bezr()
        xCtrl = [[-self.wAxial/2, -self.wAxial/2], [ self.wAxial/2,  self.wAxial/2]]
        yCtrl = [[-self.lAxial/2,  self.lAxial/2], [-self.lAxial/2,  self.lAxial/2]]
        zCtrl = [[0, 0], [0, 0]]
        bAxial0.setControlPoints3D(xCtrl, yCtrl, zCtrl)
        sAxial0 = strp.stripe()
        sAxial0.setSurfs([bAxial0.copy()])

        lAxial = [sAxial0]
        for i in range(self.nPairs):
            sAxial1 = strp.stripe()
            sAxial2 = strp.stripe()

            sAxial1.setSurfs([bAxial0.copy()])
            sAxial2.setSurfs([bAxial0.copy()])

            sAxial1.move(dX= self.sAxial * (i + 1))
            sAxial2.move(dX=-self.sAxial * (i + 1))
            lAxial.append(sAxial1)
            lAxial.append(sAxial2)

        return lAxial

    def genBezierBias(self, srfOffUp, srfOffDn, offBias, bSurfOut=False, bVerb=False, alg=0, nIterMax=25):
        #
        print("Model Info: Generate Central Surfaces", flush=True)

        #
        nU = 5
        nV = 6

        surfRMd = self.genBezierCentral(srfOffUp=srfOffUp, srfOffDn=srfOffDn, nVOut=nV, nU=nU)
        surfLMd = self.getLeftFromRight(surfRMd, dY=-offBias)

        # Re-Evaluate Distribution
        bzrMdPair = bzpr.bezierPair(surfRMd, surfLMd, self.sctnBias, vGap=self.vGap)
        xTrim = self.wAxial * 0.5

        #
        dXI = max(bzrMdPair.ptch.getCorners()[0]) - max(srfOffUp.getCorners()[0])
        dOff = dXI / bzrMdPair.bzrA.getDX()

        bzrMdPair.bzrA = self.genBezierCentral(srfOffUp=srfOffUp, srfOffDn=srfOffDn, nVOut=nV, nU=nU, dOff=dOff)
        bzrMdPair.bzrB = self.getLeftFromRight(bzrMdPair.bzrA, dY=-offBias)

        #
        srfOffUp.refTanBorderBzr['N'] = bzrMdPair.bzrA.getCtrlPt(v=0)
        srfOffDn.refTanBorderBzr['S'] = bzrMdPair.bzrA.getCtrlPt(v=nV-1)

        #
        dAxlSep = self.sAxial - self.wAxial
        xTanMd0 = self.getTanPoints(srfOffUp, nP = 3 * nU, dL = 0.01 * dAxlSep)
        # NOTE: Tangent points are erased. For elliptic section they are pointless. Consider them for lenticular
        xTanMd0 = []

        #
        bzrMdPair.setPenetrationPoints(xTrim, dL = 1.0)
        bzrMdPair.setTangentPoints(xTanMd0)

        #
        algList = ["Steepest Descent", "Conjugate Gradient", "Adam"]
        if(bVerb):
            print("Model Info:     Start Loop", flush=True)
            print("Model Info:     Algorithm: " + algList[alg], flush=True)

        timeStart = time.time()
        uSmpA, vSmpA, uSmpB, vSmpB = bzrMdPair.getSamplePoints(dL = 2.5e-2)
        timeStamp = time.strftime("%H:%M:%S", time.localtime())
        timeEnd = time.time()
        print("Model Info:         Penetration Points: Time: %06.2f s - timeStamp: %s" % (timeEnd - timeStart, timeStamp), flush=True)

        if(bSurfOut):
            srfOffUp = self.propSurf(srfOffUp)
            srfOffDn = self.propSurf(srfOffDn)
            bzrMdPair.export2VTK("%03i"   % (0))
            srfOffUp.export2VTK("Up_%03i" % (0))
            srfOffDn.export2VTK("Dn_%03i" % (0))

        #
        timeStart = time.time()
        X0 = bzrMdPair.bzrA.getCtrlPt(v=0)[2]
        self.getMinimumMdSurfaces(bzrMdPair, bVerbose=False, bInit=True, alpha0=0.1, dAlpha=1.1)
        J0 = self.getSensitivity(srfOffUp, bzrMdPair.bzrA, 5*nU, nU)
        N0 = numpy.linalg.norm(J0)
        N  = N0
        timeEnd = time.time()
        timeStamp = time.strftime("%H:%M:%S", time.localtime())
        print("Model Info:         Connection Iteration: Initial - Res: %0.3e - alpha: %0.3e - Time: %06.2f s - timeStamp: %s" % (N0, 0.0, timeEnd - timeStart, timeStamp), flush=True)

        #
        bzrMdPair.refinePenetrationPointsTri(uSmpA, vSmpA, uSmpB, vSmpB, name = '0', bSVG=False)

        #
        bInit = False
        for refIter in range(100):
            print("Model Info:         Refinement Iter: %03i" % (refIter), flush=True)

            bzrMdPair.initPointSlackPntn(1.0, 1.0)

            #
            N      = 1.0
            iter   = 1
            alpha  = 0.01
            while(iter < nIterMax and N > 1e-2 and alpha > 1e-10 and N < 1e10):
                #
                timeStart = time.time()

                #
                X = [X0[i] - alpha * J0[i] for i in range(nU)]

                #
                bzrMdPair, srfOffUp, srfOffDn = self.updateConnection(bzrMdPair, srfOffUp, srfOffDn, X)

                self.getMinimumMdSurfaces(bzrMdPair, bVerbose=False, alpha0=0.1, dAlpha=1.01, bInit=bInit)

                F = self.getFunction   (srfOffUp, bzrMdPair.bzrA, 5*nU)
                J = self.getSensitivity(srfOffUp, bzrMdPair.bzrA, 5*nU, nU)
                N = numpy.linalg.norm(J)
                C = numpy.dot(J0, J)

                timeEnd = time.time()
                timeStamp = time.strftime("%H:%M:%S", time.localtime())

                if(C < 0.0 and iter > 1):
                    bInit = True
                    alpha /= 10.0
                    N = 1.0
                    print("Model Info:                 Connection Iteration: %03i     - Res: --------- - alpha: %0.3e - Time: %06.2f s - timeStamp: %s - Coeff: %0.3e - Refine Alpha" % (iter, alpha, timeEnd - timeStart, timeStamp, C), flush=True)
                else:
                    bInit = False
                    if(iter > 10 and N < 0.1):
                        alpha *= 1.1
                    alpha = min([0.5, alpha])
                    if(iter == 1):
                        print("Model Info:                 Connection Iteration: %03i     - Res: %0.3e - alpha: %0.3e - Time: %06.2f s - timeStamp: %s" % (iter, N, alpha, timeEnd - timeStart, timeStamp), flush=True)
                    else:
                        print("Model Info:                 Connection Iteration: %03i     - Res: %0.3e - alpha: %0.3e - Time: %06.2f s - timeStamp: %s - Coeff:  %0.3e" % (iter, N, alpha, timeEnd - timeStart, timeStamp, C), flush=True)

                    #
                    X0 = X
                    N0 = N
                    J0 = J

                    #
                    srfOffUp = self.propSurf(srfOffUp)
                    srfOffDn = self.propSurf(srfOffDn)

                    iter += 1

            #
            if(bSurfOut):
                bzrMdPair.export2VTK("%03i"   % (refIter+1))
                srfOffUp.export2VTK("Up_%03i" % (refIter+1))
                srfOffDn.export2VTK("Dn_%03i" % (refIter+1))

            if(alpha <= 1e-10 or N > 1e6):
                print("Model Info: ERROR: Geometry not feasible")
                print('%%%% EXIT WRONG %%%%')
                sys.exit(0)

            #
            pntn = []
            for i in range(len(uSmpA)):
                pntn.append(bzrMdPair.getPenetration(uSmpA[i], vSmpA[i], uSmpB[i], vSmpB[i]))

            if(len(pntn)>0):
                if(min(pntn) < - self.vGap * 1e-2):
                    print("Model Info:             Penetration: %0.3e - Refining penetration points" % (min(pntn)))
                    bzrMdPair.refinePenetrationPointsTri(uSmpA, vSmpA, uSmpB, vSmpB, name = str(refIter+1), bSVG=False)

                else:
                    print("Model Info:             Penetration:", min(pntn))
                    break
            else:
                break

        self.bzrMdPair = bzrMdPair

        return bzrMdPair.bzrA, bzrMdPair.bzrB, srfOffUp, srfOffDn

    def getRefUpCurve(self, surfBUp, surfRUp, u):
        nW = surfRUp.nV

        xCtrUp0, yCtrUp0, zCtrUp0 = surfBUp.getControlPoints(u=u)
        x0 = surfRUp.xCtrl[u][nW - 1]
        z0 = surfRUp.zCtrl[u][nW - 1]
        if(zCtrUp0[nW-1] < z0):
            xTan, zTan, tTan = bezr.bezr().getTangent2D(x0, z0, xCtrUp0, zCtrUp0)
        else:
            xTan = xCtrUp0[nW-1]
            zTan = zCtrUp0[nW-1]
            tTan = 1.0
        uLin = list(numpy.linspace(0, 1, 100))
        xRef = []
        yRef = []
        zRef = []
        for u in uLin:
            x, y, z = bezr.bezr().getPoint2D(u, xCtrUp0, yCtrUp0, zCtrUp0)
            xRef.append(x)
            yRef.append(y)
            if(u < tTan):
                zRef.append(z)
            else:
                zRef.append(self.extrap1([xTan, x0], [zTan, z0], [x])[0])

        xB = []
        zB = []
        for u in uLin:
            x, _, z = bezr.bezr().getPoint2D(u, xCtrUp0, yCtrUp0, zCtrUp0)
            xB.append(x)
            zB.append(z)

        return xRef, yRef, zRef

    def getMinimumMdSurfaces(self, bzrMdPair, bVerbose, alpha0, dAlpha, bInit=False):
        #

        if(bInit):
            bzrMdPair.bzrA.solveDrchlt(bLng=True, bTrn=False)
            bzrMdPair.bzrB.solveDrchlt(bLng=True, bTrn=False)
            bzrMdPair.initPointSlackTngt(1.0, 1.0)
            bzrMdPair.initPointSlackPntn(1.0, 1.0)

        res, nIter = bzrMdPair.minimizeArea(bVerbose=bVerbose, tol=1e-6, alpha0=alpha0, dAlpha=dAlpha)

        return res, nIter

    # Surface replication
    def getLeftFromRight(self, surfR, dX=0, dY=0, dZ=0):
        surfL = bezr.bezr()

        xCtrl = []
        yCtrl = []
        zCtrl = []
        for u in range(surfR.nU):
            xCtrl.append([])
            yCtrl.append([])
            zCtrl.append([])
            for v in range(surfR.nV):
                xCtrl[u].append( surfR.xCtrl[u][v] + dX)
                yCtrl[u].append(-surfR.yCtrl[u][v] + dY)
                zCtrl[u].append(-surfR.zCtrl[u][v] + dZ)

        xCtrl.reverse()
        yCtrl.reverse()
        zCtrl.reverse()

        surfL.setControlPoints3D(xCtrl, yCtrl, zCtrl)

        return surfL

    def getDnFromUp(self, srfUp, srfDn):
        nU = srfUp.nU
        nV = srfUp.nV

        srfDn.zCtrl = [[ - srfUp.zCtrl[nU-1-i][nV-1-j] for j in range(nV)] for i in range(nU)]

        return srfDn

    def replBezierList(self, srfUp, bzrMd, srfDn, dX, dY):
        lBzrPos = []

        for i in range(self.nPairs):
            bzrA = bzrMd.copy()
            srfA = srfUp.copy()
            srfB = srfDn.copy()

            bzrA.move(dX= dX * i, dY= dY * i)
            srfA.move(dX= dX * i, dY= dY * i)
            srfA.move(dX= dX * i, dY= dY * i)
            srfB.move(dX= dX * i, dY= dY * i)

            lBzrPos.append(bzrA)
            if(i % 2 == 0):
                lBzrPos.append(srfB)
            else:
                lBzrPos.append(srfA)
                bzrA.scale(dZ = -1)

        #
        lBzrNeg = []
        for i in range(self.nPairs):
            bzrA = bzrMd.copy()
            srfA = srfUp.copy()
            srfB = srfDn.copy()

            bzrA.move(dX= -dX * (i+1), dY= -dY * (i+1))
            srfA.move(dX= -dX * (i+1), dY= -dY * (i+1))
            srfB.move(dX= -dX * (i+2), dY= -dY * (i+2))

            lBzrNeg.append(bzrA)
            if(i % 2 == 0):
                lBzrNeg.append(srfB)
                bzrA.scale(dZ=-1)
            else:
                lBzrNeg.append(srfA)
        lBzrNeg.reverse()

        #
        lBzr = []
        for bzrNeg in lBzrNeg:
            lBzr.append(bzrNeg)
        lBzr.append(srfUp)
        for bzrPos in lBzrPos:
            lBzr.append(bzrPos)

        #
        for i in range(len(lBzr)):
            if(isinstance(lBzr[i], srfO.surfOff)):
                keys = list(lBzr[i].refTanBorderBzr.keys())

                if(i > 0):
                    iE = lBzr[i-1].nV - 1
                    xCrv, yCrv, zCrv = lBzr[i-1].getCtrlPt(v=iE)
                    lBzr[i].refTanBorderBzr["S"] = (xCrv.copy(), yCrv.copy(), zCrv.copy())

                if(i < len(lBzr)-1):
                    xCrv, yCrv, zCrv = lBzr[i+1].getCtrlPt(v=0)
                    lBzr[i].refTanBorderBzr["N"] = (xCrv.copy(), yCrv.copy(), zCrv.copy())

        for i in range(len(lBzr)):
            if(isinstance(lBzr[i], srfO.surfOff)):
                if('S' in keys):
                    xCrv, yCrv = lBzr[i].refTanBorderBzr["S"][:2]
                if('N' in keys):
                    xCrv, yCrv = lBzr[i].refTanBorderBzr["N"][:2]

        return lBzr

    def copyBezierList(self, bzrList0, dX, dY):
        bzrList = [bzr.copy() for bzr in bzrList0]

        for bzr in bzrList:
            bzr.move(dX=dX, dY=dY)

        return bzrList

    def propSurf(self, surfOff):
        keys = list(surfOff.refTanBorderBzr.keys())
        if(len(keys) == 1):
            key = keys[0]
            curveB = surfOff.refTanBorderBzr[key]

            curveX = [curveB[0][i] - curveB[0][0] for i in range(len(curveB[0]))]
            curveY = [curveB[1][i] - curveB[1][0] for i in range(len(curveB[1]))]
            curveZ = [curveB[2][i] for i in range(len(curveB[1]))]

            curveZ.reverse()

            xCrn, yCrn = surfOff.getCorners()[:2]

            if(keys[0] == 'N'):
                curveXTmp = [x + xCrn[0] for x in curveX]
                curveYTmp = [y + yCrn[0] for y in curveY]
                surfOff.refTanBorderBzr['S'] = (curveXTmp, curveYTmp, curveZ)

            elif(keys[0] == 'S'):
                curveXTmp = [x + xCrn[3] for x in curveX]
                curveYTmp = [y + yCrn[3] for y in curveY]
                surfOff.refTanBorderBzr['N'] = (curveXTmp, curveYTmp, curveZ)

            elif(keys[0] == 'E'):
                curveXTmp = [x + xCrn[0] for x in curveX]
                curveYTmp = [y + yCrn[0] for y in curveY]
                surfOff.refTanBorderBzr['W'] = (curveXTmp, curveYTmp, curveZ)

            elif(keys[0] == 'W'):
                curveXTmp = [x + xCrn[1] for x in curveX]
                curveYTmp = [y + yCrn[1] for y in curveY]
                surfOff.refTanBorderBzr['E'] = (curveXTmp, curveYTmp, curveZ)

        return surfOff

    #
    def getTanPoints(self, surf, nP, dL):
        pnt = []

        uList = numpy.linspace(0, 1, nP)
        for u in uList:
            x0, y0, z0 = surf.getCoord(u, 0.9999)
            tX, tY, tZ = surf.dSdV    (u, 0.9999)

            x = x0 + tX * dL
            y = y0 + tY * dL
            z = z0 + tZ * dL

            pnt.append([x, y, z])

        return pnt

    def correctBorder(self, srfA0, srfB0):
        srfA = srfA0.copy()
        srfB = srfB0.copy()

        nU = srfA.nU - 1

        for i in range(nU):
            srfA.zCtrl[i][0] = max([srfA.zCtrl[i][0], srfB.zCtrl[i][0]])
            srfA.zCtrl[i][1] = max([srfA.zCtrl[i][1], srfB.zCtrl[i][1]])

        return srfA

    #
    def getLocalCoords(self, surf, xPnt):
        uPnt = []
        for x, y, _ in xPnt:
            u, v = surf.getLocalCoords(x, y)
            uPnt.append([u, v])

        return uPnt

    def getFunction(self, surfA, surfB, nP):
        F = 0.0
        uLin = numpy.linspace(0, 1, nP)
        for u in uLin:
            xA, yA, zA = surfA.getPoint3D(u, 0.999)
            xB, yB, zB = surfB.getPoint3D(u, 0.001)
            xC, _,  zC = surfB.getPoint3D(u, 0.0)

            zM  = zA + (xC - xA)*(zB - zA)/(xB - xA)
            F += numpy.power(zC - zM, 2.0)/numpy.sqrt((xA - xB)**2 + (yA - yB)**2)

        return F

    def getSensitivity(self, surfA, surfB, nP, nU):
        J = numpy.zeros(nU)
        uLin = numpy.linspace(0, 1, nP)

        for j in range(nU):
            for u in uLin:
                xA, yA, zA = surfA.getPoint3D(u, 0.999)
                xB, yB, zB = surfB.getPoint3D(u, 0.001)
                xC, _,  zC = surfB.getPoint3D(u, 0.0)
                dC = surfB.dSdP(u, 0.0, j, 0)[2]

                zM  = zA + (xC - xA)*(zB - zA)/(xB - xA)

                J[j] += 2 * (zC - zM) * dC / numpy.sqrt((xA - xB)**2 + (yA - yB)**2)

        return J

    #
    def updateConnection(self, bzrPairMd, srfOffUp, srfOffDn, X):
        nU = bzrPairMd.bzrA.nU
        nV = bzrPairMd.bzrA.nV

        # Replicate change to Md surface
        for i in range(nU):
            bzrPairMd.bzrA.zCtrl[i][0]    =  X[i]
            bzrPairMd.bzrA.zCtrl[i][nV-1] = -X[nU-i-1]
            bzrPairMd.bzrB.zCtrl[i][0]    = -X[nU-i-1]
            bzrPairMd.bzrB.zCtrl[i][nV-1] =  X[i]

            srfOffUp.refTanBorderBzr['N'][2][i] =  X[i]
            srfOffDn.refTanBorderBzr['S'][2][i] = -X[nU-i-1]

        return bzrPairMd, srfOffUp, srfOffDn

    #
    def getLength(self, srfA0, nP, v):
        srfA = srfA0.copy()

        vX = []
        vY = []
        vZ = []
        for i in range(nP):
            xA, yA, zA = srfA.getPoint3D(i/(nP-1), v)
            vX.append(xA)
            vY.append(yA)
            vZ.append(zA)

        L = 0
        for i in range(nP-1):
            dX = vX[i+1] - vX[i]
            dY = vY[i+1] - vY[i]
            dZ = vZ[i+1] - vZ[i]
            L += numpy.sqrt(dX ** 2 + dY ** 2 + dZ ** 2)

        return L

    def getPntVol(self, srfA0, srfB0, nP, v):
        srfA = srfA0.copy()
        srfB = srfB0.copy()

        vX = []
        vY = []
        vZ = []
        for i in range(nP):
            xA, yA, zA = srfA.getPoint3D(i/(nP-1), v)
            zB         = srfB.getPoint3D(i/(nP-1), v)[2]
            vX.append(xA)
            vY.append(yA)
            if(zA - zB < 0):
                vZ.append((zA - zB) ** 2)
            else:
                vZ.append(0)

        z = 0
        for i in range(nP-1):
            dX = vX[i+1] - vX[i]
            dY = vY[i+1] - vY[i]
            zM =(vZ[i+1] + vZ[i])/2
            z += zM * numpy.sqrt(dX ** 2 + dY ** 2)

        return z

    #
    def getDirR(self, x, y):
        dX = 1.0
        dY = 1.0
        dZ = 0.0

        for stripe in self.strBiasR:
            if(stripe.isInside(x, y)):
                dX, dY, dZ = stripe.dSdV(x, y)

        return dX, dY, dZ

    def getDirL(self, x0, y0):
        dX = 1.0
        dY = 1.0
        dZ = 0.0
        for stripe in self.strBiasL:
            if(stripe.isInside(x0, y0)):
                dX, dY, dZ = stripe.dSdV(x0, y0)

        return dX, dY, dZ

    #
    def generateProjectedPolygons(self, offBias, sepBias):
        xAxial0  = [-self.wAxial/2, -self.wAxial/2,  self.wAxial/2,  self.wAxial/2]
        yAxial0  = [-self.lAxial/2,  self.lAxial/2,  self.lAxial/2, -self.lAxial/2]

        xBiasR0  = [-self.wBias /2, -self.wBias /2,  self.wBias /2,  self.wBias /2]
        yBiasR0  = [-self.lAxial/2,  self.lAxial/2,  self.lAxial/2, -self.lAxial/2]

        for i in range(4):
            xNew = xBiasR0[i] * numpy.cos(-self.theta) - yBiasR0[i] * numpy.sin(-self.theta)
            yNew = xBiasR0[i] * numpy.sin(-self.theta) + yBiasR0[i] * numpy.cos(-self.theta)

            xBiasR0[i] = xNew
            yBiasR0[i] = yNew
        yBiasR0 = [y - offBias for y in yBiasR0]

        xBiasL0  = [-self.wBias /2, -self.wBias /2,  self.wBias /2,  self.wBias /2]
        yBiasL0  = [-self.lAxial/2,  self.lAxial/2,  self.lAxial/2, -self.lAxial/2]
        for i in range(4):
            xNew = xBiasL0[i] * numpy.cos( self.theta) - yBiasL0[i] * numpy.sin( self.theta)
            yNew = xBiasL0[i] * numpy.sin( self.theta) + yBiasL0[i] * numpy.cos( self.theta)

            xBiasL0[i] = xNew
            yBiasL0[i] = yNew

        polyAxial = []
        polyBiasR = []
        polyBiasL = []

        polyTmpA = Polygon([[xAxial0[i], yAxial0[i]] for i in range(4)])
        polyTmpR = Polygon([[xBiasR0[i], yBiasR0[i]] for i in range(4)])
        polyTmpL = Polygon([[xBiasL0[i], yBiasL0[i]] for i in range(4)])

        polyAxial.append(polyTmpA)
        polyBiasR.append(polyTmpR)
        polyBiasL.append(polyTmpL)

        for j in range(1, 2 * self.nPairs + 1):
            polyAxial.append(Polygon([[xAxial0[i] + self.sAxial * j, yAxial0[i]] for i in range(4)]))
            polyAxial.append(Polygon([[xAxial0[i] - self.sAxial * j, yAxial0[i]] for i in range(4)]))

            polyBiasR.append(Polygon([[xBiasR0[i], yBiasR0[i] + sepBias * j] for i in range(4)]))
            polyBiasR.append(Polygon([[xBiasR0[i], yBiasR0[i] - sepBias * j] for i in range(4)]))

            polyBiasL.append(Polygon([[xBiasL0[i], yBiasL0[i] + sepBias * j] for i in range(4)]))
            polyBiasL.append(Polygon([[xBiasL0[i], yBiasL0[i] - sepBias * j] for i in range(4)]))

        #
        x0 = -self.sAxial
        y0 = -offBias - sepBias/2

        dx = 2*self.sAxial
        dy = sepBias

        polyBox = Polygon([[x0, y0] , [x0 + dx, y0] , [x0 + dx, y0 + dy] , [x0, y0 + dy]])

        return polyAxial, polyBiasR, polyBiasL, polyBox

    def exportSVG(self, polyAxial, polyBiasR, polyBiasL, polyBox, path="./", nameMod=""):
        fFile = open(path+"model"+nameMod+".svg", "w")

        xMin = str( -   self.lAxial * numpy.sin(self.theta))
        xLng = str( 2 * self.lAxial * numpy.sin(self.theta))
        yMin = str( -   self.lAxial * numpy.cos(self.theta))
        yLng = str( 2 * self.lAxial * numpy.cos(self.theta))

        fFile.write("<svg viewBox=\"" + xMin + " " + yMin + " " + xLng + " " + yLng + "\" xmlns=\"http://www.w3.org/2000/svg\">\n")

        #
        x, y = polyBox.exterior.coords.xy
        pnt0 = str(x[0]) + "," + str(y[0])
        pnt1 = str(x[1]) + "," + str(y[1])
        pnt2 = str(x[2]) + "," + str(y[2])
        pnt3 = str(x[3]) + "," + str(y[3])
        fFile.write("<polygon points=\"" + pnt0 + " " + pnt1 + " " + pnt2 + " " + pnt3 + " \" fill=\"rgba(125, 125, 125, 0.5)\"/>\n")

        for i in range(len(polyAxial)):
            x, y = polyAxial[i].exterior.coords.xy
            pnt0 = str(x[0]) + "," + str(y[0])
            pnt1 = str(x[1]) + "," + str(y[1])
            pnt2 = str(x[2]) + "," + str(y[2])
            pnt3 = str(x[3]) + "," + str(y[3])
            fFile.write("<polygon points=\"" + pnt0 + " " + pnt1 + " " + pnt2 + " " + pnt3 + " \" fill=\"rgba(0, 0, 255, 0.5)\"/>\n")

        for i in range(len(polyBiasR)):
            x, y = polyBiasR[i].exterior.coords.xy
            pnt0 = str(x[0]) + "," + str(y[0])
            pnt1 = str(x[1]) + "," + str(y[1])
            pnt2 = str(x[2]) + "," + str(y[2])
            pnt3 = str(x[3]) + "," + str(y[3])
            fFile.write("<polygon points=\"" + pnt0 + " " + pnt1 + " " + pnt2 + " " + pnt3 + " \" fill=\"rgba(0, 255, 0, 0.5)\"/>\n")

        for i in range(len(polyBiasL)):
            x, y = polyBiasL[i].exterior.coords.xy
            pnt0 = str(x[0]) + "," + str(y[0])
            pnt1 = str(x[1]) + "," + str(y[1])
            pnt2 = str(x[2]) + "," + str(y[2])
            pnt3 = str(x[3]) + "," + str(y[3])
            fFile.write("<polygon points=\"" + pnt0 + " " + pnt1 + " " + pnt2 + " " + pnt3 + " \" fill=\"rgba(255, 0, 0, 0.5)\"/>\n")

        fFile.write("</svg>")
        fFile.close()

    def getCoverFactor(self, polyAxial, polyBiasR, polyBiasL, polyBox):
        A0 = polyBox.area

        polyPart = [polyBox]

        for tool in polyAxial:
            polyPartTmp = []
            for part in polyPart:
                cut = part - tool

                if(cut.geom_type == "Polygon"):
                    polyPartTmp.append(cut)
                elif(cut.geom_type == "MultiPolygon"):
                    for cutSub in cut.geoms:
                        polyPartTmp.append(cutSub)
            polyPart = polyPartTmp

        for tool in polyBiasR:
            polyPartTmp = []
            for part in polyPart:
                cut = part - tool

                if(cut.geom_type == "Polygon"):
                    polyPartTmp.append(cut)
                elif(cut.geom_type == "MultiPolygon"):
                    for cutSub in cut.geoms:
                        polyPartTmp.append(cutSub)
            polyPart = polyPartTmp

        for tool in polyBiasL:
            polyPartTmp = []
            for part in polyPart:
                cut = part - tool

                if(cut.geom_type == "Polygon"):
                    polyPartTmp.append(cut)
                elif(cut.geom_type == "MultiPolygon"):
                    for cutSub in cut.geoms:
                        polyPartTmp.append(cutSub)
            polyPart = polyPartTmp

        A1 = 0
        for pPart in polyPart:
            A1 += pPart.area

        if(A0 != 0):
            cf = 1 - A1 / A0
        else:
            cf = 1.0

        return cf

    def exportBox2VTK(self, name):
        fFile = open(name + ".box.vtk", "w")
        fFile.write("# vtk DataFile Version 2.0\n")
        fFile.write("The direction test\n")
        fFile.write("ASCII\n")
        fFile.write("DATASET UNSTRUCTURED_GRID\n\n")

        fFile.write("POINTS 8 float\n")

        x = [0 for _ in range(8)]
        y = [0 for _ in range(8)]
        z = [0 for _ in range(8)]

        x[0] = self.box["xMin"]
        x[1] = self.box["xMax"]
        x[2] = self.box["xMax"]
        x[3] = self.box["xMin"]
        x[4] = self.box["xMin"]
        x[5] = self.box["xMax"]
        x[6] = self.box["xMax"]
        x[7] = self.box["xMin"]

        y[0] = self.box["yMin"]
        y[1] = self.box["yMin"]
        y[2] = self.box["yMax"]
        y[3] = self.box["yMax"]
        y[4] = self.box["yMin"]
        y[5] = self.box["yMin"]
        y[6] = self.box["yMax"]
        y[7] = self.box["yMax"]

        z[0] = self.box["zMin"]
        z[1] = self.box["zMin"]
        z[2] = self.box["zMin"]
        z[3] = self.box["zMin"]
        z[4] = self.box["zMax"]
        z[5] = self.box["zMax"]
        z[6] = self.box["zMax"]
        z[7] = self.box["zMax"]

        for i in range(8):
            fFile.write(str(x[i]) + " " + str(y[i]) + " " + str(z[i]) + "\n")

        fFile.write("\nCELLS 1 9\n")
        fFile.write("8 0 1 2 3 4 5 6 7\n\n")
        fFile.write("CELL_TYPES 1\n")
        fFile.write("12\n")

        fFile.close()

    def dummyExportDirections(self, name):
        #
        nU = 50
        nV = 50

        #
        fFile = open("dir/dirAxial" + name + ".vtk", "w")
        fFile.write("# vtk DataFile Version 2.0\n")
        fFile.write("The direction test\n")
        fFile.write("ASCII\n")
        fFile.write("DATASET POLYDATA\n\n")

        x  = []
        y  = []
        z  = []
        dX = []
        dY = []
        dZ = []
        for strp in self.strAxial:
            for srf in strp.lSurf:
                for i in range(nU):
                    for j in range(nV):
                        u = numpy.random.random(1)[0]
                        v = numpy.random.random(1)[0]

                        xL, yL, zL = srf.getPoint3D(u, v)

                        if(xL >= self.box["xMin"] and xL <= self.box["xMax"]):
                            if(yL >= self.box["yMin"] and yL <= self.box["yMax"]):
                                if(zL >= self.box["zMin"] and zL <= self.box["zMax"]):
                                    x.append(xL)
                                    y.append(yL)
                                    z.append(zL)

                                    tX, tY, tZ = srf.dSdV(u, v)
                                    tL = numpy.sqrt(tX**2 + tY**2 + tZ**2)
                                    dX.append(tX/tL)
                                    dY.append(tY/tL)
                                    dZ.append(tZ/tL)
        fFile.write("POINTS " + str(len(x)) + " float\n")
        for i in range(len(x)):
            fFile.write(str(x[i]) + " " + str(y[i]) + " " + str(z[i]) + "\n")
        fFile.write("\nPOINT_DATA " + str(len(x)) + "\nVECTORS Direction float\n")
        for i in range(len(x)):
            fFile.write(str(dX[i]) + " " + str(dY[i]) + " " + str(dZ[i]) + "\n")
        fFile.close()


        #
        fFile = open("dir/dirBiasR" + name + ".vtk", "w")
        fFile.write("# vtk DataFile Version 2.0\n")
        fFile.write("The direction test\n")
        fFile.write("ASCII\n")
        fFile.write("DATASET POLYDATA\n\n")

        x  = []
        y  = []
        z  = []
        dX = []
        dY = []
        dZ = []
        for strp in self.strBiasR:
            for srf in strp.lSurf:
                for i in range(nU):
                    for j in range(nV):
                        u = numpy.random.random(1)[0]
                        v = numpy.random.random(1)[0]

                        xL, yL, zL = srf.getPoint3D(u, v)

                        if(xL >= self.box["xMin"] and xL <= self.box["xMax"]):
                            if(yL >= self.box["yMin"] and yL <= self.box["yMax"]):
                                if(zL >= self.box["zMin"] and zL <= self.box["zMax"]):
                                    x.append(xL)
                                    y.append(yL)
                                    z.append(zL)

                                    tX, tY, tZ = srf.dSdV(u, v)
                                    tL = numpy.sqrt(tX**2 + tY**2 + tZ**2)
                                    dX.append(tX/tL)
                                    dY.append(tY/tL)
                                    dZ.append(tZ/tL)
        fFile.write("POINTS " + str(len(x)) + " float\n")
        for i in range(len(x)):
            fFile.write(str(x[i]) + " " + str(y[i]) + " " + str(z[i]) + "\n")
        fFile.write("\nPOINT_DATA " + str(len(x)) + "\nVECTORS Direction float\n")
        for i in range(len(x)):
            fFile.write(str(dX[i]) + " " + str(dY[i]) + " " + str(dZ[i]) + "\n")
        fFile.close()

        #
        fFile = open("dir/dirBiasL" + name + ".vtk", "w")
        fFile.write("# vtk DataFile Version 2.0\n")
        fFile.write("The direction test\n")
        fFile.write("ASCII\n")
        fFile.write("DATASET POLYDATA\n\n")

        x  = []
        y  = []
        z  = []
        dX = []
        dY = []
        dZ = []
        for strp in self.strBiasL:
            for srf in strp.lSurf:
                for i in range(nU):
                    for j in range(nV):
                        u = numpy.random.random(1)[0]
                        v = numpy.random.random(1)[0]

                        xL, yL, zL = srf.getPoint3D(u, v)

                        if(xL >= self.box["xMin"] and xL <= self.box["xMax"]):
                            if(yL >= self.box["yMin"] and yL <= self.box["yMax"]):
                                if(zL >= self.box["zMin"] and zL <= self.box["zMax"]):
                                    x.append(xL)
                                    y.append(yL)
                                    z.append(zL)

                                    tX, tY, tZ = srf.dSdV(u, v)
                                    tL = numpy.sqrt(tX**2 + tY**2 + tZ**2)
                                    dX.append(tX/tL)
                                    dY.append(tY/tL)
                                    dZ.append(tZ/tL)
        fFile.write("POINTS " + str(len(x)) + " float\n")
        for i in range(len(x)):
            fFile.write(str(x[i]) + " " + str(y[i]) + " " + str(z[i]) + "\n")
        fFile.write("\nPOINT_DATA " + str(len(x)) + "\nVECTORS Direction float\n")
        for i in range(len(x)):
            fFile.write(str(dX[i]) + " " + str(dY[i]) + " " + str(dZ[i]) + "\n")
        fFile.close()
