import numpy

class point:
    def __init__(self, x, y, z=0, L=0, S=0, O=0):
        self.x = x
        self.y = y
        self.z = z
        self.L = L
        self.S = S

    def copy(self):
        pntOut = point(0, 0)
        
        pntOut.x = self.x
        pntOut.y = self.y
        pntOut.z = self.z
        pntOut.L = self.L
        pntOut.S = self.S

        return pntOut
    
    def __eq__(self, other):
        d = numpy.sqrt((self.x - other.x)**2 + (self.y - other.y)**2 + (self.z - other.z)**2)

        if(d < 1e-10):
            bEqual = True
        else:
            bEqual = False
        
        return bEqual
    def __repr__(self) -> str:
        return '[' + str(self.x) + ',' + str(self.y) + ',' + str(self.z) + ',' + str(self.L) + ',' + str(self.S) + ']'