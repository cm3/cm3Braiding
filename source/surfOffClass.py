import numpy
import bilnClass as biln
import pntSetClass as pSet
import tool
import bezrClass as bezr
# import matplotlib.pyplot as pyplot

class surfOff:
    def __init__(self):
        self.fnU  = None
        self.fnV  = None
        self.xCrn = None
        self.yCrn = None
        self.dir  = 1
        self.uMin = -1.0
        self.uMax =  1.0
        self.vMin = -1.0
        self.vMax =  1.0
        self.vGap =  0.0

        self.refTanBorderBzr={}

    def copy(self):
        sOut = surfOff()

        sOut.fnU  = self.fnU
        sOut.fnV  = self.fnV
        sOut.xCrn = self.xCrn
        sOut.yCrn = self.yCrn
        sOut.dir  = self.dir
        sOut.uMin = self.uMin
        sOut.uMax = self.uMax
        sOut.vMin = self.vMin
        sOut.vMax = self.vMax
        sOut.vGap = self.vGap

        sOut.refTanBorderBzr = self.refTanBorderBzr.copy()

        return sOut

    def setFn(self, fnU, fnV, dir=1):
        self.fnU = fnU
        self.fnV = fnV
        self.dir = dir

    def setTrim(self, uMin = 0.0, uMax = 1.0, vMin = 0.0, vMax = 1.0):
        self.uMin = uMin
        self.uMax = uMax
        self.vMin = vMin
        self.vMax = vMax

    def setCorners(self, xCrn, yCrn):
        self.xCrn = xCrn
        self.yCrn = yCrn

    def getCorners(self):
        return self.xCrn, self.yCrn

    def getCoord(self, u, v):
        xi  = 2 * (u - 0.5)
        eta = 2 * (v - 0.5)

        xi  = max([-1.0, xi])
        xi  = min([ 1.0, xi])

        eta = max([-1.0, eta])
        eta = min([ 1.0, eta])

        N = biln.biln().N(xi, eta)

        x = numpy.dot(self.xCrn, N)
        y = numpy.dot(self.yCrn, N)

        fU = self.fnU(xi , 2*(self.uMin - 0.5), 2*(self.uMax - 0.5))
        fV = self.fnV(eta, 2*(self.vMin - 0.5), 2*(self.vMax - 0.5))

        z = self.dir * (fU + fV + self.vGap)

        return x, y, z

    def getLocalCoords(self, x0, y0):
        xC, yC = self.getCorners()
        xC = numpy.array(self.xCrn)
        yC = numpy.array(self.yCrn)

        X = numpy.zeros(2)
        
        dV = 1.0
        nI = 0
        while(dV > 1e-6 and nI < 100):
            nI += 1

            N      = biln.biln().N     (X[0], X[1])
            dNdXi  = biln.biln().dNdXi (X[0], X[1])
            dNdEta = biln.biln().dNdEta(X[0], X[1])

            F = numpy.zeros(2)
            F[0] = sum([xC[i] * N[i] for i in range(4)]) - x0
            F[1] = sum([yC[i] * N[i] for i in range(4)]) - y0

            J = numpy.zeros([2, 2])
            J[0][0] = sum([xC[i] * dNdXi [i] for i in range(4)])
            J[1][0] = sum([yC[i] * dNdXi [i] for i in range(4)])
            J[0][1] = sum([xC[i] * dNdEta[i] for i in range(4)])
            J[1][1] = sum([yC[i] * dNdEta[i] for i in range(4)])

            dX = numpy.linalg.solve(J, F)

            X -= dX

            dV =  numpy.linalg.norm(dX)

        u = (X[0] + 1) / 2
        v = (X[1] + 1) / 2

        return u, v

    #
    def getDX(self):
        xCrn = self.getCorners()[0]
        return max(xCrn) - min(xCrn)

    def getDY(self):
        xCrn = self.getCorners()[1]
        return max(xCrn) - min(xCrn)

    def getDZ(self):
        xCrn = self.getCorners()[2]
        return max(xCrn) - min(xCrn)

    #
    def getPoint3D(self, u, v):
        #
        def F(u, v, xR, yR, zR, sgn):
            dH = 1e-3
            x0, y0, z0 = self.getCoord(u, v-dH)
            x1, y1, z1 = self.getCoord(u, v)
            x2, y2, z2 = self.getCoord(u, v+dH)

            m02 =       (z2 - z0)/numpy.sqrt((x2 - x0)**2 + (y2 - y0)**2)
            mR1 = sgn * (zR - z1)/numpy.sqrt((xR - x1)**2 + (yR - y1)**2)

            return mR1 - m02

        #
        keys = list(self.refTanBorderBzr.keys())

        zV = []
        if(len(keys) > 0):
            for key in keys:
                xBzr, yBzr, zBzr = self.refTanBorderBzr[key]

                xR, yR, zR = bezr.bezr().getPoint2D(u, xBzr, yBzr, zBzr)

                nI = 0
                if(key=='S' or key=='N'):
                    dL = 1.0

                    v0  = 0.9 if(key=='N') else  0.1
                    sgn = 1.0 if(key=='N') else -1.0

                    Vn = numpy.linspace(0.0001, 0.9999, 3)
                    Fn = [F(u, vL, xR, yR, zR, sgn) for vL in Vn]

                    while(nI < 100 and dL > 1e-3):
                        nI += 1

                        Vn[1] = (Vn[0] + Vn[2])/2.0
                        Fn[1] = F(u, Vn[1], xR, yR, zR, sgn)

                        if  (numpy.sign(Fn[0]) == numpy.sign(Fn[1]) and numpy.sign(Fn[1]) != numpy.sign(Fn[2])):
                            Vn[0] = Vn[1]
                            Fn[0] = Fn[1]
                        elif(numpy.sign(Fn[0]) != numpy.sign(Fn[1]) and numpy.sign(Fn[1]) == numpy.sign(Fn[2])):
                            Vn[2] = Vn[1]
                            Fn[2] = Fn[1]

                        dL = Vn[2] - Vn[0]

                    v0 = (Vn[2] + Vn[0]) / 2

                    xB, yB, zB = self.getCoord(u, v0)


                    if(v > v0):
                        if(key=='N'):
                            x = tool.extrap1([v0, 1.0], [xB, xR], [v])[0]
                            y = tool.extrap1([v0, 1.0], [yB, yR], [v])[0]
                            z = tool.extrap1([v0, 1.0], [zB, zR], [v])[0]
                        elif(key=='S'):
                            x, y, z = self.getCoord(u, v)
                    else:
                        if(key=='N'):
                            x, y, z = self.getCoord(u, v)
                        elif(key=='S'):
                            x = tool.extrap1([0.0, v0], [xR, xB], [v])[0]
                            y = tool.extrap1([0.0, v0], [yR, yB], [v])[0]
                            z = tool.extrap1([0.0, v0], [zR, zB], [v])[0]

                elif(key=='E' or key == 'W'):
                    raise KeyError("Not implemented for E or W")
                else:
                    x, y, z = self.getCoord(u, v)

                zV.append(z)

            zM = self.getCoord(0.5, 0.5)[2]
            if(zM < 0):
                z = min(zV)
            else:
                z = max(zV)
        else:
            x, y, z = self.getCoord(u, v)

        return x, y, z

    def dSdU(self, u, v):
        dH = 1e-6
        x0, y0, z0 = self.getCoordWithTangency(u,      v)
        x1, y1, z1 = self.getCoordWithTangency(u + dH, v)

        dXdH = (x1 - x0)/dH
        dYdH = (y1 - y0)/dH
        dZdH = (z1 - z0)/dH
        dL = numpy.sqrt(dXdH**2 + dYdH**2 + dZdH**2)

        return dXdH/dL, dYdH/dL, dZdH/dL

    def dSdV(self, u, v):
        dH = 1e-6
        x0, y0, z0 = self.getPoint3D(u, v)
        x1, y1, z1 = self.getPoint3D(u, v + dH)

        dXdH = (x1 - x0)/dH
        dYdH = (y1 - y0)/dH
        dZdH = (z1 - z0)/dH
        dL = numpy.sqrt(dXdH**2 + dYdH**2 + dZdH**2)

        return dXdH/dL, dYdH/dL, dZdH/dL

    #
    def isInsideXY(self, x0, y0):
        xC = self.xCrn.copy()
        yC = self.yCrn.copy()

        xC.append(xC[0])
        yC.append(yC[0])
        nX = len(xC)
        nY = len(yC)

        nInt = 0
        for i in range(nX-1):
            dX = xC[i+1] - xC[i]
            dY = yC[i+1] - yC[i]
            dL = numpy.sqrt(dX ** 2 + dY ** 2)
            dX /= dL
            dY /= dL

            T = numpy.zeros([2, 2])
            x = numpy.zeros( 2)

            T[0][0] = 1
            T[0][1] = -dX
            T[1][0] = 0
            T[1][1] = -dY

            x[0] = xC[i] - x0
            x[1] = yC[i] - y0

            L = numpy.linalg.solve(T, x)
            if(L[0] > 0 and L[1] > 0 and L[1] <= dL):
                nInt += 1

        if(nInt % 2 != 0):
            bIsIn = True
        else:
            bIsIn = False

        return bIsIn

    #
    def move(self, dX=0, dY=0, dZ=0):
        self.xCrn = [x + dX for x in self.xCrn]
        self.yCrn = [y + dY for y in self.yCrn]

        keys = list(self.refTanBorderBzr.keys())
        for key in keys:
            newX = [x + dX for x in self.refTanBorderBzr[key][0]]
            newY = [y + dY for y in self.refTanBorderBzr[key][1]]
            newZ = [z + dZ for z in self.refTanBorderBzr[key][2]]
            self.refTanBorderBzr[key] = (newX, newY, newZ)

    def getLeftFromRight(self, dX=0, dY=0):
        sOut = surfOff()

        sOut.xCrn = self.xCrn.copy()
        sOut.yCrn = self.yCrn.copy()

        sOut.xCrn.reverse()
        sOut.yCrn.reverse()

        sOut.xCrn = tool.rotateList(sOut.xCrn, 2)
        sOut.yCrn = tool.rotateList(sOut.yCrn, 2)

        sOut.fnU = self.fnU
        sOut.fnV = self.fnV
        sOut.dir =-self.dir

        sOut.xCrn = [ x + dX for x in sOut.xCrn]
        sOut.yCrn = [-y + dY for y in sOut.yCrn]

        sOut.uMin = self.uMin
        sOut.uMax = self.uMax
        sOut.vMin = self.vMin
        sOut.vMax = self.vMax
        sOut.vGap = self.vGap

        return sOut

    def resetTanCurve(self):
        self.refTanBorderBzr = {}

    def export2VTK(self, fileMod, nI=100, nJ=100):
        u = numpy.linspace(0, 1, nI)
        v = numpy.linspace(0, 1, nJ)

        fFile = open(fileMod + ".vtk", "w")

        fFile.write("# vtk DataFile Version 2.0\n")
        fFile.write("The direction test\n")
        fFile.write("ASCII\n")
        fFile.write("DATASET POLYDATA\n\n")

        # Init index array
        iIdx = []
        nPnt = 0
        for i in range(nI):
            iIdxTmp = []
            for j in range(nJ):
                nPnt += 1
                iIdxTmp.append(-1)
            iIdx.append(iIdxTmp)

        fFile.write("POINTS " + str(nPnt) + " float\n")

        iPnt = -1
        for i in range(nI):

            for j in range(nJ):
                iPnt += 1
                x, y, z = self.getPoint3D(u[i], v[j])

                iIdx[i][j] = iPnt
                fFile.write(str(x) + " " + str(y) + " " + str(z) + "\n")

        nPoly = (nI - 1)*(nJ - 1)
        fFile.write("\nPOLYGONS " + str(nPoly) + " " + str(nPoly*5) + "\n")

        for i in range(nI - 1):
            for j in range(nJ - 1):
                fFile.write("4 " + str(iIdx[i][j]) + " " + str(iIdx[i+1][j]) + " " + str(
                    iIdx[i+1][j+1]) + " " + str(iIdx[i][j+1]) + "\n")

        fFile.close()
