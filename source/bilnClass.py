class biln:
    def __init__(self):
        pass

    def N(self, xi, eta):
        N = [0.0 for i in range(4)]
        N[0] = 0.25 * (1 - xi) * (1 - eta)
        N[1] = 0.25 * (1 + xi) * (1 - eta)
        N[2] = 0.25 * (1 + xi) * (1 + eta)
        N[3] = 0.25 * (1 - xi) * (1 + eta)

        return N

    def dNdXi(self, xi, eta):
        dNdXi = [0.0 for i in range(4)]
        dNdXi[0] = -0.25 * (1 - eta)
        dNdXi[1] =  0.25 * (1 - eta)
        dNdXi[2] =  0.25 * (1 + eta)
        dNdXi[3] = -0.25 * (1 + eta)

        return dNdXi

    def dNdEta(self, xi, eta):
        dNdEta = [0.0 for i in range(4)]
        dNdEta[0] = -0.25 * (1 - xi)
        dNdEta[1] = -0.25 * (1 + xi)
        dNdEta[2] =  0.25 * (1 + xi)
        dNdEta[3] =  0.25 * (1 - xi)

        return dNdEta