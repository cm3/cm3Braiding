import numpy

class box:
    def __init__(self):
        self.x = [0 for _ in range(8)]
        self.y = [0 for _ in range(8)]
        self.z = [0 for _ in range(8)]

    def setPoint(self, i, x, y, z):
        self.x[i] = x
        self.y[i] = y
        self.z[i] = z

    def getN(self, xi, eta, zeta):
        N = [0 for _ in range(8)]

        N[0] = 0.125 * (1 - xi) * (1 - eta) * (1 - zeta)
        N[1] = 0.125 * (1 + xi) * (1 - eta) * (1 - zeta)
        N[2] = 0.125 * (1 + xi) * (1 + eta) * (1 - zeta)
        N[3] = 0.125 * (1 - xi) * (1 + eta) * (1 - zeta)
        N[4] = 0.125 * (1 - xi) * (1 - eta) * (1 + zeta)
        N[5] = 0.125 * (1 + xi) * (1 - eta) * (1 + zeta)
        N[6] = 0.125 * (1 + xi) * (1 + eta) * (1 + zeta)
        N[7] = 0.125 * (1 - xi) * (1 + eta) * (1 + zeta)

        return N
    
    def getdNdXi(self, xi, eta, zeta):
        dN = [0 for _ in range(8)]

        dN[0] = - 0.125 * (1 - eta) * (1 - zeta)
        dN[1] =   0.125 * (1 - eta) * (1 - zeta)
        dN[2] =   0.125 * (1 + eta) * (1 - zeta)
        dN[3] = - 0.125 * (1 + eta) * (1 - zeta)
        dN[4] = - 0.125 * (1 - eta) * (1 + zeta)
        dN[5] =   0.125 * (1 - eta) * (1 + zeta)
        dN[6] =   0.125 * (1 + eta) * (1 + zeta)
        dN[7] = - 0.125 * (1 + eta) * (1 + zeta)

        return dN
    
    def getdNdEta(self, xi, eta, zeta):
        dN = [0 for _ in range(8)]

        dN[0] = - 0.125 * (1 - xi) * (1 - zeta)
        dN[1] = - 0.125 * (1 + xi) * (1 - zeta)
        dN[2] =   0.125 * (1 + xi) * (1 - zeta)
        dN[3] =   0.125 * (1 - xi) * (1 - zeta)
        dN[4] = - 0.125 * (1 - xi) * (1 + zeta)
        dN[5] = - 0.125 * (1 + xi) * (1 + zeta)
        dN[6] =   0.125 * (1 + xi) * (1 + zeta)
        dN[7] =   0.125 * (1 - xi) * (1 + zeta)

        return dN

    def getdNdZeta(self, xi, eta, zeta):
        dN = [0 for _ in range(8)]

        dN[0] = - 0.125 * (1 - xi) * (1 - eta)
        dN[1] = - 0.125 * (1 + xi) * (1 - eta)
        dN[2] = - 0.125 * (1 + xi) * (1 + eta)
        dN[3] = - 0.125 * (1 - xi) * (1 + eta)
        dN[4] =   0.125 * (1 - xi) * (1 - eta)
        dN[5] =   0.125 * (1 + xi) * (1 - eta)
        dN[6] =   0.125 * (1 + xi) * (1 + eta)
        dN[7] =   0.125 * (1 - xi) * (1 + eta)

        return dN

    def isInside(self, x, y, z):
        xi   = 0
        eta  = 0
        zeta = 0

        dV = 1.0
        nT = 0
        while(dV > 1e-6 and nT < 10):
            nT += 1

            F = numpy.zeros(3)
            J = numpy.zeros([3, 3])

            N       = self.getN      (xi, eta, zeta)
            dNdXi   = self.getdNdXi  (xi, eta, zeta)
            dNdEta  = self.getdNdEta (xi, eta, zeta)
            dNdZeta = self.getdNdZeta(xi, eta, zeta)

            F[0] = numpy.dot(self.x, N) - x
            F[1] = numpy.dot(self.y, N) - y
            F[2] = numpy.dot(self.z, N) - z

            J[0][0] = numpy.dot(self.x, dNdXi)
            J[0][1] = numpy.dot(self.x, dNdEta)
            J[0][2] = numpy.dot(self.x, dNdZeta)

            J[1][0] = numpy.dot(self.y, dNdXi)
            J[1][1] = numpy.dot(self.y, dNdEta)
            J[1][2] = numpy.dot(self.y, dNdZeta)

            J[2][0] = numpy.dot(self.z, dNdXi)
            J[2][1] = numpy.dot(self.z, dNdEta)
            J[2][2] = numpy.dot(self.z, dNdZeta)

            dX = numpy.linalg.solve(J, F)

            xi   -= dX[0]
            eta  -= dX[1]
            zeta -= dX[2]

            dV = numpy.sqrt(numpy.dot(F, F))

        if(-1.0 <= xi and xi <= 1.0 and -1.0 <= eta and eta <= 1.0 and -1.0 <= zeta and zeta <= 1.0):
            return True
        else:
            return False