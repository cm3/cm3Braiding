# General Imports
import argparse
import gmsh
import numpy
import time
import sys
import os

# Project Imports
import braidClass as braid
import chamis
import dataIO
import toolData

#-------------------------------------------------------
timeStart = time.time()

#-------------------------------------------------------
def writeTest(name, msgID):
    cwd = os.getcwd()

    messages = {}
    messages['01'] = "01: Started"
    messages['02'] = "02: Finished"

    file = open('test/test_%s.out' % (str(name)), '+a')
    file.write(messages[msgID] + "\n")
    file.close()

#-------------------------------------------------------
# Argument parsing
#-------------------------------------------------------
parser = argparse.ArgumentParser(description='Creates an RUC and calculates the RVE')
parser.add_argument('--name',      type=str, default='0', help='filename modifier')
parser.add_argument('-dG3D',       type=str, default='',  help='run dG3 solver', required=False, nargs='?')
parser.add_argument('-GMSH',       type=str, default='',  help='open GMSH'     , required=False, nargs='?')
parser.add_argument('--pattern',   type=str, default='',  help='choose pattern to consider')
parser.add_argument('-refCF',      type=str, default='',  help='run a sweep of theta and S_axial of the cover factor', required=False, nargs='?')
parser.add_argument('--rndPrm',    type=str, default='0', help='number of random parameters (0, 2, 4 or 6)')
parser.add_argument('-loadModel',  type=str, default='',  help='load braid model', required=False, nargs='?')

parser.add_argument('-test',       type=str, default='',  help='exports a short status file to be checked.', required=False, nargs='?')

parser.add_argument('-removeMSH',  type=str, default='',  help='remove the MSH file of the model.', required=False, nargs='?')
parser.add_argument('-removeBREP', type=str, default='',  help='remove the BREP file of the model.', required=False, nargs='?')
parser.add_argument('-fileSTP',    type=str, default='',  help='generate a STP file of the model.', required=False, nargs='?')
parser.add_argument('-fileVTK',    type=str, default='',  help='generate a VTK file of the model.', required=False, nargs='?')
parser.add_argument('-fileDir',    type=str, default='',  help='generate the direction files of the model as CSV.', required=False, nargs='?')

args = parser.parse_args()

#-------------------------------------------------------
# Write test file, if commanded
#-------------------------------------------------------
if(args.test is None):
    cwd = os.getcwd()
    if(not os.path.isdir(cwd + "/test")):
        os.makedirs(cwd + "/test")
    fileTest = open('test/test_%s.out' % (str(args.name)), '+w')
    fileTest.close()

    writeTest( args.name, '01')

#-------------------------------------------------------
# Check file structure for run case
#-------------------------------------------------------
folders = ["brd", "csv", "dir", "msh", "png", "res", "stp", "svg", "vtk"]
cwd = os.getcwd()
for folder in folders:
    rfDir = cwd + "/" + folder
    if(not os.path.isdir(rfDir)):
        os.makedirs(rfDir)

#---------------------------------------------
print("Model Info: Model Index:", str(args.name), flush=True)

if(args.dG3D is None):
    from dG3Dpy import *
    print("Model Info: dG3D: To run", flush=True)
else:
    print("Model Info: dG3D: Not to run", flush=True)

if(args.GMSH is None):
    print("Model Info: GMSH: To open", flush=True)
else:
    print("Model Info: GMSH: Not to open", flush=True)

if(args.removeMSH is None):
    print("Model Info: Remove MSH", flush=True)
if(args.fileSTP is None):
    print("Model Info: Generate STP", flush=True)
if(args.fileVTK is None):
    print("Model Info: Generate VTK", flush=True)

#-------------------------------------------------------
# Extract Name From Arguments
#-------------------------------------------------------
modelFile = "model" + args.name
modelIndex = int(args.name)

#-------------------------------------------------------
# Read Input Data
#-------------------------------------------------------
if(args.loadModel != None):
    if(args.pattern == "dia"):
        dataIn = dataIO.importCSVToVariables("data/patternDia0.csv")
        wAxial = dataIn[0]
        hAxial = dataIn[1]
        sAxial = dataIn[2]
        lAxial = dataIn[3]
        vGap   = dataIn[4]
        wBias  = dataIn[5]
        hBias  = dataIn[6]
        dExp   = dataIn[7]
        theta  = dataIn[8]
        braidType = "diamond"

    elif(args.pattern == "reg"):
        dataIn = dataIO.importCSVToVariables("data/patternReg0.csv")
        wAxial = dataIn[0]
        hAxial = dataIn[1]
        sAxial = dataIn[2]
        lAxial = dataIn[3]
        vGap   = dataIn[4]
        wBias  = dataIn[5]
        hBias  = dataIn[6]
        dExp   = dataIn[7]
        theta  = dataIn[8]
        braidType = "regular"

    elif(args.pattern == "cat"):
        dataIn = dataIO.importCSVToVariables("data/patternCat0.csv")
        wAxial = dataIn[0]
        hAxial = dataIn[1]
        sAxial = dataIn[2]
        lAxial = dataIn[3]
        vGap   = dataIn[4]
        wBias  = dataIn[5]
        hBias  = dataIn[6]
        dExp   = dataIn[7]
        theta  = dataIn[8]
        braidType = "regular"

    elif(args.pattern == "modelDia"):
        dataIn = dataIO.importCSVToVariables("csv/model"+args.name+".csv")
        wAxial = dataIn[0]
        hAxial = dataIn[1]
        sAxial = dataIn[2]
        lAxial = dataIn[3]
        vGap   = dataIn[4]
        wBias  = dataIn[5]
        hBias  = dataIn[6]
        dExp   = dataIn[7]
        theta  = dataIn[8]
        braidType = "diamond"

    elif(args.pattern == "modelReg"):
        dataIn = dataIO.importCSVToVariables("csv/model"+args.name+".csv")
        wAxial = dataIn[0]
        hAxial = dataIn[1]
        sAxial = dataIn[2]
        lAxial = dataIn[3]
        vGap   = dataIn[4]
        wBias  = dataIn[5]
        hBias  = dataIn[6]
        dExp   = dataIn[7]
        theta  = dataIn[8]
        braidType = "regular"

    else:
        print("Pattern not recognized", flush=True)
        sys.exit(0)

# Materials
rhoF, ELF, ETF, NuLTF, NuTTF, GLTF, GTTF, nAxial, nBias, dFiber = dataIO.importCSVToVariables("data/materialCarbonT700s.csv")
rhoM, EM, NuM, GM = dataIO.importCSVToVariables("data/materialEpoxyE862.csv")

# Fibers cross section area
areaAxial = nAxial * (numpy.pi * dFiber ** 2) / 4.0
areaBias  = nBias  * (numpy.pi * dFiber ** 2) / 4.0

# Initialize GMSH
gmsh.initialize()
gmsh.model.add("Model" + args.name)
gmsh.option.setNumber("Geometry.Tolerance", 1e-6)
gmsh.option.setNumber("Geometry.AutoCoherence", 2)

#-------------------------------------------------------
# Model
#-------------------------------------------------------
brdFileName = "brd/model%s" % (args.name)
if(args.loadModel == None):
    print("Model Info: Load model", brdFileName)
    if(os.path.exists(brdFileName+".brd")):
        braidModel = dataIO.importFile(brdFileName+".brd")

        if(not os.path.exists(brdFileName+".brep")):
            braidModel.pntsSctnAxial(50)
            braidModel.pntsSctnBias (50)
            braidModel.addToGMSH    (nP0 = 25)
            dataIO.exportFile(brdFileName+".brd", braidModel)
            gmsh.write(brdFileName+".brep")
        else:
            gmsh.model.occ.importShapes(brdFileName+".brep")
            gmsh.model.occ.synchronize()

    else:
        print("Model Info: ERROR: File does not exist")
        print('%%%% EXIT WRONG %%%%')
        sys.exit(0)

else:
    if(args.refCF is None):
        toolData.generateCoverFactor(500, 500, wAxial, hAxial, sAxial, lAxial, wBias, hBias, dExp, vGap, theta, args.name, braidType)
        sys.exit(0)
    else:
        # Generate Model
        sAxial, theta, wAxial, hAxial, wBias, hBias = toolData.generateRandom(int(args.rndPrm), sAxial, theta, wAxial, hAxial, wBias, hBias)
        braidModel = braid.braid(wAxial, hAxial, sAxial, lAxial, wBias, hBias, dExp, vGap, theta, 25, 25, args.name)
        braidModel.generate(braidType, nPair=1, bSurfOut=False)
        dataIO.exportFile(brdFileName+".brd", braidModel)

        braidModel.addToGMSH(nP0 = 25)
        dataIO.exportFile(brdFileName+".brd", braidModel)
        gmsh.write(brdFileName+".brep")

#-------------------------------------------------------
braidModel.getTowType()
braidModel.setVolPhysicalGroups()
braidModel.setSrfPhysicalGroups()
dataIO.exportFile(brdFileName+".brd", braidModel)
gmsh.model.occ.synchronize()

# Intermediate export of braid model
if(args.fileSTP is None):
    gmsh.write("stp/" + modelFile + ".step")

#-------------------------------------------------------
# Mesh
#-------------------------------------------------------
# Extract Surfaces for Refinement
#-------------------------------------------------------    
srfRfnAxl, srfRfnSpr = braidModel.getSurfacesForRefinement()
pntRefinemet = braidModel.getPointsForRefinement()
iPntRfnt = []
for x, y, z in pntRefinemet:
    iPntRfnt.append(gmsh.model.occ.addPoint(x, y, z))
gmsh.model.occ.synchronize()

gmsh.model.addPhysicalGroup(0, iPntRfnt, 9999)
gmsh.model.setPhysicalName (3, 9999, "Refinement Points")
gmsh.model.occ.synchronize()

#-------------------------------------------------------
# Refinement Fields
#-------------------------------------------------------
# Surface section over axial tow
gmsh.model.mesh.field.add("MathEval", 1)
gmsh.model.mesh.field.setString(1, "F", str(braidModel.meshSize(0.05, 0.25) / 6))

gmsh.model.mesh.field.add("Restrict", 2)
gmsh.model.mesh.field.setNumber (2, "InField", 1)
gmsh.model.mesh.field.setNumbers(2, "SurfacesList", srfRfnAxl)

# Distance from points
gmsh.model.mesh.field.add("Distance", 3)
gmsh.model.mesh.field.setNumbers(3, "PointsList", iPntRfnt)

gmsh.model.mesh.field.add("Threshold", 4)
gmsh.model.mesh.field.setNumber(4, "InField", 3)
gmsh.model.mesh.field.setNumber(4, "SizeMin", braidModel.meshSize(0.10, 0.25) / 6)
gmsh.model.mesh.field.setNumber(4, "SizeMax", 0.25)
gmsh.model.mesh.field.setNumber(4, "DistMin", min([braidModel.wAxial, braidModel.wBias]) / 8.0)
gmsh.model.mesh.field.setNumber(4, "DistMax", min([braidModel.wAxial, braidModel.wBias]) / 6.0)

gmsh.model.mesh.field.add("Restrict", 5)
gmsh.model.mesh.field.setNumber (5, "InField", 4)
gmsh.model.mesh.field.setNumbers(5, "SurfacesList", srfRfnSpr)

# All together
gmsh.model.mesh.field.add("Min", 10)
gmsh.model.mesh.field.setNumbers(10, "FieldsList", [2, 5])
gmsh.model.mesh.field.setAsBackgroundMesh(10)

#-------------------------------------------------------
# General Mesh Parameters
#-------------------------------------------------------
gmsh.option.setNumber("Mesh.Algorithm",                  5)
gmsh.option.setNumber("Mesh.Algorithm3D",                1)
gmsh.option.setNumber("Mesh.SecondOrderLinear",          1)
gmsh.option.setNumber("Mesh.MeshSizeFromPoints",         0)
gmsh.option.setNumber("Mesh.MeshSizeExtendFromBoundary", 0)
gmsh.option.setNumber("Mesh.MeshSizeFromCurvature",      3)
gmsh.option.setNumber("Mesh.MeshSizeMax",                0.25)
gmsh.option.setNumber("Mesh.MeshSizeMin",                0.05)
gmsh.option.setNumber("Mesh.Smoothing",                  250)
gmsh.option.setNumber("Mesh.OptimizeNetgen",             True)
gmsh.model.mesh.generate(3)
gmsh.write("msh/" + modelFile + ".msh")

#-------------------------------------------------------
# Open FLTK
#-------------------------------------------------------
if(args.GMSH is None):
    gmsh.fltk.run()

#-------------------------------------------------------
# Get the Tows Volumes
#-------------------------------------------------------
volume = [0, 0, 0, 0]
for v in range(4):
    gmsh.plugin.setNumber("MeshVolume", "Dimension", 3)
    gmsh.plugin.setNumber("MeshVolume", "PhysicalGroup", 1001 + v)
    gmsh.plugin.run("MeshVolume")
    views = gmsh.view.getTags()
    _, _, data = gmsh.view.getListData(views[-1])
    volume[v] = data[0][-1]

#-------------------------------------------------------
# Volume fraction
#-------------------------------------------------------
VfA = areaAxial / braidModel.sctnAxial.getArea()
VfB = areaBias  / braidModel.sctnBias. getArea()

if(args.pattern == "cat"):
    # Catera imposes the tows volume fractions
    VfA = 0.86
    VfB = 0.86

vA   = volume[0]
vB   = volume[1] + volume[2]
vRUC = sum(volume)
VfRUC = (VfA * vA + VfB * vB) / vRUC

dctVolFct = {"VfA": VfA, "VfB": VfB, "VfRUC": VfRUC, "vA": vA, "vB": vB, "vRUC": vRUC}
dataIO.exportDictionaryToCSV("csv/model" + args.name + "vf", [dctVolFct])

#-------------------------------------------------------
# Export to VTK to check geometry
#-------------------------------------------------------
if(args.fileVTK is None):
    gmsh.model.removePhysicalGroups([(3, 1004)])
    gmsh.model.removePhysicalGroups([(2, 100)])
    gmsh.model.removePhysicalGroups([(2, 101)])
    gmsh.model.removePhysicalGroups([(2, 110)])
    gmsh.model.removePhysicalGroups([(2, 111)])
    gmsh.model.removePhysicalGroups([(2, 120)])
    gmsh.model.removePhysicalGroups([(2, 121)])
    gmsh.write("vtk/" + modelFile + ".vtk")
    braidModel.exportBox2VTK("vtk/" + modelFile)

#
if(args.dG3D is None):
    bFileDir = True if args.fileDir == None else False
    if(bFileDir):
        # Debug file to check if directions are correctly calculated
        nameDirA = "dir/dirA" + args.name + ".csv"
        nameDirR = "dir/dirR" + args.name + ".csv"
        nameDirL = "dir/dirL" + args.name + ".csv"

        fileDirA = open(nameDirA, "w")
        fileDirR = open(nameDirR, "w")
        fileDirL = open(nameDirL, "w")

        fileDirA.write("x;y;z;dX;dY;dZ\n")
        fileDirR.write("x;y;z;dX;dY;dZ\n")
        fileDirL.write("x;y;z;dX;dY;dZ\n")

        fileDirA.close()
        fileDirR.close()
        fileDirL.close()

    # Definition of direction function
    def dirA(x, y, z):
        global braidModel, nameDirA, bFileDir

        dX = 0
        dY = 1
        dZ = 0

        if(bFileDir):
            fileDir = open(nameDirA, "a")
            fileDir.write(str(x) + ";" + str(y) + ";" + str(z) + ";" + str(dX) + ";" + str(dY) + ";" + str(dZ) + "\n")
            fileDir.close()

        return dX, dY, dZ

    def dirR(x, y, z):
        global braidModel, nameDirR, bFileDir

        dX, dY, dZ = braidModel.getDirR(x, y)

        if(bFileDir):
            fileDir = open(nameDirR, "a")
            fileDir.write(str(x) + ";" + str(y) + ";" + str(z) + ";" + str(dX) + ";" + str(dY) + ";" + str(dZ) + "\n")
            fileDir.close()

        return dX, dY, dZ

    def dirL(x, y, z):
        global braidModel, nameDirL, bFileDir

        dX, dY, dZ = braidModel.getDirL(x, y)

        if(bFileDir):
            fileDir = open(nameDirL, "a")
            fileDir.write(str(x) + ";" + str(y) + ";" + str(z) + ";" + str(dX) + ";" + str(dY) + ";" + str(dZ) + "\n")
            fileDir.close()

        return dX, dY, dZ

    lawnum1 = 1    # unique number of law
    rho1, E1_L, E1_T, G1_LT, G1_TT, nu1_TT, nu1_LT_major, nu1_LT_minor, chamisDictA = chamis.chamis(VfA, rhoF, rhoM, ELF, ETF, EM, GLTF, GTTF, GM, NuLTF, NuM)
    dirGenAxial = UserDirectionGeneratorPython(dirA)

    lawnum2 = 2    # unique number of law
    rho2, E2_L, E2_T, G2_LT, G2_TT, nu2_TT, nu2_LT_major, nu2_LT_minor, chamisDictB = chamis.chamis(VfB, rhoF, rhoM, ELF, ETF, EM, GLTF, GTTF, GM, NuLTF, NuM)
    dirGenTowL = UserDirectionGeneratorPython(dirR)

    lawnum3 = 3    # unique number of law
    dirGenTowR = UserDirectionGeneratorPython(dirL)

    lawnum4 = 4    # unique number of law
    rho4    = rhoM # [kg/m^3] Density
    E4      = EM   # [Pa]
    nu4     = NuM

    # Export Chamis Values
    dataIO.exportDictionaryToCSV("csv/model" + args.name + "chamis", [chamisDictA, chamisDictB])

    # Creation of material laws:
    law1 = TransverseIsoUserDirectionDG3DMaterialLaw(lawnum1, rho1, E1_T, nu1_TT, E1_L, G1_LT, nu1_LT_minor, dirGenAxial)
    law2 = TransverseIsoUserDirectionDG3DMaterialLaw(lawnum2, rho2, E2_T, nu2_TT, E2_L, G2_LT, nu2_LT_minor, dirGenTowL)
    law3 = TransverseIsoUserDirectionDG3DMaterialLaw(lawnum3, rho2, E2_T, nu2_TT, E2_L, G2_LT, nu2_LT_minor, dirGenTowR)
    law4 = dG3DLinearElasticMaterialLaw(lawnum4, rho4, E4, nu4)

    myfield1 = dG3DDomain(modelIndex, 1001, 0, lawnum1, 0, 3) # axial tows
    myfield2 = dG3DDomain(modelIndex, 1002, 0, lawnum2, 0, 3) # bias tows minus
    myfield3 = dG3DDomain(modelIndex, 1003, 0, lawnum3, 0, 3) # bias tows plus
    myfield4 = dG3DDomain(modelIndex, 1004, 0, lawnum4, 0, 3) # pure matrix region

    # ------------------------------------------------ solver -------------------------------------------------------------
    sol         = 2     # Gmm=0 (default) Taucs=1 PETsc=2
    soltype     = 1     # StaticLinear=0 (default) StaticNonLinear=1
    nstep       = 1     # number of step (used only if soltype=1)
    ftime       = 1.    # Final time (used only if soltype=1)
    tol         = 1.e-5 # relative tolerance for NR scheme (used only if soltype=1)
    nstepArch   = 1     # Number of step between 2 archiving (used only if soltype=1)

    # creation of Solver
    mysolver = nonLinearMechSolver(modelIndex)
    mysolver.loadModel           ("msh/" + modelFile + ".msh")
    mysolver.addDomain           (myfield1)
    mysolver.addMaterialLaw      (law1)
    mysolver.addDomain           (myfield2)
    mysolver.addMaterialLaw      (law2)
    mysolver.addDomain           (myfield3)
    mysolver.addMaterialLaw      (law3)
    mysolver.addDomain           (myfield4)
    mysolver.addMaterialLaw      (law4)
    mysolver.Scheme              (soltype)
    mysolver.Solver              (sol)
    mysolver.snlData             (nstep,ftime,tol)
    mysolver.stepBetweenArchiving(nstepArch)

    #options for solvers with PETSc
    mysolver.options("-ksp_type preonly -pc_type lu")

    # Solver:
    # Displacement elimination = 0
    # Multiplier elimination   = 1 
    # Displacement+ multiplier = 2 
    # Constraint elimination   = 3
    # Note: faster with 3, but limited in terms of surface elements number
    system = 3

    # Control:
    # Load control = 0
    # Arc length control euler = 1
    control = 0
    mysolver.setSystemType(system)
    mysolver.setControlType(control)

    microBC = nonLinearPeriodicBC(modelIndex, 3)
    microBC.setBCPhysical(100, 110, 120, 101, 111, 121) # physical for BC, periodic beween 100-101, 110-111, 120-121
    method    = 5       # 5 - projection method
    degree    = 3       # Order used for polynomial interpolation
    addvertex = False   # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex
    microBC.setPeriodicBCOptions(method, degree, addvertex) # degree,addvertex are not used when using method=5

    # applied strain
    microBC.setDeformationGradient(1.0, 0.0, 0., 0., 1., 0., 0., 0., 1.0)# FXX, FXY, FXZ, FYX, FYY, FYZ, FZX, FZY, FZZ
    #
    mysolver.addMicroBC(microBC)

    mysolver.stressAveragingFlag(True) # set stress averaging ON- True , OFF-False
    mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface

    #tangent averaging flag
    mysolver.tangentAveragingFlag(True) # set tangent averaging ON -True, OFF -False
    mysolver.setTangentAveragingMethod(2) # 0- perturbation, 1- condensation 2 - in-system

    mysolver.internalPointBuildView("sig_xx", IPField.SIG_XX)
    mysolver.internalPointBuildView("sig_yy", IPField.SIG_YY)
    mysolver.internalPointBuildView("sig_zz", IPField.SIG_ZZ)
    mysolver.internalPointBuildView("sig_xy", IPField.SIG_XY)
    mysolver.internalPointBuildView("sig_xz", IPField.SIG_XZ)
    mysolver.internalPointBuildView("sig_yz", IPField.SIG_YZ)
    mysolver.internalPointBuildView("sig_VM", IPField.SVM)
    mysolver.internalPointBuildView("eps_xx", IPField.STRAIN_XX)
    mysolver.internalPointBuildView("eps_yy", IPField.STRAIN_YY)
    mysolver.internalPointBuildView("eps_zz", IPField.STRAIN_ZZ)
    mysolver.internalPointBuildView("eps_xy", IPField.STRAIN_XY)
    mysolver.internalPointBuildView("eps_xz", IPField.STRAIN_XZ)
    mysolver.internalPointBuildView("eps_yz", IPField.STRAIN_YZ)

    mysolver.internalPointBuildView("n_x", IPField.USER0)
    mysolver.internalPointBuildView("n_y", IPField.USER1)
    mysolver.internalPointBuildView("n_z", IPField.USER2)

    mysolver.initMicroSolver() #if only extracting the homogenized Hooke tensor

#
if(args.removeMSH is None):
    if(os.path.isfile("msh/" + modelFile + ".msh")):
        os.remove("msh/" + modelFile + ".msh")
        print('Model Info: Removed file', modelFile + ".msh" )

if(args.removeBREP is None):
    if(os.path.isfile("brd/" + modelFile + ".brep")):
        os.remove("brd/" + modelFile + ".brep")
        print('Model Info: Removed file', modelFile + ".brep" )

#
timeEnd = time.time()
print("Model Info: Run time:", timeEnd - timeStart, flush=True)
print('%%%% EXIT OK %%%%')
