class pntSet:
    def __init__(self, xPnt=[]):
        self.X = []
        self.Y = []
        self.Z = []

        for x, y, z in xPnt:
            self.X.append([x])
            self.Y.append([y])
            self.Z.append([z])
        
    def export2VTK(self, fileMod):
        nX = len(self.X)
        if(nX > 0):
            nY = len(self.X[0])
        else:
            nY = 0

        fFile = open("pntSet" + fileMod + ".vtk", "w")

        fFile.write("# vtk DataFile Version 2.0\n")
        fFile.write("The direction test\n")
        fFile.write("ASCII\n")
        fFile.write("DATASET POLYDATA\n\n")

        # Init index array
        iIdx = []
        nPnt = 0
        for i in range(nX):
            iIdxTmp = []
            for j in range(nY):
                nPnt += 1
                iIdxTmp.append(-1)
            iIdx.append(iIdxTmp)

        fFile.write("POINTS " + str(nPnt) + " float\n")

        iPnt = -1
        for i in range(nX):
            for j in range(nY):
                iPnt += 1
                iIdx[i][j] = iPnt
                fFile.write(str(self.X[i][j]) + " " + str(self.Y[i][j]) + " " + str(self.Z[i][j]) + "\n")


    