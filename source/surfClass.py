import numpy
# import matplotlib.pyplot as pyplot
import bilnClass as biln
import tool

class surf:
    # Default constructor
    def __init__(self, nI=0, nJ=0):
        self.nI = nI
        self.nJ = nJ

        self.X, self.Y, self.Z , self.W = self.initZero(self.nI, self.nJ)

    #
    def __add__(self, otherSurf):
        nI = self.nI
        nJ = self.nJ

        surfOut = self.copy()
        for i in range(nI):
            for j in range(nJ):
                surfOut.Z[i][j] = self.Z[i][j] + otherSurf.Z[i][j]

        return surfOut
    
    def __sub__(self, otherSurf):
        nI = self.nI
        nJ = self.nJ

        surfOut = self.copy()
        for i in range(nI):
            for j in range(nJ):
                surfOut.Z[i][j] = self.Z[i][j] - otherSurf.Z[i][j]
 
        return surfOut

    def copy(self):
        nI = self.nI
        nJ = self.nJ

        surfOut = surf(nI, nJ)
        for i in range(nI):
            for j in range(nJ):
                surfOut.X[i][j] = self.X[i][j]
                surfOut.Y[i][j] = self.Y[i][j]
                surfOut.Z[i][j] = self.Z[i][j]
                surfOut.W[i][j] = self.W[i][j]

        return surfOut

    # Resize surface and intialize values to 0
    def resize(self, nI, nJ):
        self.nI = nI
        self.nJ = nJ

        self.X, self.Y, self.Z = self.initZero(self.nI, self.nJ)

    # Intialize values to 0
    def initZero(self, nI, nJ):
        xSurf = []
        ySurf = []
        zSurf = []
        wSurf = []

        for i in range(nI):
            xSurf.append([])
            ySurf.append([])
            zSurf.append([])
            wSurf.append([])

            for j in range(nJ):
                xSurf[i].append(0.0)
                ySurf[i].append(0.0)
                zSurf[i].append(0.0)
                wSurf[i].append(0.0)
        
        return xSurf, ySurf, zSurf, wSurf

    def move(self, dX=0, dY=0, dZ=0):
        nI = self.nI
        nJ = self.nJ
        for i in range(nI):
            for j in range(nJ):
                self.X[i][j] += dX
                self.Y[i][j] += dY
                self.Z[i][j] += dZ

    #    
    def subSurfInZ(self, dZ):
        if(self.nI == len(dZ) and self.nJ == len(dZ[0])):
            surfOut = surf(self.nI, self.nJ)

            for i in range(self.nI):
                for j in range(self.nJ):
                    surfOut.X[i][j] = self.X[i][j]
                    surfOut.Y[i][j] = self.Y[i][j]
                    surfOut.Z[i][j] = self.Z[i][j] - dZ[i][j]
        else:
            print("Surface size not compatible", flush=True)
            raise("Surface size not compatible")
        return surfOut

    def getMaxZ(self):
        zMax = 0
        for i in range(self.nI):
            for j in range(self.nJ):
                zMax = max([zMax, self.Z[i][j]])

        return zMax

    def getMinZ(self):
        zMin = 0
        for i in range(self.nI):
            for j in range(self.nJ):
                zMin = min([zMin, self.Z[i][j]])

        return zMin

    #
    def getSliceI(self, i):
        slcX = []
        slcY = []
        slcZ = []
        for j in range(self.nJ):
            slcX.append(self.X[i][j])
            slcY.append(self.Y[i][j])
            slcZ.append(self.Z[i][j])

        return slcX, slcY, slcZ

    def getSliceJ(self, j):
        slcX = []
        slcY = []
        slcZ = []
        for i in range(self.nI):
            slcX.append(self.X[i][j])
            slcY.append(self.Y[i][j])
            slcZ.append(self.Z[i][j])

        return slcX, slcY, slcZ

    def setSlcZI(self, i, z):
        for j in range(self.nJ):
            self.Z[i][j] = z[j]

    def setSlcZJ(self, j, z):
        for i in range(self.nI):
            self.Z[i][j] = z[i]

    def getLocalCoords(self, x0, y0):
        nI = self.nI
        nJ = self.nJ

        bl = biln.biln()

        #
        i0 = 0
        j0 = 0

        #
        for i in range(nI*nJ):
            xC = [self.X[i0][j0], self.X[i0 + 1][j0], self.X[i0 + 1][j0 + 1], self.X[i0][j0 + 1]]
            yC = [self.Y[i0][j0], self.Y[i0 + 1][j0], self.Y[i0 + 1][j0 + 1], self.Y[i0][j0 + 1]]

            xi  = 0
            eta = 0

            dV = 1.0
            nT = 0
            while(dV > 1e-6 and nT < 1000):
                nT += 1

                N      = bl.N(xi, eta)
                dNdXi  = bl.dNdXi(xi, eta)
                dNdEta = bl.dNdEta(xi, eta)

                F = numpy.zeros(2)
                J = numpy.zeros([2, 2])

                F[0] = numpy.dot(N, xC) - x0
                F[1] = numpy.dot(N, yC) - y0

                J[0][0] = numpy.dot(dNdXi,  xC)
                J[0][1] = numpy.dot(dNdEta, xC)
                J[1][0] = numpy.dot(dNdXi,  yC)
                J[1][1] = numpy.dot(dNdEta, yC)

                dN = numpy.linalg.solve(J, F)
                dV = numpy.sqrt(numpy.dot(F, F))

                xi  -= dN[0]
                eta -= dN[1]
            
            if(xi > 1):
                i0 += 1
            elif(xi < -1):
                i0 -= 1

            if(eta > 1):
                j0 += 1
            elif(eta < -1):
                j0 -= 1

            if(xi >= -1 and xi <= 1 and eta >= -1 and eta <= 1):
                break

            i0 = min([i0, nI - 2])
            j0 = min([j0, nJ - 2])

            i0 = max([i0, 0])
            j0 = max([j0, 0])

        return i0, j0, xi, eta
    
    def getPoint3D(self, i0, j0, xi, eta):
        xC = [self.X[i0][j0], self.X[i0 + 1][j0], self.X[i0 + 1][j0 + 1], self.X[i0][j0 + 1]]
        yC = [self.Y[i0][j0], self.Y[i0 + 1][j0], self.Y[i0 + 1][j0 + 1], self.Y[i0][j0 + 1]]
        zC = [self.Z[i0][j0], self.Z[i0 + 1][j0], self.Z[i0 + 1][j0 + 1], self.Z[i0][j0 + 1]]
        bl = biln.biln()
        N = bl.N(xi, eta)

        return numpy.dot(N, xC), numpy.dot(N, yC), numpy.dot(N, zC)

    #
    def getTangent(self, i0, j0, xi, eta, dH=1e-6):
        xC = [self.X[i0][j0], self.X[i0 + 1][j0], self.X[i0 + 1][j0 + 1], self.X[i0][j0 + 1]]
        yC = [self.Y[i0][j0], self.Y[i0 + 1][j0], self.Y[i0 + 1][j0 + 1], self.Y[i0][j0 + 1]]
        zC = [self.Z[i0][j0], self.Z[i0 + 1][j0], self.Z[i0 + 1][j0 + 1], self.Z[i0][j0 + 1]]

        bl = biln.biln()

        dNdXi  = bl.dNdXi (xi, eta)
        dNdEta = bl.dNdEta(xi, eta)

        dXdXi  = numpy.dot(dNdXi,  xC)
        dYdXi  = numpy.dot(dNdXi,  yC)
        dZdXi  = numpy.dot(dNdXi,  zC)

        dXdEta = numpy.dot(dNdEta, xC)
        dYdEta = numpy.dot(dNdEta, yC)
        dZdEta = numpy.dot(dNdEta, zC)

        dLXi  = numpy.sqrt(dXdXi **2 + dYdXi **2 + dZdXi **2)
        dLEta = numpy.sqrt(dXdEta**2 + dYdEta**2 + dZdEta**2)

        tgXi  = [dXdXi /dLXi,  dYdXi /dLXi,  dZdXi /dLXi]
        tgEta = [dXdEta/dLEta, dYdEta/dLEta, dZdEta/dLEta]

        return tgXi, tgEta
        
    def getNormal(self, i0, j0, xi, eta, dH=1e-6):
        tgXi, tgEta = self.getTangent(i0, j0, xi, eta, dH)

        nrml = numpy.cross(tgEta, tgXi)
        lnml = numpy.sqrt(nrml[0]**2 + nrml[1]**2 + nrml[2]**2)
        nrml /= lnml
        
        return nrml

    #
    def linBorder(self, dst):
        self.linN(dst)
        self.linS(dst)
        self.linE(dst)
        self.linW(dst)

    def linN(self, dst):
        for i in range(self.nI):
            xSlc, ySlc, zSlc = self.getSliceI(i)
            dX = [xSlc[j+1] - xSlc[j] for j in range(len(xSlc)-1)]
            dY = [ySlc[j+1] - ySlc[j] for j in range(len(xSlc)-1)]
            lSlc = [0.0]
            for j in range(len(dX)):
                lSlc.append(lSlc[j] + numpy.sqrt(dX[j]**2 + dY[j]**2))
            for j in range(len(lSlc)):
                lSlc[j] /= lSlc[len(lSlc)-1]
            lTrm = []
            zTrm = []
            for j in range(len(lSlc)):
                if(lSlc[j] < 1 - dst):
                    lTrm.append(lSlc[j])
                    zTrm.append(zSlc[j])
            zLin = tool.extrap1(lTrm, zTrm, lSlc)
            for j in range(self.nJ):
                self.Z[i][j] = zLin[j]
    
    def linS(self, dst):
        for i in range(self.nI):
            xSlc, ySlc, zSlc = self.getSliceI(i)
            dX = [xSlc[j+1] - xSlc[j] for j in range(len(xSlc)-1)]
            dY = [ySlc[j+1] - ySlc[j] for j in range(len(xSlc)-1)]
            lSlc = [0.0]
            for j in range(len(dX)):
                lSlc.append(lSlc[j] + numpy.sqrt(dX[j]**2 + dY[j]**2))
            for j in range(len(lSlc)):
                lSlc[j] /= lSlc[len(lSlc)-1]
            lTrm = []
            zTrm = []
            for j in range(len(lSlc)):
                if(lSlc[j] > dst):
                    lTrm.append(lSlc[j])
                    zTrm.append(zSlc[j])
            zLin = tool.extrap1(lTrm, zTrm, lSlc)
            for j in range(self.nJ):
                self.Z[i][j] = zLin[j]

    def linE(self, dst):
        for j in range(self.nJ):
            xSlc, ySlc, zSlc = self.getSliceJ(j)
            dX = [xSlc[i+1] - xSlc[i] for i in range(len(xSlc)-1)]
            dY = [ySlc[i+1] - ySlc[i] for i in range(len(xSlc)-1)]
            lSlc = [0.0]
            for i in range(len(dX)):
                lSlc.append(lSlc[i] + numpy.sqrt(dX[i]**2 + dY[i]**2))
            for i in range(len(lSlc)):
                lSlc[i] /= lSlc[len(lSlc)-1]
            lTrm = []
            zTrm = []
            for i in range(len(lSlc)):
                if(lSlc[i] < 1 - dst):
                    lTrm.append(lSlc[i])
                    zTrm.append(zSlc[i])
            zLin = tool.extrap1(lTrm, zTrm, lSlc)
            for i in range(self.nI):
                self.Z[i][j] = zLin[i]

    def linW(self, dst):
        for j in range(self.nJ):
            xSlc, ySlc, zSlc = self.getSliceJ(j)
            dX = [xSlc[i+1] - xSlc[i] for i in range(len(xSlc)-1)]
            dY = [ySlc[i+1] - ySlc[i] for i in range(len(xSlc)-1)]
            lSlc = [0.0]
            for i in range(len(dX)):
                lSlc.append(lSlc[i] + numpy.sqrt(dX[i]**2 + dY[i]**2))
            for i in range(len(lSlc)):
                lSlc[i] /= lSlc[len(lSlc)-1]
            lTrm = []
            zTrm = []
            for i in range(len(lSlc)):
                if(lSlc[i] > dst):
                    lTrm.append(lSlc[i])
                    zTrm.append(zSlc[i])
            zLin = tool.extrap1(lTrm, zTrm, lSlc)
            for i in range(self.nI):
                self.Z[i][j] = zLin[i]
    #
    def export2VTK(self, fileMod):
        nX = len(self.X)
        nY = len(self.X[0])

        fFile = open("surfSrf" + fileMod + ".vtk", "w")

        fFile.write("# vtk DataFile Version 2.0\n")
        fFile.write("The direction test\n")
        fFile.write("ASCII\n")
        fFile.write("DATASET POLYDATA\n\n")

        # Init index array
        iIdx = []
        nPnt = 0
        for i in range(nX):
            iIdxTmp = []
            for j in range(nY):
                nPnt += 1
                iIdxTmp.append(-1)
            iIdx.append(iIdxTmp)

        fFile.write("POINTS " + str(nPnt) + " float\n")

        iPnt = -1
        for i in range(nX):
            for j in range(nY):
                iPnt += 1
                iIdx[i][j] = iPnt
                fFile.write(str(self.X[i][j]) + " " + str(self.Y[i][j]) + " " + str(self.Z[i][j]) + "\n")

        nPoly = (nX - 1)*(nY - 1)
        fFile.write("\nPOLYGONS " + str(nPoly) + " " + str(nPoly*5) + "\n")

        for i in range(nX - 1):
            for j in range(nY - 1):
                fFile.write("4 " + str(iIdx[i][j]) + " " + str(iIdx[i+1][j]) + " " + str(
                    iIdx[i+1][j+1]) + " " + str(iIdx[i][j+1]) + "\n")

        fFile.write("\nPOINT_DATA " + str(nPnt) + "\n")
        fFile.write("SCALARS Weights float 1\nLOOKUP_TABLE table\n")
        for i in range(nX):
            for j in range(nY):
                fFile.write(str(self.W[i][j]) + "\n")


        fFile.close()
    
    def getCorners(self):
        xCrnr = [self.X[0][0], self.X[self.nI - 1][0], self.X[self.nI - 1][self.nJ - 1], self.X[0][self.nJ - 1]]
        yCrnr = [self.Y[0][0], self.Y[self.nI - 1][0], self.Y[self.nI - 1][self.nJ - 1], self.Y[0][self.nJ - 1]]
        zCrnr = [self.Z[0][0], self.Z[self.nI - 1][0], self.Z[self.nI - 1][self.nJ - 1], self.Z[0][self.nJ - 1]]
        
        return xCrnr, yCrnr, zCrnr
                