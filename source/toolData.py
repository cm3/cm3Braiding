import numpy
import pickle
import braidClass as braid

def generateCoverFactor(nI, nJ, wAxial, hAxial, sAxial, lAxial, wBias, hBias, dExp, vGap, theta, name, braidType):
    vX = numpy.linspace(sAxial - 5.0, sAxial + 5.0, nJ)
    vY = numpy.linspace(theta  - 5.0, theta  + 5.0, nI)

    xf = [[0 for _ in range(nJ)] for _ in range(nI)]
    yf = [[0 for _ in range(nJ)] for _ in range(nI)]
    cf = [[0 for _ in range(nJ)] for _ in range(nI)]
    for i in range(nI):
        for j in range(nJ):
            print(i, j)
            xf[i][j] = vX[i]
            yf[i][j] = vY[j]
            braidModel = braid.braid(wAxial, hAxial, vX[i], lAxial, wBias, hBias, dExp, vGap, vY[j], 50, 50, name)
            braidModel.generate(braidType, nPair=1, bSurfOut=False, bGenVol=False)
            cf[i][j] = braidModel.coverFactor

    pickle.dump(xf, open("xf.out", 'wb'))
    pickle.dump(yf, open("yf.out", 'wb'))
    pickle.dump(cf, open("cf.out", 'wb'))

def generateRandom(nParam, sAxial, theta, wAxial, hAxial, wBias, hBias):
    if(nParam==2):
        sAxial += 10.0 * (numpy.random.rand() - 0.5)
        theta  += 10.0 * (numpy.random.rand() - 0.5)

    elif(nParam==4):
        sAxial += 10.0 * (numpy.random.rand() - 0.5)
        theta  += 10.0 * (numpy.random.rand() - 0.5)
        hAxial += 0.50 * (numpy.random.rand() - 0.5)
        hBias  += 0.50 * (numpy.random.rand() - 0.5)

    elif(nParam==6):
        sAxial += 10.0 * (numpy.random.rand() - 0.5)
        theta  += 10.0 * (numpy.random.rand() - 0.5)
        wAxial += 2.00 * (numpy.random.rand() - 0.5)
        hAxial += 0.50 * (numpy.random.rand() - 0.5)
        wBias  += 2.00 * (numpy.random.rand() - 0.5)
        hBias  += 0.50 * (numpy.random.rand() - 0.5)

    return sAxial, theta, wAxial, hAxial, wBias, hBias