import math

def chamis(Vf, rhoF, rhoM, ELF, ETF, EM, GLTF, GTTF, GM, NuLTF, NuM, exportName=None):
    rho      = Vf * rhoF + (1 - Vf) * rhoM          # [kg/m^3] Density  
    E_L      = Vf * ELF  + (1 - Vf) * EM            # [Pa] Young's modulus LONGITUDINAL
    E_T      = EM/(1 - math.sqrt(Vf)*(1 - EM/ETF))  # [Pa] Young's modulus TRANSVERSE
    G_LT     = GM/(1 - math.sqrt(Vf)*(1 - GM/GLTF)) # [Pa]
    G_TT     = GM/(1 - math.sqrt(Vf)*(1 - GM/GTTF)) # [Pa]
    nu_TT    = E_T/(2 * G_TT) - 1                   # [-] Poisson's ratio
    nu_LT_M  = Vf * NuLTF + (1 - Vf) * NuM          # [-] Major LT Poisson's ratio
    nu_LT_m  = nu_LT_M*(E_T / E_L)                  # [-] Minor LT Poisson's ratio

    chamisDict = {"rho": rho, "E_L": E_L, "E_T": E_T, "G_LT": G_LT,
                "G_TT": G_TT, "nu_TT": nu_TT, "nu_LT_major": nu_LT_M, "nu_LT_m": nu_LT_m}

    return rho, E_L, E_T, G_LT, G_TT, nu_TT, nu_LT_M, nu_LT_m, chamisDict
