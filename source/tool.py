import gmsh
import numpy
import scipy
import matplotlib.pyplot as pl
import bezrClass as bzr
import pntSetClass as pSet
from scipy.spatial import Delaunay   as dln
from scipy.spatial import ConvexHull as cvh

def extrap1(x, y, x0):
    xTmp = []
    yTmp = []
    for i in range(len(x)):
        if(not numpy.isnan(x[i]) and not numpy.isnan(y[i])):
            xTmp.append(x[i])
            yTmp.append(y[i])
    x = xTmp.copy()
    y = yTmp.copy()

    if(x[0] > x[1]):
        x.reverse()
        y.reverse()

    xMin = min(x)
    xMax = max(x)

    nX = len(x)
    nX0 = len(x0)

    y0 = list(numpy.interp(x0, x, y))

    for i in range(nX0):
        if(x0[i] < xMin - 1e-10):
            y0[i] = (x0[i] - x[0])*(y[1] - y[0])/(x[1] - x[0]) + y[0]
        if(x0[i] > xMax + 1e-10):
            y0[i] = (x0[i] - x[nX-2])*(y[nX-1] - y[nX-2])/(x[nX-1] - x[nX-2]) + y[nX-2]

    return y0

def getZeros(x, y):
    nX = len(x)
    nY = len(y)

    x0 = []
    if(nX == nY and nX >= 2):
        for i in range(nX - 1):
            xA = x[i]
            xB = x[i + 1]
            yA = y[i]
            yB = y[i + 1]

            if(yA == 0 and yB != 0):
                x0.append(xA)
            elif(yA != 0 and yB == 0):
                x0.append(xB)
            elif((yA < 0 and yB > 0) or (yA > 0 and yB < 0)):
                xC = xA - yA * (xB - xA) / (yB - yA)
                x0.append(xC)

    return x0

def rotateList(lst, n=1):
    return lst[n:] + lst[:n]

def findIndices(lst, value):
    vMax = max(lst)
    vMin = min(lst)

    dTol = (vMax - vMin) * 1e-6

    iIdx = []
    for i in range(len(lst)):
        if(abs(lst[i] - value) < dTol):
            iIdx.append(i)

    return iIdx

def flatCos(theta, b):
    c = numpy.cos(theta)
    return numpy.sqrt((1+b)/(1 + (b*c)**2))*c

def getDerivative(x, y):
    nX = len(x)

    dX = [x[i+1] - x[i] for i in range(nX-1)]
    dY = [y[i+1] - y[i] for i in range(nX-1)]
    xM = [(x[i] + x[i+1]) / 2 for i in range(nX-1)]

    dM = [dY[i]/dX[i] for i in range(nX-1)]
    
    if(nX > 2):
        return extrap1(xM, dM, x)
    else:
        return [dM, dM]

def getCurveLength(x, y, z):
    nX = len(x)

    dX = [x[i+1] - x[i] for i in range(nX-1)]
    dY = [y[i+1] - y[i] for i in range(nX-1)]
    dZ = [z[i+1] - z[i] for i in range(nX-1)]

    L = 0.0
    for i in range(nX-1):
        L += numpy.sqrt(dX**2 + dY**2 + dZ**2)

    return L

def solveSVD(A,b):
    # Compute svd of A
    # U, s, Vh = numpy.linalg.svd(A)
    U, s, Vh = scipy.linalg.svd(A, lapack_driver='gesvd')

    # U diag(s) Vh x = b <=> diag(s) Vh x = U.T b = c
    c = numpy.dot(U.T, b)

    # diag(s) Vh x = c <=> Vh x = diag(1/s) c = w (trivial inversion of a diagonal matrix)
    w = numpy.dot(numpy.diag(1 / s), c)

    # Vh x = w <=> x = Vh.H w (where .H stands for hermitian = conjugate transpose)
    x = numpy.dot(Vh.conj().T, w)

    return x

def RBF(x0, y0, z0, x, y, e = 0.01):
    nX0 = len(x0)
    nY0 = len(y0)
    nZ0 = len(z0)

    nX = len(x)
    nY = len(y)

    if(nX0 == nY0 and nX0 == nZ0 and nX == nY):
        A = numpy.zeros([nX0, nX0])
        b = numpy.zeros(nX0)

        for i in range(nX0):
            for j in range(nX0):
                dX = x0[i] - x0[j]
                dY = y0[i] - y0[j]
                r  = numpy.sqrt(dX ** 2 + dY ** 2)
                A[i][j] = numpy.exp(-(e * r) ** 2)
            b[i] = z0[i]

        w = numpy.linalg.solve(A, b)

        z = [0 for _ in range(nX)]
        for i in range(nX):
            for j in range(nX0):
                dX = x0[j] - x[i]
                dY = y0[j] - y[i]
                r  = numpy.sqrt(dX ** 2 + dY ** 2)
                z[i] += w[j] * numpy.exp(-(e * r) ** 2)
    return w, z

def smoothNodes(x0, y0, nIterMax=10, dUpdate=1.0, dlnTri=None):
    nP = len(x0)

    #
    iCrn = list(cvh([[x0[i], y0[i]] for i in range(nP)]).vertices)
    iCrn.append(iCrn[0])

    iNodeBorder=[]
    for j in range(len(iCrn) - 1):
        i0 = iCrn[j]
        i1 = iCrn[j+1]
        for i in range(nP):
            p1 = numpy.array([x0[i0], y0[i0]])
            p2 = numpy.array([x0[i1], y0[i1]])
            p3 = numpy.array([x0[i], y0[i]])
            d = numpy.cross(p2-p1,p3-p1)/numpy.linalg.norm(p2-p1)
            if(d < 1e-6):
                iNodeBorder.append(i)

    #
    dV = 1.0
    nT = 0
    while(dV>1e-6 and nT < nIterMax):
        nT += 1

        if(dlnTri):
            dlnTri  = list(dln([[x0[i],  y0[i]]    for i in range(nP)]).simplices)
            dlnTri = [list(triLcl) for triLcl in dlnTri]

        iNodeToTri = {}
        for i in range(nP):
            iNodeToTri[i] = []

        for tri in dlnTri:
            for iA in tri:
                for iB in tri:
                    if(iB not in iNodeToTri[iA] and iA != iB):
                        iNodeToTri[iA].append(iB)

        #
        x = [0 for _ in range(nP)]
        y = [0 for _ in range(nP)]
        for i in range(nP):
            if(i not in iNodeBorder):
                n = 0
                sumX = 0
                sumY = 0
                for j in iNodeToTri[i]:
                    sumX += dUpdate * (x0[j] - x0[i])
                    sumY += dUpdate * (y0[j] - y0[i])
                    n    += 1
                x[i] = x0[i] + sumX / n
                y[i] = y0[i] + sumY / n
            else:
                x[i] = x0[i]
                y[i] = y0[i]

        #
        x0 = x.copy()
        y0 = y.copy()

        # import matplotlib.pyplot as pl
        # pl.figure()
        # tri0 = dln([[x0[i], y0[i]] for i in range(nP)])
        # pl.plot(x0, y0, '.')
        # pl.triplot(x0, y0, tri0.simplices)
        # ax = pl.gca()
        # ax.set_aspect('equal')
        # pl.savefig('dummy'+str(nT)+'.svg')
        # pl.close()
    return x0, y0

def getCubicCoeff(x0, y0):

    A = numpy.zeros([4, 4])
    b = numpy.zeros(4)

    for i in range(4):
        for j in range(4):
            A[i][j] = x0[i]**(3-j)
        b[i] = y0[i]

    c = numpy.linalg.solve(A, b)

    return c

def getLineIntersection(xA, xB, xC, xD, yA, yB, yC, yD):
    xE = ((xA * yB - yA * xB) * (xC - xD) - (xA - xB) * (xC * yD - yC * xD)) / ((xA - xB) * (yC - yD) - (yA - yB) * (xC-xD))
    yE = ((xA * yB - yA * xB) * (yC - yD) - (yA - yB) * (xC * yD - yC * xD)) / ((xA - xB) * (yC - yD) - (yA - yB) * (xC-xD))

    return xE, yE

def getLinePerpendicular(xA, xB, yA, yB):
    dXRot = yA - yB
    dYRot = xB - xA

    xP, yP = [dXRot,dYRot]
    
    return xP, yP
