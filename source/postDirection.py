import argparse
import csv

def readAndExport2VTK(prefix, name):
    #---------------------------------------------------------------------
    pnt  = []
    dir = []
    with open('dir/dir' + prefix + name + '.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        line_count = 0
        for row in csv_reader:
            if line_count != 0:
                pnt.append([row[0], row[1], row[2]])
                dir.append([row[3], row[4], row[5]])
            line_count += 1

    fFile = open("dir/pnt" + prefix + name + ".vtk", "w")

    fFile.write("# vtk DataFile Version 2.0\n")
    fFile.write("The direction test\n")
    fFile.write("ASCII\n")
    fFile.write("DATASET POLYDATA\n")
    fFile.write("POINTS " + str(len(pnt)) + " float\n")
    for x, y, z in pnt:
        fFile.write(str(x) + " " + str(y) + " " + str(z) + "\n")

    fFile.write("\n")
    fFile.write("POINT_DATA "+ str(len(pnt)) + "\n")
    fFile.write("VECTORS Direction float\n")
    for tx, ty, tz in dir:
        fFile.write(str(tx) + " " + str(ty) + " " + str(tz) + "\n")

    fFile.close()

parser = argparse.ArgumentParser(description='Creates an RUC and calculates the RVE')
parser.add_argument('--name', type=str, default='0', help='filename modifier')
args = parser.parse_args()

readAndExport2VTK("A", args.name)
readAndExport2VTK("R", args.name)
readAndExport2VTK("L", args.name)