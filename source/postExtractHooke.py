import csv
import argparse
import numpy

parser = argparse.ArgumentParser(description='Creates an RUC and calculates the RVE')
parser.add_argument('--name',      type=str, default=None, help='filename modifier')

args = parser.parse_args()

if(args.name != None):
    filename = "res/model" + str(args.name) + "_E_0_GP_0_tangent.csv"

else:
    filename = "E_0_GP_0_tangent.csv"

try:
    with open(filename, 'r') as data:
        for line in csv.DictReader(data, delimiter=';'):
            cValues = line
    
    cNames = {}
    cNames["Comp.0000"] = [0, 0]
    cNames["Comp.0011"] = [0, 1]
    cNames["Comp.0022"] = [0, 2]
    cNames["Comp.0012"] = [0, 3]
    cNames["Comp.0002"] = [0, 4]
    cNames["Comp.0001"] = [0, 5]
    cNames["Comp.1111"] = [1, 1]
    cNames["Comp.1122"] = [1, 2]
    cNames["Comp.1112"] = [1, 3]
    cNames["Comp.1102"] = [1, 4]
    cNames["Comp.1101"] = [1, 5]
    cNames["Comp.2222"] = [2, 2]
    cNames["Comp.2212"] = [2, 3]
    cNames["Comp.2202"] = [2, 4]
    cNames["Comp.2201"] = [2, 5]
    cNames["Comp.1212"] = [3, 3]
    cNames["Comp.1202"] = [3, 4]
    cNames["Comp.1201"] = [3, 5]
    cNames["Comp.0202"] = [4, 4]
    cNames["Comp.0201"] = [4, 5]
    cNames["Comp.0101"] = [5, 5]

    C = numpy.zeros([6, 6])

    for key in cNames.keys():
        i, j = cNames[key]
        C[i][j] = cValues[key]
        C[j][i] = cValues[key]
    D = numpy.linalg.inv(C)
    
    data = {}
    data["E_x"]   = 1 / D[0][0]
    data["E_y"]   = 1 / D[1][1]
    data["E_z"]   = 1 / D[2][2]
    data["Nu_yx"] = - D[0][1] * data["E_y"]
    data["Nu_zx"] = - D[0][2] * data["E_z"]
    data["Nu_zy"] = - D[1][2] * data["E_z"]

    for key in data.keys():
        print(key, ":", data[key])

except:
    print("Error: no name provided")
