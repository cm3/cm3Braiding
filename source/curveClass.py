import numpy

class curve:
    def __init__(self):
        self.xB = []
        self.yB = []

    def setXPoints(self, xB):
        self.xB = xB.copy()
        self.nX = len(xB)

    def setYPoints(self, yB):
        self.yB = yB.copy()
        self.nY = len(yB)

    def setPoints(self, xB, yB):
        self.setXPoints(xB)
        self.setYPoints(yB)

    def getDerivative(self, xB, yB):
        #
        nX = len(xB)
        nY = len(yB)

        mB = []
        if(nX == nY and nX >= 2):
            xC = []
            yC = []
            mC = []
            for i in range(nX - 1):
                xC.append((xB[i] + xB[i + 1]) / 2.0)
                yC.append((yB[i] + yB[i + 1]) / 2.0)
                mC.append((yB[i] - yB[i + 1]) / (xB[i] - xB[i + 1]))

            mB = numpy.interp(xB, xC, mC)

            #
            mB[0] = (yB[1] - yB[0]) / (xB[1] - xB[0])
            mB[nX-1] = (yB[nX-1] - yB[nX-2]) / (xB[nX-1] - xB[nX-2])

        return mB

    def findTangency(self, x0, y0):
        nX = self.nX
        nY = self.nY

        xB = self.xB
        yB = self.yB

        dYB = self.getDerivative(xB, yB)

        xTan = []
        if(nX == nY and nX >= 2):
            dV = 1.0
            dH = 1e-6

            xM = (min(xB) + max(xB)) / 2
            if(x0 > xM):
                xT = 0.9*xM + 0.1*max(xB)
            elif(x0 < xM):
                xT = 0.9*xM + 0.1*min(xB)

            nIter = 0
            while(dV > 1e-10 and nIter < 100):
                nIter += 1
                dY0 = self.extrap1(xB, dYB, [xT - dH])
                dY1 = self.extrap1(xB, dYB, [xT + dH])

                yT = self.extrap1(xB, yB,  [xT])[0]
                dYT = self.extrap1(xB, dYB, [xT])[0]
                d2YT = (dY1[0] - dY0[0])/(2*dH)

                F = dYT - (yT - y0) / (xT - x0)
                J = d2YT - dYT / (xT - x0)

                dN = F/J

                dV = abs(F)

                xT -= dN

                if(xT < min(xB)):
                    nIter = 100
                    xT = xB[0]
                    yT = yB[0]
                elif(xT > max(xB)):
                    nIter = 100
                    xT = xB[nX - 1]
                    yT = yB[nX - 1]

            xTan = xT
            yTan = yT

        return xTan, yTan

    #
    def getZeros(self, x, y):
        nX = len(x)
        nY = len(y)

        x0 = []
        if(nX == nY and nX >= 2):
            for i in range(nX - 1):
                xA = x[i]
                xB = x[i + 1]
                yA = y[i]
                yB = y[i + 1]

                if(yA == 0 and yB != 0):
                    x0.append(xA)
                elif(yA != 0 and yB == 0):
                    x0.append(xB)
                elif((yA < 0 and yB > 0) or (yA > 0 and yB < 0)):
                    xC = xA - yA * (xB - xA) / (yB - yA)
                    x0.append(xC)

        return x0

    def extrap1(self, x, y, x0):
        xTmp = []
        yTmp = []
        for i in range(len(x)):
            if(not numpy.isnan(x[i]) and not numpy.isnan(y[i])):
                xTmp.append(x[i])
                yTmp.append(y[i])
        x = xTmp.copy()
        y = yTmp.copy()

        if(x[0] > x[1]):
            x.reverse()
            y.reverse()

        xMin = min(x)
        xMax = max(x)

        nX  = len(x)
        nX0 = len(x0)

        y0 = list(numpy.interp(x0, x, y))

        for i in range(nX0):
            if(x0[i] < xMin - 1e-10):
                y0[i] = (x0[i] - x[0])*(y[1] - y[0])/(x[1] - x[0]) + y[0]
            if(x0[i] > xMax + 1e-10):
                y0[i] = (x0[i] - x[nX-2])*(y[nX-1] - y[nX-2])/(x[nX-1] - x[nX-2]) + y[nX-2]

        return y0

