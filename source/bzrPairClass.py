import numpy
import tool
import sys

import patchClass as ptch
import pointClass as pnt

from scipy.spatial import Delaunay as dln
from scipy.spatial import ConvexHull as cvh

from shapely.geometry import Point, Polygon

class bezierPair:
    def __init__(self, bzrA = None, bzrB = None, sctn = None, vGap = 0) -> None:
        self.bzrA = bzrA
        self.bzrB = bzrB
        self.sctn = sctn
        self.vGap = vGap

        self.ptch = self.__getIntersectionPatch()

        #
        self.pntn  = []
        self.tngt  = []

    #
    def setPenetrationPoints(self, xTrim, dL=0.1):
        self.pntn = []
        dX = self.ptch.getDX() / 2
        nP = max([2, int(numpy.ceil(dX / dL))])
        pntGblInt = self.__getIntProjZ(self.ptch, nP=nP)

        self.uA = []
        self.vA = []
        self.uB = []
        self.vB = []

        #
        xPntn = []
        yPntn = []
        nPntn = 0
        for x, y in pntGblInt:
            if(x > xTrim - 1e-6):
                nPntn += 1
                xPntn.append(x)
                yPntn.append(y)
                self.addPenetrationPoint(x, y, 1.0, 1.0)

        #
        if(nPntn >= 4):
            triPntn = list(dln([[xPntn[i], yPntn[i]] for i in range(nPntn)]).simplices)
            self.triPntn = [list(triLcl) for triLcl in triPntn]
        else:
            self.triPntn = []

    def setTangentPoints(self, xTan):
        for x, y, z in xTan:
            self.tngt.append(pnt.point(x, y, z))

        self.uT = []
        self.vT = []
        for pntLcl in self.tngt:
            uTmp = self.bzrA.getLocalCoords(pntLcl.x, pntLcl.y)
            self.uT.append(uTmp[0])
            self.vT.append(uTmp[1])

    #
    def addPenetrationPoint(self, x, y, L = 1.0, S = 1.0):
        self.pntn.append(pnt.point(x, y, 0.0, L, S))

        uTmp = self.bzrA.getLocalCoords(x, y)
        self.uA.append(uTmp[0])
        self.vA.append(uTmp[1])

        uTmp = self.bzrB.getLocalCoords(x, y)
        self.uB.append(uTmp[0])
        self.vB.append(uTmp[1])

    def movePenetrationPoint(self, i, x, y):
        self.pntn[i].x = x
        self.pntn[i].y = y

        uTmp = self.bzrA.getLocalCoords(x, y)
        self.uA[i] = uTmp[0]
        self.vA[i] = uTmp[1]

        uTmp = self.bzrB.getLocalCoords(x, y)
        self.uB[i] = uTmp[0]
        self.vB[i] = uTmp[1]

    #
    def initPointSlackPntn(self, L, S):
        # Interpenetration points
        for pntLcl in self.pntn:
            pntLcl.L = L
            pntLcl.S = S

    def initPointSlackTngt(self, L, S):
        # Tangency points
        for pntLcl in self.tngt:
            pntLcl.L = L
            pntLcl.S = S

    #
    def minimizeArea(self, bVerbose, tol, alpha0, dAlpha):
        #
        nU = self.bzrA.nU
        nV = self.bzrA.nV

        nI = len(self.pntn)
        nJ = len(self.tngt)

        #
        iSnsA = []
        iSnsB = []
        for i in range(nU):
            for j in range(1, nV - 1):
                iSnsA.append([i, j])
                iSnsB.append([nU - i - 1, j])
        nK = len(iSnsA)

        #
        N      = 1.0
        nIter  = 0
        alpha  = alpha0
        if(bVerbose):
            print("Model Info:             Minimize Surface")

        while(N > tol and nIter < 1000 and N < 1e6):
            nIter += 1

            #
            ZA0 = [self.bzrA.zCtrl[iSnsA[k][0]][iSnsA[k][1]] for k in range(nK)]
            ZB0 = [self.bzrB.zCtrl[iSnsB[k][0]][iSnsB[k][1]] for k in range(nK)]
            LP0 = [self.pntn[i].L for i in range(nI)]
            SP0 = [self.pntn[i].S for i in range(nI)]
            LT0 = [self.tngt[l].L for l in range(nJ)]
            ST0 = [self.tngt[l].S for l in range(nJ)]

            #
            F, J = self.__getSystem(iSnsA, dH=1e-4)
            
            # S = numpy.linalg.solve(J, F)
            S = tool.solveSVD(J, F)

            N0 = N
            N  = numpy.linalg.norm(F)
            dN = N0 - N

            self.__updatePoints(iSnsA, iSnsB, ZA0, ZB0, LP0, SP0, LT0, ST0, S, alpha)

            #
            if(nIter >= 10):
                if(N < 0.1):
                    alpha *= dAlpha
                    alpha = min([0.5, alpha])
                else:
                    alpha = 0.1

            if(bVerbose):
                print("Model Info:                 Iter: %03i - Res: %0.5e - alpha: %0.5e" % (nIter, N, alpha), flush=True)

        return N, nIter

    #
    def __getIntersectionPatch(self):
        xA, yA, _ = self.bzrA.getCorners()
        xB, yB, _ = self.bzrB.getCorners()

        patchA = ptch.patch(xA, yA)
        patchB = ptch.patch(xB, yB)

        #
        pntA, tngA = self.__getPatchPointsAndDirection(patchA)
        pntB, tngB = self.__getPatchPointsAndDirection(patchB)

        #
        xInt, yInt = self.__getContourIntersection(pntA, tngA, pntB, tngB)

        #
        polyA = Polygon([[xA[i], yA[i]] for i in range(4)])
        polyB = Polygon([[xB[i], yB[i]] for i in range(4)])
        if(polyA.intersects(polyB)):
            while(xInt[0] != max(xInt)):
                xInt = tool.rotateList(xInt, -1)
                yInt = tool.rotateList(yInt, -1)
            patchInt0 = ptch.patch(xInt, yInt)

            i0 = numpy.argmax(xInt)
            i1 = numpy.argmax(yInt)
            i3 = numpy.argmin(yInt)

            if(xInt[i1] < min(xA)):
                x0 = xInt[i0]
                x1 = xInt[i1]
                x3 = xInt[i3]

                y0 = yInt[i0]
                y1 = yInt[i1]
                y3 = yInt[i3]

                xM = min(xA)
                yM = y0 + (xM - x0)*(y1 - y0)/(x1 - x0)
                xN = min(xA)
                yN = y0 + (xN - x0)*(y3 - y0)/(x3 - x0)

                u, _ = patchInt0.projectPoint(xM, yM)
                _, v = patchInt0.projectPoint(xN, yN)

                xO, yO, _ = patchInt0.getPoint(u, v)

                xInt[1] = xM
                xInt[2] = xO
                xInt[3] = xN

                yInt[1] = yM
                yInt[2] = yO
                yInt[3] = yN
        else:
            xInt = [0 for _ in range(4)]
            yInt = [0 for _ in range(4)]

            xInt[0] =  1e-3
            xInt[2] = -1e-3

            yInt[1] =  1e-3
            yInt[3] = -1e-3

        #
        patchCorr = ptch.patch(xInt, yInt)

        return patchCorr

    def __getIntProjZ(self, patch0, nP):
        xA, yA, _ = self.bzrA.getCorners()
        xB, yB, _ = self.bzrB.getCorners()

        # xMin = max([min(xA), min(xB)])
        # yMin = max([min(yA), min(yB)])
        # xMax = min([max(xA), max(xB)])
        # yMax = min([max(yA), max(yB)])

        #
        pntI0 = patch0.getTesellationLinear(nP)

        pntInt = []
        for xy in pntI0:
            #if(xy[0] >= xMin and xy[0] <= xMax and xy[1] >= yMin and xy[1] <= yMax):
                pntInt.append(xy)

        return pntInt

    def __getPatchPointsAndDirection(self, patchIn):
        #
        u = [-1,  1, 1, -1]
        v = [-1, -1, 1,  1]

        #
        pnt = []
        tng = []
        for i in range(4):
            xL, yL, _ = patchIn.getPoint(u[i], v[i])
            pnt.append([xL, yL])
            pnt.append([xL, yL])

            xL, yL, _ = patchIn.getDirU(u[i], v[i])
            tng.append([xL, yL])

            xL, yL, _ = patchIn.getDirV(u[i], v[i])
            tng.append([xL, yL])

        return pnt, tng

    def __getContourIntersection(self, pntA, tngA, pntB, tngB):
        pntOrg = []
        for i in range(8):
            for j in range(8):
                T = numpy.zeros([2, 2])
                X = numpy.zeros(2)

                if(abs(tngA[i][0]) > 1e-10 and abs(tngB[j][0]) > 1e-10):
                    T[0][0] =  tngA[i][0]
                    T[0][1] = -tngB[j][0]
                    T[1][0] =  tngA[i][1]
                    T[1][1] = -tngB[j][1]

                    if(abs(numpy.linalg.det(T)) > 1e-10):
                        X[0] = pntB[j][0] - pntA[i][0]
                        X[1] = pntB[j][1] - pntA[i][1]

                        L = numpy.linalg.solve(T, X)

                        xL = pntA[i][0] + L[0] * tngA[i][0]
                        yL = pntA[i][1] + L[0] * tngA[i][1]

                        if(len(pntOrg) == 0):
                            pntOrg.append([xL, yL])
                        else:
                            bExist = False
                            for k in range(len(pntOrg)):
                                dX = xL - pntOrg[k][0]
                                dY = yL - pntOrg[k][1]
                                d = numpy.sqrt(dX ** 2 + dY ** 2)
                                if(d<1e-10):
                                    bExist = True
                                    break

                            if(not bExist):
                                pntOrg.append([xL, yL])

        # Sorting
        xM = sum([xy[0] for xy in pntOrg])/len(pntOrg)
        yM = sum([xy[1] for xy in pntOrg])/len(pntOrg)
        dM = []

        for xy in pntOrg:
            xL = xy[0] - xM
            yL = xy[1] - yM
            dM.append(numpy.arctan2(yL, xL))
        iM = numpy.argsort(dM)

        xSorted = []
        ySorted = []
        for i in range(len(iM)):
            xSorted.append(pntOrg[iM[i]][0])
            ySorted.append(pntOrg[iM[i]][1])

        return xSorted, ySorted

    #
    def getSamplePoints(self, dL):
        #
        xCrn, yCrn = self.ptch.getCorners()[:2]

        i0 = numpy.argmax(xCrn)
        i1 = numpy.argmax(yCrn)
        i2 = numpy.argmin(yCrn)

        #
        L = numpy.sqrt((xCrn[i0] - xCrn[i1])**2 + (yCrn[i0] - yCrn[i1])**2)
        nX = max([int(numpy.ceil(L/dL + 1)), 10])

        #
        xUp = numpy.linspace(xCrn[i1], xCrn[i0], nX)
        yUp = numpy.linspace(yCrn[i1], yCrn[i0], nX)
        xDn = numpy.linspace(xCrn[i2], xCrn[i0], nX)
        yDn = numpy.linspace(yCrn[i2], yCrn[i0], nX)

        #
        uA = []
        vA = []
        uB = []
        vB = []
        for i in range(nX-1):
            dY = yUp[i] - yDn[i]
            nY = max([int(numpy.ceil(dY * nX / L)), 2])
            xRef = numpy.linspace(xDn[i], xUp[i], nY)
            yRef = numpy.linspace(yDn[i], yUp[i], nY)

            for j in range(nY):
                xLcl = xRef[j]
                yLcl = yRef[j]
                uTmpA, vTmpA = self.bzrA.getLocalCoords(xLcl, yLcl)
                uTmpB, vTmpB = self.bzrB.getLocalCoords(xLcl, yLcl)
                if(uTmpA >= 0.0 and uTmpA <= 1.0 and vTmpA >= 0.0 and vTmpA <= 1.0):
                    if(uTmpB >= 0.0 and uTmpB <= 1.0 and vTmpB >= 0.0 and vTmpB <= 1.0):
                        uA.append(uTmpA)
                        vA.append(vTmpA)
                        uB.append(uTmpB)
                        vB.append(vTmpB)

        return uA, vA, uB, vB

    def refinePenetrationPointsCtr(self, uSmplA, vSmplA, uSmplB, vSmplB, name=None, bSVG=False):
        #
        nSmpl = len(uSmplA)

        #
        xPntn = [p.x for p in self.pntn]
        yPntn = [p.y for p in self.pntn]
        lPntn = [p.L for p in self.pntn]
        sPntn = [p.S for p in self.pntn]
        tPntn = self.triPntn

        # Get the interpenetration value at sample points
        xSmpl = []
        ySmpl = []
        cSmpl = []
        for i in range(nSmpl):
            xLcl, yLcl = self.bzrA.getPoint3D(uSmplA[i], vSmplA[i])[:2]
            cLcl = self.__funPenetration(self.bzrA, self.bzrB, uSmplA[i], vSmplA[i], uSmplB[i], vSmplB[i])
            xSmpl.append(xLcl)
            ySmpl.append(yLcl)
            cSmpl.append(cLcl)
        
        xNew = []
        yNew = []
        nNew = 0
        for i in range(nSmpl):
            if(cSmpl[i] < 0.8 * min(cSmpl) and min(cSmpl) < 0.0):
                nNew += 1
                xNew.append(xSmpl[i])
                yNew.append(ySmpl[i])

        # Add list of valid simplices
        idxTri = []
        for i in range(nNew):
            pLcl = Point(xNew[i], yNew[i])
            for iA, iB, iC in tPntn:
                poly = Polygon([(xPntn[iA], yPntn[iA]), (xPntn[iB], yPntn[iB]), (xPntn[iC], yPntn[iC])])
                if(pLcl.within(poly) and [iA, iB, iC] not in idxTri):
                    idxTri.append([iA, iB, iC])

        # Divide simplices
        for iA, iB, iC in idxTri:
            xD = (xPntn[iA] + xPntn[iB] + xPntn[iC])/3.0
            yD = (yPntn[iA] + yPntn[iB] + yPntn[iC])/3.0
            lD = (lPntn[iA] + lPntn[iB] + lPntn[iC])/3.0
            sD = (sPntn[iA] + sPntn[iB] + sPntn[iC])/3.0

            pointD = pnt.point(xD, yD)

            if(pointD not in self.pntn):
                self.addPenetrationPoint(xD, yD, lD, sD)

            iD = self.pntn.index(pointD)

            self.triPntn.append([iA, iB, iD])
            self.triPntn.append([iB, iC, iD])
            self.triPntn.append([iC, iA, iD])
        
        # Remove simplices
        for iA, iB, iC in idxTri:
            self.triPntn.remove([iA, iB, iC])

        #
        if(bSVG):
            try:
                import matplotlib.pyplot as pyplot
                pyplot.figure()
                xCrnA, yCrnA = self.bzrA.getCorners()[:2]
                xCrnB, yCrnB = self.bzrB.getCorners()[:2]
                pyplot.triplot([p.x for p in self.pntn], [p.y for p in self.pntn], self.triPntn, lw=0.1)
                pyplot.plot(xCrnA + [xCrnA[0]], yCrnA + [yCrnA[0]], 'c', linewidth=0.2)
                pyplot.plot(xCrnB + [xCrnB[0]], yCrnB + [yCrnB[0]], 'g', linewidth=0.2)
                pyplot.plot([p.x for p in self.pntn], [p.y for p in self.pntn], 'g.', markersize=1)
                pyplot.xlabel('x [mm]')
                pyplot.ylabel('y [mm]')
                ax = pyplot.gca()
                ax.set_aspect('equal')
                pyplot.savefig("penetration_" + name + ".svg")
            except(NameError):
                print("Model Info: Error", NameError)
                print("Model Info: Continue without figure")

    def refinePenetrationPointsTri(self, uSmplA, vSmplA, uSmplB, vSmplB, name=None, bSVG=False):
        #
        nSmpl = len(uSmplA)

        #
        xPntn = [p.x for p in self.pntn]
        yPntn = [p.y for p in self.pntn]
        lPntn = [p.L for p in self.pntn]
        sPntn = [p.S for p in self.pntn]

        # Get the interpenetration value at sample points
        xSmpl = []
        ySmpl = []
        cSmpl = []
        for i in range(nSmpl):
            xLcl, yLcl = self.bzrA.getPoint3D(uSmplA[i], vSmplA[i])[:2]
            cLcl = self.__funPenetration(self.bzrA, self.bzrB, uSmplA[i], vSmplA[i], uSmplB[i], vSmplB[i])
            xSmpl.append(xLcl)
            ySmpl.append(yLcl)
            cSmpl.append(cLcl)
        
        xNew = []
        yNew = []
        nNew = 0
        for i in range(nSmpl):
            if(cSmpl[i] < 0.8 * min(cSmpl) and min(cSmpl) < 0.0):
                nNew += 1
                xNew.append(xSmpl[i])
                yNew.append(ySmpl[i])

        # Add list of valid simplices
        idxTri = []
        for i in range(nNew):
            pLcl = Point(xNew[i], yNew[i])
            for iA, iB, iC in self.triPntn:
                poly = Polygon([(xPntn[iA], yPntn[iA]), (xPntn[iB], yPntn[iB]), (xPntn[iC], yPntn[iC])])
                if(pLcl.within(poly) and [iA, iB, iC] not in idxTri):
                    idxTri.append([iA, iB, iC])

        # Divide simplices
        for iA, iB, iC in idxTri:
            xD = (xPntn[iA] + xPntn[iB])/2.0
            yD = (yPntn[iA] + yPntn[iB])/2.0
            lD = (lPntn[iA] + lPntn[iB])/2.0
            sD = (sPntn[iA] + sPntn[iB])/2.0

            xE = (xPntn[iB] + xPntn[iC])/2.0
            yE = (yPntn[iB] + yPntn[iC])/2.0
            lE = (lPntn[iB] + lPntn[iC])/2.0
            sE = (sPntn[iB] + sPntn[iC])/2.0

            xF = (xPntn[iA] + xPntn[iC])/2.0
            yF = (yPntn[iA] + yPntn[iC])/2.0
            lF = (lPntn[iA] + lPntn[iC])/2.0
            sF = (sPntn[iA] + sPntn[iC])/2.0

            pointD = pnt.point(xD, yD)
            pointE = pnt.point(xE, yE)
            pointF = pnt.point(xF, yF)

            if(pointD not in self.pntn):
                self.addPenetrationPoint(xD, yD, lD, sD)

            if(pointE not in self.pntn):
                self.addPenetrationPoint(xE, yE, lE, sE)

            if(pointF not in self.pntn):
                self.addPenetrationPoint(xF, yF, lF, sF)

            iD = self.pntn.index(pointD)
            iE = self.pntn.index(pointE)
            iF = self.pntn.index(pointF)

            self.triPntn.append([iA, iD, iF])
            self.triPntn.append([iB, iE, iD])
            self.triPntn.append([iC, iF, iE])
            self.triPntn.append([iD, iE, iF])
        
        # Remove simplices
        for iA, iB, iC in idxTri:
            self.triPntn.remove([iA, iB, iC])

        #
        if(bSVG):
            try:
                import matplotlib.pyplot as pyplot
                pyplot.figure()
                xCrnA, yCrnA = self.bzrA.getCorners()[:2]
                xCrnB, yCrnB = self.bzrB.getCorners()[:2]
                pyplot.triplot([p.x for p in self.pntn], [p.y for p in self.pntn], self.triPntn, lw=0.1)
                pyplot.plot(xCrnA + [xCrnA[0]], yCrnA + [yCrnA[0]], 'c', linewidth=0.5)
                pyplot.plot(xCrnB + [xCrnB[0]], yCrnB + [yCrnB[0]], 'g', linewidth=0.5)
                pyplot.plot([p.x for p in self.pntn], [p.y for p in self.pntn], 'g.', markersize=0.5)
                pyplot.xlabel('x [mm]')
                pyplot.ylabel('y [mm]')
                ax = pyplot.gca()
                ax.set_aspect('equal')
                pyplot.savefig("penetration_" + name + ".svg")
            except(NameError):
                print("Model Info: Error", NameError)
                print("Model Info: Continue without figure")

    def getPenetration(self, uA, vA, uB, vB):
        return self.__funPenetration(self.bzrA, self.bzrB, uA, vA, uB, vB)

    #
    def __updatePoints(self, iSnsA, iSnsB, ZA0, ZB0, LP0, SP0, LT0, ST0, J, eta):
        nX = len(iSnsA)
        nI = len(self.pntn)
        nJ = len(self.tngt)

        for x in range(nX):
            self.bzrA.zCtrl[iSnsA[x][0]][iSnsA[x][1]] = ZA0[x] - eta * J[x]
            self.bzrB.zCtrl[iSnsB[x][0]][iSnsB[x][1]] = ZB0[x] + eta * J[x]
        for i in range(nI):
            self.pntn[i].L = LP0[i] - eta * J[i + nX]
            self.pntn[i].S = SP0[i] - eta * J[i + nX + nI]
        for j in range(nJ):
            self.tngt[j].L = LT0[j] - eta * J[j + nX + 2 * nI]
            self.tngt[j].S = ST0[j] - eta * J[j + nX + 2 * nI + nJ]

    #
    def __getSystem(self, iSns, dH=1e-6):
        xPnt = self.pntn
        xTan = self.tngt

        nX = len(iSns)
        nI = len(xPnt)
        nJ = len(xTan)

        def getG():
            nI = len(self.pntn)
            C = [0.0 for _ in range(nI)]
            for i in range(nI):
                C[i] = self.__funPenetration(self.bzrA, self.bzrB,   self.uA[i],   self.vA[i],   self.uB[i],   self.vB[i])
            return C

        def getH():
            nJ = len(self.tngt)
            C = [0.0 for _ in range(nJ)]
            for j in range(nJ):
                C[j] = self.bzrA.getPoint3D(  self.uT[j],   self.vT[j])[2] - self.tngt[j].z
            return C

        def getDGDX(i0, j0):
            nI = len(self.pntn)
            C = [0.0 for _ in range(nI)]
            for i in range(nI):
                C[i] += self.__derPenetration(self.bzrA, self.bzrB,   self.uA[i],   self.vA[i],   self.uB[i],   self.vB[i], i0, j0)
                C[i] -= self.__derPenetration(self.bzrA, self.bzrB, 1-self.uA[i], 1-self.vA[i], 1-self.uB[i], 1-self.vB[i], i0, j0)
            return C

        def getDHDX(i0, j0):
            nJ = len(self.tngt)
            C = [0.0 for _ in range(nJ)]
            for j in range(nJ):
                C[j] += self.bzrA.dSdP(  self.uT[j],   self.vT[j], i0, j0)[2]
                C[j] -= self.bzrA.dSdP(1-self.uT[j], 1-self.vT[j], i0, j0)[2]
            return C

        vSize = [nX, nI, nI, nJ, nJ]

        #
        F = [numpy.zeros(nF) for nF in vSize]

        G = getG()
        H = getH()
        lmb = [pLcl.L for pLcl in self.pntn]
        tht = [pLcl.L for pLcl in self.tngt]
        S   = [pLcl.S for pLcl in self.pntn]
        T   = [pLcl.S for pLcl in self.tngt]

        for x in range(nX):
            i0, j0 = iSns[x]
            dGdX = getDGDX(i0, j0)
            dHdX = getDHDX(i0, j0)
            dDdP = self.bzrA.dDdP(i0, j0)[2]
            F[0][x]  = dDdP
            F[0][x] -= sum([lmb[i] * dGdX[i] for i in range(nI)])
            F[0][x] -= sum([tht[j] * dHdX[j] for j in range(nJ)])

        for i in range(nI):
            F[1][i] = - (G[i] - S[i]**2)
            F[2][i] = 2 * lmb[i] * S[i]

        for j in range(nJ):
            F[3][j] = - (H[j] - T[j]**2)
            F[4][j] = 2 * tht[j] * T[j]

        #
        J = [[numpy.zeros([nG, nF]) for nF in vSize] for nG in vSize]

        for y in range(nX):
            i1, j1 = iSns[y]
            self.bzrA.zCtrl[i1][j1] += dH

            for x in range(nX):
                i0, j0 = iSns[x]
                dGdX = getDGDX(i0, j0)
                dHdX = getDHDX(i0, j0)
                dDdP = self.bzrA.dDdP(i0, j0)[2]
                J[0][0][x][y]  = dDdP
                J[0][0][x][y] -= sum([lmb[i] * dGdX[i] for i in range(nI)])
                J[0][0][x][y] -= sum([tht[j] * dHdX[j] for j in range(nJ)])
                J[0][0][x][y] -= F[0][x]
                J[0][0][x][y] /= dH
            self.bzrA.zCtrl[i1][j1] -= dH

        for x in range(nX):
            i0, j0 = iSns[x]
            dGdX = getDGDX(i0, j0)
            dHdX = getDHDX(i0, j0)

            for i in range(nI):
                J[1][0][i][x] = - dGdX[i]

            for j in range(nJ):
                J[3][0][j][x] = - dHdX[j]

        for i in range(nI):
            J[2][1][i][i] = 2 * S[i]
            J[2][2][i][i] = 2 * lmb[i]

        for j in range(nJ):
            J[4][3][j][j] = 2 * T[j]
            J[4][4][j][j] = 2 * tht[j]

        # Transpose
        J[0][1] = J[1][0].T
        J[1][2] = J[2][1].T
        J[0][3] = J[3][0].T
        J[3][4] = J[4][3].T

        #
        fCnt = numpy.concatenate

        FOut = fCnt((F[0], F[1], F[2], F[3], F[4]))

        J0 = fCnt((J[0][0], J[1][0], J[2][0], J[3][0], J[4][0]))
        J1 = fCnt((J[0][1], J[1][1], J[2][1], J[3][1], J[4][1]))
        J2 = fCnt((J[0][2], J[1][2], J[2][2], J[3][2], J[4][2]))
        J3 = fCnt((J[0][3], J[1][3], J[2][3], J[3][3], J[4][3]))
        J4 = fCnt((J[0][4], J[1][4], J[2][4], J[3][4], J[4][4]))
        JOut = fCnt((J0, J1, J2, J3, J4), axis=1)

        return FOut, JOut

    #
    def __funPenetration(self, srfA, srfB, uA, vA, uB, vB):
        _, _, zA = srfA.getPoint3D(uA, vA)
        _,  _, zB = srfB.getPoint3D(uB, vB)
        hA = self.sctn.getNrmDn(uA)
        hB = self.sctn.getNrmUp(uB)

        dZ = zA + hA - (zB + hB) - self.vGap

        return dZ

    def __derPenetration(self, srfA, srfB, uA, vA, uB, vB, i0, j0):
        nI = srfB.nU

        zA = srfA.dSdP(uA, vA,          i0, j0)[2]
        zB = srfB.dSdP(uB, vB, nI - 1 - i0, j0)[2]
        dZ = zA + zB

        return dZ

    def getIntersectionAreaLength(self):
        xCrnA = self.bzrA.getCorners()[0]
        xCrnP = self.ptch.getCorners()[0]
        return max(xCrnP) - min(xCrnA)

    #
    def export2VTK(self, name, bCtr=False):
        self.bzrA.export2VTK("SrfA" + name, bCtr=bCtr)
        self.bzrB.export2VTK("SrfB" + name, bCtr=bCtr)
