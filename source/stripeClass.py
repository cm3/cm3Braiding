import numpy
import tool
import gmsh

import patchClass as ptch

class stripe:
    def __init__(self, bzrList0=[]):
        self.lSurf = [bzr.copy() for bzr in bzrList0]
        self.Crn = []

    def __len__(self):
        return len(self.lSurf)

    def setSurfs(self, bzrList0):
        self.lSurf = [bzr.copy() for bzr in bzrList0]

    def move(self, dX=0, dY=0, dZ=0):
        for bzr in self.lSurf:
            bzr.move(dX=dX, dY=dY, dZ=dZ)

    def getCorners(self):
        xCtr0 = []
        yCtr0 = []
        for bzr in self.lSurf:
            xTmp, yTmp = bzr.getCorners()[:2]
            for x in xTmp:
                xCtr0.append(x)
            for y in yTmp:
                yCtr0.append(y)
        nC = len(xCtr0)

        bVal = [[1 for _ in range(nC)] for _ in range(nC)]

        for i in range(nC):
            bVal[i][i] = 0

        for i in range(nC):
            for j in range(i + 1, nC):
                dX = xCtr0[i] - xCtr0[j]
                dY = yCtr0[i] - yCtr0[j]
                dD = numpy.sqrt(dX ** 2 + dY ** 2)
                if(dD < 1e-12):
                    for k in range(nC):
                        bVal[i][k] = 0
                        bVal[k][i] = 0
                        bVal[j][k] = 0
                        bVal[k][j] = 0

        iCtr = []
        for i in range(nC):
            for j in range(i + 1, nC):
                if(bVal[i][j] == 1):
                    dX = xCtr0[i] - xCtr0[j]
                    dY = yCtr0[i] - yCtr0[j]
                    iCtr.append(i)
                    iCtr.append(j)
        iCtr = list(set(iCtr))
        xCtr = [xCtr0[i] for i in iCtr]
        yCtr = [yCtr0[i] for i in iCtr]
        nC = len(xCtr)

        xMin = min(xCtr)
        yMin = min(yCtr)

        xCtr = [xCtr[i] - xMin for i in range(nC)]
        yCtr = [yCtr[i] - yMin for i in range(nC)]
        dCtr = [numpy.sqrt(xCtr[i]**2 + yCtr[i]**2) for i in range(nC)]

        i0 = numpy.argmin(dCtr)
        iSrt = self.convexHull(xCtr, yCtr, i0)

        xCtr = [xCtr[i] + xMin for i in iSrt]
        yCtr = [yCtr[i] + yMin for i in iSrt]

        d01 = [xCtr[1] - xCtr[0], yCtr[1] - yCtr[0], 0]
        d12 = [xCtr[2] - xCtr[1], yCtr[2] - yCtr[1], 0]
        if(numpy.cross(d12, d01)[2] > 0):
            xCtr.reverse()
            yCtr.reverse()

        dL01 = numpy.sqrt((xCtr[1] - xCtr[0])**2 + (yCtr[1] - yCtr[0]) ** 2)
        dL12 = numpy.sqrt((xCtr[2] - xCtr[1])**2 + (yCtr[2] - yCtr[1]) ** 2)
        #print(dL01, dL12)
        while(dL01 > dL12 or xCtr[0] != min(xCtr)):
            xCtr = tool.rotateList(xCtr)
            yCtr = tool.rotateList(yCtr)

            dL01 = numpy.sqrt((xCtr[1] - xCtr[0])**2 + (yCtr[1] - yCtr[0]) ** 2)
            dL12 = numpy.sqrt((xCtr[2] - xCtr[1])**2 + (yCtr[2] - yCtr[1]) ** 2)
        #    print(dL01, dL12)

        return xCtr, yCtr

    def convexHull(self, x, y, i0):
        n = len(x)
        def orientation(p, q, r):
            val =  (q[1] - p[1]) * (r[0] - q[0]) - (q[0] - p[0]) * (r[1] - q[1])
            if(val == 0):
                pnt = 0
            elif(val > 0):
                pnt = 1
            else:
                pnt = 2
            return pnt

        hull = []
        p = i0
        q = 0
        while(True):
            hull.append(p)

            q = (p + 1) % n
            for i in range(n):
                if(orientation([x[p], y[p]], [x[i], y[i]], [x[q], y[q]]) == 2):
                    q = i

            p = q
            if(p == i0):
                break

        hull.reverse()

        return hull

    def isInside(self, x0, y0):
        bIsIn = False
        for srf in self.lSurf:
            if(srf.isInsideXY(x0, y0)):
                bIsIn = True
                break

        return bIsIn

    def getZ(self, x0, y0):
        z = 0
        for srf in self.lSurf:
            bInside = srf.isInsideXY(x0, y0)
            if(bInside):
                u, v = srf.getLocalCoords(x0, y0)
                z = srf.getPoint3D(u, v)[2]
                break

        return z

    def dSdV(self, x0, y0):
        dX = 0
        dY = 0
        dZ = 0
        for srf in self.lSurf:
            bInside = srf.isInsideXY(x0, y0)
            if(bInside):
                u, v = srf.getLocalCoords(x0, y0)
                dX, dY, dZ = srf.dSdV(u, v)
                dL = numpy.sqrt(dX ** 2 + dY ** 2 + dZ ** 2)
                dX /= dL
                dY /= dL
                dZ /= dL
                break
        
        return dX, dY, dZ
    
    def getLocalCoords(self, x, y):
        xCrn, yCrn = self.getCorners()
        patch = ptch.patch(xCrn, yCrn)
        return patch.projectPoint(x, y)
    
    def getXY(self, u, v):
        xCrn, yCrn = self.getCorners()
        patch = ptch.patch(xCrn, yCrn)
        return patch.getPoint(u, v)[:2]
    
    def getVertPlanes(self, dZ):
        x, y = self.getCorners()

        x += [x[0]]
        y += [y[0]]

        planes = []
        for i in range(4):
            points = []
            points.append(gmsh.model.occ.addPoint(x[i],   y[i],   -dZ))
            points.append(gmsh.model.occ.addPoint(x[i+1], y[i+1], -dZ))
            points.append(gmsh.model.occ.addPoint(x[i+1], y[i+1],  dZ))
            points.append(gmsh.model.occ.addPoint(x[i],   y[i],    dZ))
            points.append(points[0])

            curves = []
            for j in range(4):
                curves.append(gmsh.model.occ.addLine(points[j], points[j+1]))
            gmsh.model.occ.synchronize()
            
            curveLoop = gmsh.model.occ.addCurveLoop(curves)

            planes.append((2, gmsh.model.occ.addPlaneSurface([curveLoop])))
            gmsh.model.occ.synchronize()
        
        return planes
