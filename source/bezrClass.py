import numpy
import surfClass as surf
import curveClass as crv
import bilnClass as bln
# import matplotlib.pyplot as pyplot
import math
import tool

class bezr:
    def __init__(self):
        self.xCtrl = []
        self.yCtrl = []
        self.zCtrl = []

        self.nU = 0
        self.nV = 0

    def copy(self):
        bezrOut = bezr()

        bezrOut.nU = self.nU
        bezrOut.nV = self.nV

        bezrOut.dN = self.dN
        bezrOut.dS = self.dS
        bezrOut.dE = self.dE
        bezrOut.dW = self.dW

        if(self.nV == 0):
            for i in range(self.nU):
                bezrOut.xCtrl.append(0.0)
                bezrOut.yCtrl.append(0.0)
                bezrOut.zCtrl.append(0.0)
                bezrOut.wCtrl.append(0.0)

                bezrOut.xCtrl[i] = self.xCtrl[i]
                bezrOut.yCtrl[i] = self.yCtrl[i]
                bezrOut.zCtrl[i] = self.zCtrl[i]
        else:
            for i in range(self.nU):
                bezrOut.xCtrl.append([])
                bezrOut.yCtrl.append([])
                bezrOut.zCtrl.append([])

                for j in range(self.nV):
                    bezrOut.xCtrl[i].append(0.0)
                    bezrOut.yCtrl[i].append(0.0)
                    bezrOut.zCtrl[i].append(0.0)

                    bezrOut.xCtrl[i][j] = self.xCtrl[i][j]
                    bezrOut.yCtrl[i][j] = self.yCtrl[i][j]
                    bezrOut.zCtrl[i][j] = self.zCtrl[i][j]

        return bezrOut

    def setControlPoints3D(self, xCtrl, yCtrl, zCtrl):
        self.xCtrl = xCtrl
        self.yCtrl = yCtrl
        self.zCtrl = zCtrl

        self.nU = len(xCtrl)
        self.nV = len(xCtrl[0])

        self.dN = [1.0 for i in range(self.nU)]
        self.dS = [0.0 for i in range(self.nU)]
        self.dE = [1.0 for i in range(self.nV)]
        self.dW = [0.0 for i in range(self.nV)]

    def bernstein(self, i, m, u):
        if((1 - u) != 0 or u != 0):
            bst = math.comb(m, i) * numpy.power(u, i) * numpy.power(1 - u, m - i)
        else:
            bst = 0

        return bst

    # Points
    def getPoint2D(self, t, Px, Py, Pz):
        m = len(Px) - 1
        M = self.getM(m)

        t = numpy.array([[numpy.power(t, i) for i in range(m+1)]])

        mm = numpy.matmul

        x = mm(mm(t, M), Px)[0]
        y = mm(mm(t, M), Py)[0]
        z = mm(mm(t, M), Pz)[0]

        return x, y, z

    def getPoint3D(self, u0, v0):
        m = self.nU - 1
        n = self.nV - 1

        u = numpy.array([[numpy.power(u0, i) for i in range(m + 1)]])
        v = numpy.array([[numpy.power(v0, j) for j in range(n + 1)]]).transpose()

        M = self.getM(m)
        N = self.getM(n).transpose()

        mm = numpy.matmul

        Px, Py, Pz = self.getP()

        x = mm(mm(mm(mm(u, M), Px), N), v)[0][0]
        y = mm(mm(mm(mm(u, M), Py), N), v)[0][0]
        z = mm(mm(mm(mm(u, M), Pz), N), v)[0][0]

        return x, y, z

    def getM(self, m):
        nU = m + 1

        M  = numpy.zeros([nU, nU])
        for i in range(nU):
            for j in range(i + 1):
                mA = math.comb(m, j)
                mB = math.comb(m-j, i-j)

                if( (i+j) % 2 == 0):
                    mC =  1
                else:
                    mC = -1

                M[i][j] = mA * mB * mC

        return M

    def getN(self, n):
        nV = n + 1

        N  = numpy.zeros([nV, nV])
        for i in range(nV):
            for j in range(i + 1):
                nA = math.comb(n, j)
                nB = math.comb(n-j, i-j)

                if( (i+j) % 2 == 0):
                    nC =  1
                else:
                    nC = -1

                N[i][j] = nA * nB * nC

        return N

    def getP(self):
        nU = self.nU
        nV = self.nV

        Px = numpy.zeros([nU, nV])
        Py = numpy.zeros([nU, nV])
        Pz = numpy.zeros([nU, nV])

        for i in range(nU):
            for j in range(nV):
                Px[i][j] = self.xCtrl[i][j]
                Py[i][j] = self.yCtrl[i][j]
                Pz[i][j] = self.zCtrl[i][j]

        return Px, Py, Pz

    #
    def getSurf(self, nU=2, nV=2):
        uList = numpy.linspace(0, 1, nU)
        vList = numpy.linspace(0, 1, nV)

        x = []
        y = []
        z = []
        for i in range(len(uList)):
            x.append([])
            y.append([])
            z.append([])
            for j in range(len(vList)):
                xLcl, yLcl, zLcl = self.getPoint3D(uList[i], vList[j])
                x[i].append(xLcl)
                y[i].append(yLcl)
                z[i].append(zLcl)

        return x, y, z

    def dSdP(self, u, v, i0, j0):
        m = self.nU - 1
        n = self.nV - 1

        bValI = self.bernstein(i0, m, u)
        bValJ = self.bernstein(j0, n, v)

        dX = bValI * bValJ
        dY = bValI * bValJ
        dZ = bValI * bValJ

        return dX, dY, dZ

    def dSdU(self, u, v, bNorm=False):
        m = self.nU - 1
        n = self.nV - 1

        dXdU = 0
        dYdU = 0
        dZdU = 0
        for i in range(self.nU - 1):
            bValI = self.bernstein(i, m - 1, u)
            for j in range(self.nV):
                bValJ = self.bernstein(j, n, v)

                dXdU += bValI * bValJ * (self.xCtrl[i+1][j] - self.xCtrl[i][j])
                dYdU += bValI * bValJ * (self.yCtrl[i+1][j] - self.yCtrl[i][j])
                dZdU += bValI * bValJ * (self.zCtrl[i+1][j] - self.zCtrl[i][j])
        dXdU *= m
        dYdU *= m
        dZdU *= m

        if(bNorm):
            dL = numpy.sqrt(dXdV**2 + dYdV**2 + dZdV**2)
            if(dL > 1e-6):
                dXdV /= dL
                dYdV /= dL
                dZdV /= dL
            else:
                dXdU = 0
                dYdU = 0
                dZdU = 0

        return dXdU, dYdU, dZdU

    def dSdV(self, u, v, bNorm=False):
        m = self.nU - 1
        n = self.nV - 1

        dXdV = 0
        dYdV = 0
        dZdV = 0
        for i in range(self.nU):
            bValI = self.bernstein(i, m, u)
            for j in range(self.nV-1):
                bValJ = self.bernstein(j, n - 1, v)

                dXdV += bValI * bValJ * (self.xCtrl[i][j+1] - self.xCtrl[i][j])
                dYdV += bValI * bValJ * (self.yCtrl[i][j+1] - self.yCtrl[i][j])
                dZdV += bValI * bValJ * (self.zCtrl[i][j+1] - self.zCtrl[i][j])
        dXdV *= n
        dYdV *= n
        dZdV *= n

        if(bNorm):
            dL = numpy.sqrt(dXdV**2 + dYdV**2 + dZdV**2)
            if(dL > 1e-6):
                dXdV /= dL
                dYdV /= dL
                dZdV /= dL
            else:
                dXdV = 0
                dYdV = 0
                dZdV = 0

        return dXdV, dYdV, dZdV

    # Dirichlet
    def dDdP(self, i0, j0):
        n = self.nU - 1
        m = self.nV - 1

        dDdX  = 0
        dDdY  = 0
        dDdZ  = 0

        # Transversal term
        #if(i0 > 0 and i0 < n):
        #    dXTmp = 0
        #    dYTmp = 0
        #    dZTmp = 0
        #    for k in range(n):
        #        for l in range(m + 1):
        #            dXTmp += self.A(n, i0, k) * math.comb(m, l) / math.comb(2 * m, j0 + l) * (self.xCtrl[k+1][l] - self.xCtrl[k][l])
        #            dYTmp += self.A(n, i0, k) * math.comb(m, l) / math.comb(2 * m, j0 + l) * (self.yCtrl[k+1][l] - self.yCtrl[k][l])
        #            dZTmp += self.A(n, i0, k) * math.comb(m, l) / math.comb(2 * m, j0 + l) * (self.zCtrl[k+1][l] - self.zCtrl[k][l])
        #    dDdX += (n * n / (2 * (2 * n - 2) * m) * math.comb(n - 1, i0) * math.comb(m, j0)) * dXTmp
        #    dDdY += (n * n / (2 * (2 * n - 2) * m) * math.comb(n - 1, i0) * math.comb(m, j0)) * dYTmp
        #    dDdZ += (n * n / (2 * (2 * n - 2) * m) * math.comb(n - 1, i0) * math.comb(m, j0)) * dZTmp

        # Longitudinal term
        if(j0 > 0 and j0 < m):
            dXTmp = 0
            dYTmp = 0
            dZTmp = 0
            for k in range(n + 1):
                for l in range(m):
                    dXTmp += self.A(m, j0, l) * math.comb(n, k) / math.comb(2 * n, i0 + k) * (self.xCtrl[k][l+1] - self.xCtrl[k][l])
                    dYTmp += self.A(m, j0, l) * math.comb(n, k) / math.comb(2 * n, i0 + k) * (self.yCtrl[k][l+1] - self.yCtrl[k][l])
                    dZTmp += self.A(m, j0, l) * math.comb(n, k) / math.comb(2 * n, i0 + k) * (self.zCtrl[k][l+1] - self.zCtrl[k][l])
            dDdX += (m * m / (2 * (2 * m - 2) * n) * math.comb(n, i0) * math.comb(m - 1, j0)) * dXTmp
            dDdY += (m * m / (2 * (2 * m - 2) * n) * math.comb(n, i0) * math.comb(m - 1, j0)) * dYTmp
            dDdZ += (m * m / (2 * (2 * m - 2) * n) * math.comb(n, i0) * math.comb(m - 1, j0)) * dZTmp

        if(abs(dDdX) < 1e-16):
            dDdX = 0

        if(abs(dDdY) < 1e-16):
            dDdY = 0

        if(abs(dDdZ) < 1e-16):
            dDdZ = 0

        return dDdX, dDdZ, dDdZ

    def A(self, n, i, k):
        return (n*i - n*k - i) * math.comb(n-1, k) / (n - i) / (2 * n - 1 - i - k) / math.comb(2 * n - 2, i + k - 1)

    #
    def move(self, dX=0, dY=0, dZ=0):
        for u in range(self.nU):
            for v in range(self.nV):
                self.xCtrl[u][v] += dX
                self.yCtrl[u][v] += dY
                self.zCtrl[u][v] += dZ

    def scale(self, dX=1, dY=1, dZ=1):
        for u in range(self.nU):
            for v in range(self.nV):
                self.xCtrl[u][v] *= dX
                self.yCtrl[u][v] *= dY
                self.zCtrl[u][v] *= dZ

    #
    def fit2Surf(self, surfCtr, nU, nV, tol=1e-6, offTol=0.0):
        nI = surfCtr.nI
        nJ = surfCtr.nJ

        #
        iCtr = [[i, j] for i in range(nI) for j in range(nJ)]
        iSns = [[i, j] for i in range(nU) for j in range(nV)]

        nCtr = len(iCtr)
        nSns = len(iSns)

        xCrn, yCrn, zCrn = surfCtr.getCorners()

        u = [tool.flatCos(tht, 0) for tht in numpy.linspace(numpy.pi, 0, nU)]
        v = [tool.flatCos(tht, 0) for tht in numpy.linspace(numpy.pi, 0, nV)]
        # u = numpy.linspace(-1, 1, nU)
        # v = numpy.linspace(-1, 1, nV)

        xCtrl = [[0.0  for _ in range(nV)] for _ in range(nU)]
        yCtrl = [[0.0  for _ in range(nV)] for _ in range(nU)]
        zCtrl = [[0.0  for _ in range(nV)] for _ in range(nU)]
        for i in range(nU):
            for j in range(nV):
                N = bln.biln().N(u[i], v[j])

                xCtrl[i][j] = numpy.dot(xCrn, N)
                yCtrl[i][j] = numpy.dot(yCrn, N)

        self.setControlPoints3D(xCtrl, yCtrl, zCtrl)

        uMap, vMap = self.projectToXY(surfCtr.X, surfCtr.Y)

        dV = 1.0
        nT = 0
        dH = 1e-6
        while(dV > tol and nT < 10):
            nT += 1

            F = numpy.zeros( nCtr)
            J = numpy.zeros([nCtr, nSns])

            #
            for i in range(nCtr):
                iLcl = iCtr[i][0]
                jLcl = iCtr[i][1]
                uLcl = uMap[iLcl][jLcl]
                vLcl = vMap[iLcl][jLcl]
                F[i] = surfCtr.Z[iLcl][jLcl] - self.getPoint3D(uLcl, vLcl)[2]

            for j in range(nSns):
                self.zCtrl[iSns[j][0]][iSns[j][1]] += dH
                for i in range(nCtr):
                    iLcl = iCtr[i][0]
                    jLcl = iCtr[i][1]
                    uLcl = uMap[iLcl][jLcl]
                    vLcl = vMap[iLcl][jLcl]
                    J[i][j]  = surfCtr.Z[iLcl][jLcl] - self.getPoint3D(uLcl, vLcl)[2]
                    J[i][j] -= F[i]
                    J[i][j] /= dH
                self.zCtrl[iSns[j][0]][iSns[j][1]] -= dH

            dN = numpy.linalg.lstsq(J, F, rcond=None)[0]
            dV = numpy.sqrt(numpy.dot(dN, dN))

            for j in range(nSns):
                self.zCtrl[iSns[j][0]][iSns[j][1]] -= dN[j]

    def fitBorder2Bezier(self, sRef, border):
        nU = self.nU
        nV = self.nV

        if(border=="N"):
            iSns = [[i,           nV-1] for i in range(1, nU-1)]
            uBzr = [[i/(nU*nV-1), 1]    for i in range(nU*nV)]
            uCtr = [[i/(nU-1),    1]    for i in range(nU)]

            self.projCorner(sRef, 0, 1)
            self.projCorner(sRef, 1, 1)
        elif(border=="S"):
            iSns = [[i,           0]    for i in range(1, nU-1)]
            uBzr = [[i/(nU*nV-1), 0]    for i in range(nU*nV)]
            uCtr = [[i/(nU-1),    0]    for i in range(nU)]

            self.projCorner(sRef, 0, 0)
            self.projCorner(sRef, 1, 0)
        elif(border=="E"):
            iSns = [[nU-1, j]           for j in range(1, nV-1)]
            uBzr = [[1,    j/(nU*nV-1)] for j in range(nU*nV)]
            uCtr = [[1,    j/(nV-1)]    for j in range(nV)]

            self.projCorner(sRef, 1, 0)
            self.projCorner(sRef, 1, 1)
        elif(border=="W"):
            iSns = [[0, j]              for j in range(1, nV-1)]
            uBzr = [[0, j/(nU*nV-1)]    for j in range(nU*nV)]
            uCtr = [[0,    j/(nV-1)]    for j in range(nV)]

            self.projCorner(sRef, 0, 0)
            self.projCorner(sRef, 0, 1)

        nSns = len(iSns)

        xBzr = [self.getPoint3D(uBzr[i][0], uBzr[i][1])[0] for i in range(nU*nV)]
        yBzr = [self.getPoint3D(uBzr[i][0], uBzr[i][1])[1] for i in range(nU*nV)]

        uCtr = [sRef.getLocalCoords(xBzr[i], yBzr[i]) for i in range(nU*nV)]

        dV = 1.0
        nT = 0
        dH = 1e-6
        while(dV > 1e-6 and nT < 10):
            nT += 1

            F = numpy.zeros( nU*nV)
            J = numpy.zeros([nU*nV, nSns])

            for i in range(nU*nV):
                F[i] = sRef.getPoint3D(uCtr[i][0], uCtr[i][1])[2] - self.getPoint3D(uBzr[i][0], uBzr[i][1])[2]

            for j in range(nSns):
                self.zCtrl[iSns[j][0]][iSns[j][1]] += dH
                for i in range(nU*nV):
                    J[i][j]  = sRef.getPoint3D(uCtr[i][0], uCtr[i][1])[2] - self.getPoint3D(uBzr[i][0], uBzr[i][1])[2]
                    J[i][j] -= F[i]
                    J[i][j] /= dH
                self.zCtrl[iSns[j][0]][iSns[j][1]] -= dH

            dN = numpy.linalg.lstsq(J, F, rcond=None)[0]
            dV = numpy.sqrt(numpy.dot(dN, dN))

            for j in range(nSns):
                self.zCtrl[iSns[j][0]][iSns[j][1]] -= dN[j]

    def projCorner(self, sRef, u, v):
        if(u == 0):
            iU = 0
        elif(u == 1):
            iU = self.nU - 1

        if(v == 0):
            iV = 0
        elif(v == 1):
            iV = self.nV - 1

        xLcl, yLcl = self.getPoint3D(u, v)[:2]
        uLcl, vLcl = sRef.getLocalCoords(xLcl, yLcl)
        self.zCtrl[iU][iV] = sRef.getPoint3D(uLcl, vLcl)[2]

    #
    def integrateDirichlet(self, nMult = 5):
        xU, wU = numpy.polynomial.legendre.leggauss(nMult * self.nU)
        xV, wV = numpy.polynomial.legendre.leggauss(nMult * self.nV)

        D = 0
        for i in range(len(xU)):
            u = (xU[i] + 1.0) / 2.0
            for j in range(len(xV)):
                v = (xV[j] + 1.0) / 2.0
                dXdU, dYdU, dZdU = self.dSdU(u, v)
                dXdV, dYdV, dZdV = self.dSdV(u, v)
                D += wU[i] * wV[j] * (dXdU**2 + dYdU**2 + dZdU**2)
                D += wU[i] * wV[j] * (dXdV**2 + dYdV**2 + dZdV**2)

        D *= 0.5

        return D

    def solveDrchlt(self, bLng, bTrn, bPrint=False):
        if(bLng or bTrn):
            iSns = self.getSnsDrchlt(bLng=bLng, bTrn=bTrn)
            nS = len(iSns)

            dV = 1.0
            nT = 0
            while(dV > 1e-6 and nT < 10):
                nT += 1

                F = self.getResDrchlt(iSns)
                J = self.getJacDrchlt(iSns)

                dN = numpy.linalg.solve(J, F)

                for k in range(nS):
                    self.zCtrl[iSns[k][0]][iSns[k][1]] -= dN[k]

                dV = numpy.linalg.norm(F)

                if(bPrint):
                    print("    DBG: nT %02i - dV: %08e" % (nT, dV), flush=True)

    def getResDrchlt(self, iSns):
        # Lengths
        nS = len(iSns)

        #
        F = numpy.zeros(nS)

        #
        for i in range(nS):
            F[i] = self.dDdP(iSns[i][0], iSns[i][1])[2]

        return F

    def getJacDrchlt(self, iSns, dH=1e-6):
        nS = len(iSns)

        #
        F = self.getResDrchlt(iSns)
        J = numpy.zeros([nS, nS])

        # Minimize interpenetration
        for j in range(nS):
            #-------------------------------------------------------
            self.zCtrl[iSns[j][0]][iSns[j][1]] += dH
            for i in range(nS):
                J[i][j]  = self.dDdP(iSns[i][0], iSns[i][1])[2]
                J[i][j] -= F[i]
                J[i][j] /= dH
            self.zCtrl[iSns[j][0]][iSns[j][1]] -= dH

        return J

    def getSnsDrchlt(self, bLng, bTrn):
        nU = self.nU
        nV = self.nV

        if(bLng and bTrn):
            uSns = [[u, v] for u in range(1, nU-1) for v in range(1, nV - 1)]
        elif(not bLng and bTrn):
            uSns = [[u, v] for u in range(1, nU-1) for v in range(nV)]
        elif(bLng and not bTrn):
            uSns = [[u, v] for u in range(nU)      for v in range(1, nV - 1)]
        else:
            uSns = []

        return uSns

    #
    def getLocalCoords(self, x0, y0, u=0.5, v=0.5, tol=1e-8):
        dV = 1.0
        nT = 0
        dH = 1e-4
        while(dV > tol and nT < 100):
            nT += 1

            xA, yA, _ = self.getPoint3D(u,      v)
            # xB, yB, _ = self.getPoint3D(u + dH, v)
            # xC, yC, _ = self.getPoint3D(u,      v + dH)

            F = numpy.zeros(2, numpy.float64)
            J = numpy.zeros([2, 2], numpy.float64)

            F[0] = xA - x0
            F[1] = yA - y0

            dXdU, dYdU = self.dSdU(u, v)[:2]
            dXdV, dYdV = self.dSdV(u, v)[:2]

            J[0][0] = dXdU
            J[0][1] = dXdV
            J[1][0] = dYdU
            J[1][1] = dYdV

            dN = numpy.linalg.solve(J, F)

            dV = numpy.sqrt(numpy.dot(F, F))

            magN = numpy.sqrt(numpy.dot(dN, dN))
            if( magN > 0.1):
                dN[0] = 0.1 * dN[0] / magN
                dN[1] = 0.1 * dN[1] / magN

            u -= dN[0]
            v -= dN[1]

        return u, v

    def projectToXY(self, X, Y):
        #
        u = []
        v = []
        for i in range(len(X)):
            u.append([])
            v.append([])
            for j in range(len(X[i])):
                u[i].append(0.0)
                v[i].append(0.0)

        #
        Z = []
        nI = len(X)
        uTmp = 0.0
        for i in range(nI):
            vTmp = 0.0
            Z.append([])
            nJ = len(X[i])
            for j in range(len(X[i])):
                uTmp, vTmp = self.getLocalCoords(X[i][j], Y[i][j], uTmp, vTmp)

                u[i][j] = uTmp
                v[i][j] = vTmp

        return u, v

    #
    def getTangent2D(self, x0, y0, xCtr, yCtr):
        t = 0.5

        dV = 1.0
        nT = 0
        dT = 1e-6
        while(dV > 1e-10 and nT < 100):
            nT += 1

            xA, yA, _ = self.getPoint2D(t - dT, xCtr, yCtr, [0 for _ in range(len(xCtr))])
            xB, yB, _ = self.getPoint2D(t     , xCtr, yCtr, [0 for _ in range(len(xCtr))])
            xC, yC, _ = self.getPoint2D(t + dT, xCtr, yCtr, [0 for _ in range(len(xCtr))])

            dXdT   = (xC - xA) / (2 * dT)
            dYdT   = (yC - yA) / (2 * dT)
            d2XdT2 = (xC - 2 * xB + xA) / dT / dT
            d2YdT2 = (yC - 2 * yB + yA) / dT / dT

            mA = (yA - y0) / (xA - x0)
            mB = (yB - y0) / (xB - x0)
            mC = (yC - y0) / (xC - x0)

            F = dYdT / dXdT - mB
            J = (d2YdT2 * dXdT - dYdT * d2XdT2) / dXdT / dXdT - (mC - mA) / (2 * dT)

            dV = abs(F)

            t -= F/J

        return xB, yB, t

    #
    def export2VTK(self, fileMod, nU=100, nV=100, bCtr=False):
        if(self.nU != 0 and self.nV != 0):
            # Export Surface
            xTmp, yTmp, zTmp = self.getSurf(nU=nU, nV=nV)

            fFile = open(fileMod + ".vtk", "w")
            fFile.write("# vtk DataFile Version 2.0\n")
            fFile.write("The direction test\n")
            fFile.write("ASCII\n")
            fFile.write("DATASET POLYDATA\n\n")

            # Init index array
            iIdx = []
            nPnt = 0
            for i in range(nU):
                iIdxTmp = []
                for j in range(nV):
                    nPnt += 1
                    iIdxTmp.append(-1)
                iIdx.append(iIdxTmp)

            #
            fFile.write("POINTS " + str(nPnt) + " float\n")
            iPnt = -1
            for i in range(nU):
                for j in range(nV):
                    iPnt += 1
                    iIdx[i][j] = iPnt
                    xVal = xTmp[i][j]
                    yVal = yTmp[i][j]
                    zVal = zTmp[i][j]

                    if(abs(xVal) < 1e-10):
                        xVal = 0
                    if(abs(yVal) < 1e-10):
                        yVal = 0
                    if(abs(zVal) < 1e-10):
                        zVal = 0

                    fFile.write(str(xVal) + " " + str(yVal) + " " + str(zVal) + "\n")

            #
            nPoly = (nU - 1) * (nV - 1)
            fFile.write("\nPOLYGONS " + str(nPoly) + " " + str(nPoly*5) + "\n")

            for i in range(nU - 1):
                for j in range(nV - 1):
                    fFile.write("4 " + str(iIdx[i][j]) + " " + str(iIdx[i+1][j]) + " " + str(
                        iIdx[i+1][j+1]) + " " + str(iIdx[i][j+1]) + "\n")

#            fFile.write("\nPOINT_DATA " + str(nPnt) + "\n")
#            fFile.write("SCALARS Heights float 1\nLOOKUP_TABLE table\n")
#            for i in range(nU):
#                for j in range(nV):
#                    fFile.write(str(zTmp[i][j]) + "\n")
#
            # Export control points
            if(bCtr):
                nU = self.nU
                nV = self.nV
                xTmp = self.xCtrl
                yTmp = self.yCtrl
                zTmp = self.zCtrl

                fFile = open("surfCtr" + fileMod + ".vtk", "w")
                fFile.write("# vtk DataFile Version 2.0\n")
                fFile.write("The direction test\n")
                fFile.write("ASCII\n")
                fFile.write("DATASET POLYDATA\n\n")

                # Init index array
                iIdx = []
                nPnt = 0
                for i in range(nU):
                    iIdxTmp = []
                    for j in range(nV):
                        nPnt += 1
                        iIdxTmp.append(-1)
                    iIdx.append(iIdxTmp)

                #
                fFile.write("POINTS " + str(nPnt) + " float\n")
                iPnt = -1
                for i in range(nU):
                    for j in range(nV):
                        iPnt += 1
                        iIdx[i][j] = iPnt
                        fFile.write(str(xTmp[i][j]) + " " + str(yTmp[i][j]) + " " + str(zTmp[i][j]) + "\n")

                #
                nPoly = (nU - 1) * (nV - 1)
                fFile.write("\nPOLYGONS " + str(nPoly) + " " + str(nPoly*5) + "\n")

                for i in range(nU - 1):
                    for j in range(nV - 1):
                        fFile.write("4 " + str(iIdx[i][j]) + " " + str(iIdx[i+1][j]) + " " + str(
                            iIdx[i+1][j+1]) + " " + str(iIdx[i][j+1]) + "\n")

    def exportPntsOverSurf(self, u0, fileMod):
        nPnt = len(u0)

        fFile = open("pntBzr" + fileMod + ".vtk", "w")
        fFile.write("# vtk DataFile Version 2.0\n")
        fFile.write("The direction test\n")
        fFile.write("ASCII\n")
        fFile.write("DATASET POLYDATA\n\n")
        fFile.write("POINTS " + str(nPnt) + " float\n")

        for u, v, z in u0:
            xVal, yVal, zVal = self.getPoint3D(u, v)
            fFile.write(str(xVal) + " " + str(yVal) + " " + str(zVal) + "\n")

        fFile.close()

    #
    def fitTanSideU(self, v, sRef, nP=25):
        uLin = numpy.linspace(0, 1, 5 * self.nU)

        xSrf = []
        ySrf = []
        zSrf = []
        for u in uLin:
            xP, yP, zP = sRef.getSliceU(u, nP=nP)
            x0, y0, z0 = self.getPoint3D(u, v)

            if(z0 > zP[nP-1]):
                zT = self.fitTanPnt(x0, z0, xP, zP)
            else:
                zT = zP.copy()

            xSrf.append(xP)
            ySrf.append(yP)
            zSrf.append(zT)

        sBase = surf.surf(5 * self.nU, len(xP))
        sBase.X = xSrf
        sBase.Y = ySrf
        sBase.Z = zSrf

        self.fit2Surf(sBase, nU=self.nU, nV=self.nV)

    def getSliceU(self, u=0, nP=100):
        uLin = [u for _ in range(nP)]
        vLin = numpy.linspace(0, 1, nP)

        xPnt = [0 for _ in range(nP)]
        yPnt = [0 for _ in range(nP)]
        zPnt = [0 for _ in range(nP)]
        for i in range(nP):
            xPnt[i], yPnt[i], zPnt[i] = self.getPoint3D(uLin[i], vLin[i])

        return xPnt, yPnt, zPnt

    def getSliceV(self, v=0, nP=100):
        uLin = numpy.linspace(0, 1, nP)
        vLin = [v for _ in range(nP)]

        xPnt = [0 for _ in range(nP)]
        yPnt = [0 for _ in range(nP)]
        zPnt = [0 for _ in range(nP)]
        for i in range(nP):
            xPnt[i], yPnt[i], zPnt[i] = self.getPoint3D(uLin[i], vLin[i])

        return xPnt, yPnt, zPnt

    def fitTanPnt(self, x0, y0, xC, yC):
        dC = crv.curve().getDerivative(xC, yC)

        xT = 0.5 * min(xC) + 0.5 * max(xC)

        dC = tool.getDerivative(xC, yC)

        xA = xC[0]
        xB = xC[len(xC) - 1] * 0.99

        yA = tool.extrap1(xC, yC, [xA])[0]
        yB = tool.extrap1(xC, yC, [xB])[0]
        dA = tool.extrap1(xC, dC, [xA])[0]
        dB = tool.extrap1(xC, dC, [xB])[0]

        FA = (y0 - yA)/(x0 - xA) - dA
        FB = (y0 - yB)/(x0 - xB) - dB

        while(abs(xA - xB) > 1e-6):
            xT = (xA + xB) / 2
            yT = tool.extrap1(xC, yC, [xT])[0]
            dT = tool.extrap1(xC, dC, [xT])[0]
            FT = (y0 - yT)/(x0 - xT) - dT

            if(numpy.sign(FA) != numpy.sign(FT)):
                xB = xT
                FB = FT
            elif(numpy.sign(FB) != numpy.sign(FT)):
                xA = xT
                FA = FT

        dV = 1.0
        nT = 0
        dH = 1e-6
        while(dV>1e-6 and nT < 100):
            nT += 1

            m0 = (tool.extrap1(xC, yC, [xT])[0] - y0) / (xT - x0)
            F0 = m0 - tool.extrap1(xC, dC, [xT])[0]

            m1 = (tool.extrap1(xC, yC, [xT + dH])[0] - y0) / (xT + dH - x0)
            F1 = m1 - tool.extrap1(xC, dC, [xT+dH])[0]

            J = (F1 - F0)/dH

            xT -= F0/J
            yT  = tool.extrap1(xC, yC, [xT])[0]

            dV = abs(F0)

        yM = [0 for _ in range(len(yC))]
        for i in range(len(xC)):
            if(xC[i] < xT):
                yM[i] = yC[i]
            else:
                yM[i] = tool.extrap1([xT, x0], [yT, y0], [xC[i]])[0]

        return yM

    #
    def getCorners(self):
        xCrnr = [self.xCtrl[0][0], self.xCtrl[self.nU - 1][0], self.xCtrl[self.nU - 1][self.nV - 1], self.xCtrl[0][self.nV - 1]]
        yCrnr = [self.yCtrl[0][0], self.yCtrl[self.nU - 1][0], self.yCtrl[self.nU - 1][self.nV - 1], self.yCtrl[0][self.nV - 1]]
        zCrnr = [self.zCtrl[0][0], self.zCtrl[self.nU - 1][0], self.zCtrl[self.nU - 1][self.nV - 1], self.zCtrl[0][self.nV - 1]]

        return xCrnr, yCrnr, zCrnr

    def getCtrlPt(self, u=None, v=None):
        nU = self.nU
        nV = self.nV
        if(u != None and v == None):
            xCtrl = [self.xCtrl[u][i] for i in range(nV)]
            yCtrl = [self.yCtrl[u][i] for i in range(nV)]
            zCtrl = [self.zCtrl[u][i] for i in range(nV)]
        elif(u == None and v != None):
            xCtrl = [self.xCtrl[i][v] for i in range(nU)]
            yCtrl = [self.yCtrl[i][v] for i in range(nU)]
            zCtrl = [self.zCtrl[i][v] for i in range(nU)]
        else:
            xCtrl = []
            yCtrl = []
            zCtrl = []

        return xCtrl, yCtrl, zCtrl

    #
    def getDX(self):
        xMin = self.xCtrl[0][0]
        xMax = self.xCtrl[0][0]
        for i in range(self.nU):
            for j in range(self.nV):
                xMin = min([xMin, self.xCtrl[i][j]])
                xMax = max([xMax, self.xCtrl[i][j]])

        return xMax - xMin

    def getDY(self):
        yMin = self.yCtrl[0][0]
        yMax = self.yCtrl[0][0]
        for i in range(self.nU):
            for j in range(self.nV):
                yMin = min([yMin, self.yCtrl[i][j]])
                yMax = max([yMax, self.yCtrl[i][j]])

        return yMax - yMin

    def getDZ(self):
        zMin = self.zCtrl[0][0]
        zMax = self.zCtrl[0][0]
        for i in range(self.nU):
            for j in range(self.nV):
                zMin = min([zMin, self.zCtrl[i][j]])
                zMax = max([zMax, self.zCtrl[i][j]])

        return zMax - zMin

    def isInsideXY(self, x0, y0):
        xC, yC = self.getCorners()[:2]

        xC.append(xC[0])
        yC.append(yC[0])
        nX = len(xC)
        nY = len(yC)

        nInt = 0
        for i in range(nX-1):
            dX = xC[i+1] - xC[i]
            dY = yC[i+1] - yC[i]
            dL = numpy.sqrt(dX ** 2 + dY ** 2)
            dX /= dL
            dY /= dL

            if(abs(dX) != 1.0):
                T = numpy.zeros([2, 2])
                x = numpy.zeros( 2)

                T[0][0] = 1
                T[0][1] = -dX
                T[1][0] = 0
                T[1][1] = -dY

                x[0] = xC[i] - x0
                x[1] = yC[i] - y0

                L = numpy.linalg.solve(T, x)
                if(L[0] > 0 and L[1] > 0 and L[1] <= dL):
                    nInt += 1

        if(nInt % 2 != 0):
            bIsIn = True
        else:
            bIsIn = False

        return bIsIn
