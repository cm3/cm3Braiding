# pyRVE
## _Python Code for Geometrical Generator for Braided Composites RVE_

pyRVE is a code written in _Python_ using the _GMSH API_ that generates the Representative Unit Cell (RUC) of braided composites. It allows the generation of the RUC of triaxial braided for _Diamond_ and _Regular_ patterns.

## Requirements
To run, it requires:
- The GMSH Python API, which must be built with OpenCascade support.
  - Choose a local installation directory; `CMAKE_INSTALL_PREFIX=$HOME/local/gmsh`, and `GMSHPY_INSTALL_DIRECTORY=$HOME/local/gmsh` e.g.; 
  - Make that directory part of your `export PYTHONPATH=$HOME/local/gmsh/lib:$PYTHONPATH`.
  - After compiling use `make install`.
- The CM3 app dG3D if the final RVE homogenized solution is needed (https://gitlab.onelab.info/cm3/cm3Libraries).
- Make sure that the latest version of OpenCascade (OCCT) is used. Current used version in occt-V7.8.0.

## Usage
### File Structure
A typical run case must have a file structure, where:
- `brd`: the files `.brd` and `.brep` are located here. The `.brd` is a backup of the `braidClass` instance used in the model saved using `pickle`, the `.brep` is the Boundary Representation file that can be opened with _GMSH_.
- `csv`: the `.csv` file saved here is the initial output of the code. It contains the actually used dimensions and the final cover factor of the braid.
- `data`: It contains `.csv` files with the material properties and the dimensions of the tows. The original model dimensions are read from here.
- `dir`: In the case of running the RVE homogenization, the directions of the tow fibers are stored here. They are saved for post processing.
- `msh`: the mesh file `.msh` obtained after the geometry geneartion is stores here.
- `png`: in the case of automatic post processing, png files are stored here.
- `res`: this folder is used to store the homogenization results. They have to be moved here.
- `stp`: if acitvated, a `.stp` file of the geometry is stored here
- `svg`: the projection of the geometry on the _x-y_ plane is stored here. 
- `vtk`: A copy of the mesh file without the matrix mesh is sotred here as a `.vtk`` file.

[//]: # (According to the run requirements, the following folders are required:)
[//]: # (|Folder|Geometry|dG3D|Debug|)
[//]: # (|------|--------|----|-----|)
[//]: # (|`brd` ||||)
[//]: # (|`csv` ||||)
[//]: # (|`data`||||)
[//]: # (|`dir` ||||)


### How to Run
We will consider the current file structure to run the example in 000_Base. To run the code, it can be called from the command prompt as
```sh
python3 ../../source/mainRVE.py --name <i> --pattern <pattern>
```

In this case, the `--name` refers to the index that will be given to the model, where `<i>` must be changed to an integer and `--pattern` refers to the wanted pattern to be used, where `<pattern>`  must be changed to either `dia` or `reg`.
> Note: `--name cat` can also be used to reproduce the regular pattern benchmark of the paper. In that case, the volume fraction of fiber in the tows is hard coded as the provided value in the reference (i.e. 0.86). For other cases, the volume fraction is evaluated from the tow cross-sections. 

> Note: `mainRVE.py` must be accesible from the directory where the case is being run. This example shows the usage of the current file structure.

### All Command Line Options
The code can be run using further options that serve different purpouses, some serving pre processing needs and other serving run administration. The different command line options are:
- Required: 
    - `--name` : it gives a suffix to the run model. It is usually an integer.
    - `--pattern` : indicates the type of pattern to be used to build the geometry. The two current options are `dia` for diamond and `reg` for regular.
- Optional
    - `-dG3D`: it indicates that the homogenization of the generated RUC is to be perfomed.
    - `-GMSH` : it indicates that GMSH must be open upon competion of the generation of the mesh.
    - `-loadModel` : it will try to load a premade model. It will ignore `--pattern`.
    - `--rndPrm` : it will generate randomized geometrical parameters. It can be used to generate batches of results. It takes an argument that can be `2`, `4` or `6`. Currently, `2` gives a random value for `s_axial` and `theta`, `4` randomizes the same as `2` and adds `h_axial` and `h_bias`, and `6` randomizes the same as `4` and adds `w_axial` and `w_bias`.
- Pre-Processing
    - `-refCF`: it tells the code to generate a grid of values for `s_axial` and `theta` where only the cover factor is obtained. It is meant for posterior graphing purposes.

### Examples
Following the run options, a few examples are indicated
- A basic mesh generation run for the basic data, considering a **regular pattern**, for a model named **2**:
```sh
python3 ../../source/mainRVE.py --name 2 --pattern reg
```

- The generation of the cover factor data and export, considering a **regular pattern**:
```sh
python3 ../../source/mainRVE.py --pattern reg -refCF
```

- A run for the modified basic data, where the **2** parameters are modified _randomly_, considering a **regular pattern**, for a model named **2**:
```sh
python3 ../../source/mainRVE.py --name 2 --pattern reg --rndPrm 2
```

- A run, where model **2** already exists in `brd` folder but not the `.msh` and `.vtk` files:
```sh
python3 ../../source/mainRVE.py --name 2 -loadModel 
```

## Code Structure
The code is implemented into Python files, where `mainRVE.py` runs the whole code. The files are:
- Braid:
    - `braidClass.py` :
    - `bzrPairClass.py` :
- Geometry
    - `bezrClass.py` :
    - `bilnClass.py` :
    - `patchClass.py` :
    - `pntSetClass.py` :
    - `pointClass.py` :
    - `sctnClass.py` :
    - `stripeClass.py` :
    - `surfClass.py` :
    - `surfOffClass.py` :
- Material:
    - `chamis.py` :
- Tools:
    - `dataIO.py` :
    - `postDirection.py` :
    - `tool.py` :
    - `toolData.py` :
- `curveClass.py` :*

### Extra files

```Brep``` files corresponding to to the ```brd``` files can be downloaded from the stable version: [doi: 10.5281/zenodo.10829042](http://dx.doi.org/10.5281/zenodo.10829042)
