import torch
import pickle

# ---------------------------------------------
print("Project: ViBra\nTest Neural Network")

# ---------------------------------------------
# Input Data
# ---------------------------------------------
inputData = {}
inputData["sAxial"] = 10.0
inputData["theta"] = 45.0
inputData["wAxial"] = 4.0
inputData["hAxial"] = 0.5
inputData["wBias"] = 4.0
inputData["hBias"] = 0.5

# ---------------------------------------------
# Model loading
# ---------------------------------------------
#name = "01_Regular"
name = "02_Diamond"

model = torch.load("model_" + name + ".pt")
model.eval()

inpKeys = list(model.normDict["xMin"].keys())
outKeys = list(model.normDict["yMin"].keys())

# Normalize Data
normDict = model.normDict
inputNorm = {}
for key in inpKeys:
    inputNorm[key] = (inputData[key] - normDict["xMin"][key]) / (normDict["xMax"][key] - normDict["xMin"][key])

# Calculate normalized results
data   = torch.FloatTensor([inputNorm[i] for i in inputData.keys()])
output = model(data)

outputNorm = {}
for i in range(len(outKeys)):
    outputNorm[outKeys[i]] = output[i].item()

# Denormalize data
outputData = {}
for key in outKeys:
    outputData[key] = outputNorm[key] * (normDict["yMax"][key] - normDict["yMin"][key]) + normDict["yMin"][key]

#
print("Input:")
for key in inpKeys:
    print("    ", key, ":", inputData[key])

print("\nOutput:")
for key in outKeys:
    print("    ", key, ":", outputData[key])

