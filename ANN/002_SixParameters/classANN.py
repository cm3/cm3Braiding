import torch
import torch.nn as nn
import torch.nn.functional as fcn
from   torch.utils.data import Dataset
import pickle
import numpy

# ---------------------------------------------
def fillZerosStr(sWord, lDes):
    while(len(sWord) < lDes):
        sWord = " " + sWord
    return sWord

# ---------------------------------------------
# Classes definition
# ---------------------------------------------
class ANN(nn.Module):
    def __init__(self, nI, nH1, nH2, nH3, nH4, nO):
        super().__init__()

        self.fc1 = nn.Linear( nI, nH1)  # First hidden layer
        self.fc2 = nn.Linear(nH1, nH2)  # second hidden layer
        self.fc3 = nn.Linear(nH2, nH3)  # third hidden layer
        self.fc4 = nn.Linear(nH3, nH4)  # fourth hidden layer 
        self.fc5 = nn.Linear(nH4, nO) # Output layer
        
        self.inputMin  = []
        self.inputMax  = []
        self.outputMin = []
        self.outputMax = []

        self.optName = ""
        self.lrnRate = 1.0

    def forward(self, x):
        x = fcn.torch.sigmoid(self.fc1(x))   # First hidden layer
        x = fcn.torch.sigmoid(self.fc2(x))   # second hidden layer
        x = fcn.torch.sigmoid(self.fc3(x))   # third hidden layer 
        x = fcn.torch.sigmoid(self.fc4(x))   # fourth hidden layer 
        x = self.fc5(x) # second hidden layer

        return x

    def setScalingData(self, inputMin,  inputMax,  outputMin,  outputMax):
        self.inputMin  = inputMin
        self.inputMax  = inputMax
        self.outputMin = outputMin
        self.outputMax = outputMax

# Define custom dataset
class customDataset(Dataset):
    def __init__(self, fileIn, fileOut, nrmData=None):
        # Overload __init__ member
        # Added the reading and normalization of data
        xDict = pickle.load(open(fileIn,  'rb'))
        yDict = pickle.load(open(fileOut, 'rb'))

        xList = self.getListFromDict(xDict)
        yList = self.getListFromDict(yDict)

        xTnsr = torch.FloatTensor(xList)
        yTnsr = torch.FloatTensor(yList)

        # Check if normalization is well implemented
        if(nrmData==None):
            self.xMin, self.xMax = self.normalizationData(xTnsr)
            self.yMin, self.yMax = self.normalizationData(yTnsr)
        else:
            self.xMin = nrmData["xMin"]
            self.xMax = nrmData["xMax"]
            self.yMin = nrmData["yMin"]
            self.yMax = nrmData["yMax"]

        # 
        self.xMinDict = {}
        self.xMaxDict = {}
        iIdx = 0
        for key in xDict:
            self.xMinDict[key] = self.xMin[iIdx]
            self.xMaxDict[key] = self.xMax[iIdx]
            iIdx += 1

        #
        self.x = self.normalizeData(xTnsr, self.xMin, self.xMax)
        self.y = self.normalizeData(yTnsr, self.yMin, self.yMax)

        #
        self.n_samples = self.x.shape[0]

        # 
        self.yMinDict = {}
        self.yMaxDict = {}
        iIdx = 0
        for key in yDict:
            self.yMinDict[key] = self.yMin[iIdx]
            self.yMaxDict[key] = self.yMax[iIdx]
            iIdx += 1

        #
        self.nI = int(len(xList[0]))
        self.nO = int(len(yList[0]))
        r = numpy.power(float(self.nI) / float(self.nO), 1/3.0)
        self.nH1 = 100 #int(numpy.round(self.nO * numpy.power(r, 2.0)))
        self.nH2 = 100 #int(numpy.round(self.nO * numpy.power(r, 1.0)))
        self.nH3 = 100  # int(numpy.round(self.nO * numpy.power(r, 1.0)))
        self.nH4 = 100

        s1 = str(len(xList[0]))
        s2 = str(len(yList[0]))
        s3 = str(len(xList))

        n1 = len(s1)
        n2 = len(s2)
        n3 = len(s3)

        nChar = max([n1, n2, n3])

        s1 = fillZerosStr(s1, nChar)
        s2 = fillZerosStr(s2, nChar)
        s3 = fillZerosStr(s3, nChar)

        print("Data loaded:\n    Input File: %s\n    Output File: %s\n    Nr. of inputs : %s\n    Nr. of outputs: %s\n    Nr. of samples: %s" % (fileIn, fileOut, s1, s2, s3))

    def __getitem__(self, index):
        # Overload __getitem__ member
        return self.x[index], self.y[index]
 
    def __len__(self):
        # Overload __len__ member
        return self.n_samples

    def normalizationData(self, dataArray):
        # Normalization of the input data to lie in range [0 ,1]
        # Find extrema

        fMin = []
        fMax = []
        for i in range(len(dataArray[0])):
            fVal = []
            for j in range(len(dataArray)):
                fVal.append(float(dataArray[j][i]))
            fMin.append(min(fVal))
            fMax.append(max(fVal))
        return fMin, fMax

    def getNormDict(self):
        normDict = {}
        normDict["xMin"] = self.xMinDict
        normDict["xMax"] = self.xMaxDict
        normDict["yMin"] = self.yMinDict
        normDict["yMax"] = self.yMaxDict

        return normDict


    def getNormData(self):
        normData = {}
        normData["xMin"] = self.xMin
        normData["xMax"] = self.xMax
        normData["yMin"] = self.yMin
        normData["yMax"] = self.yMax

        return normData

    def normalizeData(self, dataArray, fMin, fMax):
        # Normalization of the input data to lie in range [0 ,1]

        for i in range(len(dataArray)):
            for j in range(len(dataArray[i])):
                dataArray[i][j] = (dataArray[i][j] - fMin[j])/(fMax[j] - fMin[j])
        
        return dataArray

    def getListFromDict(self, dictInp):
        listInp = []        
        for key in dictInp.keys():
            listInp.append(dictInp[key])

        nI = len(listInp)
        nJ = len(listInp[0])

        listOut = [[0 for _ in range(nI)] for _ in range(nJ)]
        for i in range(nI):
            for j in range(nJ):
                listOut[j][i] = listInp[i][j]
                
        
        return listOut

