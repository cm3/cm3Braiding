import torch
import torch.nn.functional as fcn
import torch.optim as optim
from   torch.utils.data import DataLoader 
import matplotlib.pyplot as pyplot
import classANN as ANN
import time

import os
from os import listdir
from os.path import isfile

# ---------------------------------------------
print("Project: ViBra\nTrain Neural Network")
tStart = time.time()

# Use the following commands 
# to avoid inconsistent behaviour 
torch.manual_seed(0)
torch.use_deterministic_algorithms(True)

# ---------------------------------------------
# Data loading
# ---------------------------------------------
#name = "01_Regular"
name = "02_Diamond"
trnData = ANN.customDataset(fileIn="inp_" + name + "_001.dat", fileOut=str("out_" + name + "_001.dat"))
nrmData = trnData.getNormData()
tstData = ANN.customDataset(fileIn="inp_" + name + "_002.dat", fileOut=str("out_" + name + "_002.dat"), nrmData=nrmData)

trnSet  = DataLoader(dataset=trnData, batch_size=200, shuffle=True)
tstSet  = DataLoader(dataset=tstData, batch_size=100, shuffle=True)

trainMinOut = trnData.yMinDict
trainMaxOut = trnData.yMaxDict

# ---------------------------------------------
# Parameters initialization
# ---------------------------------------------

nEpochs = 100000   # Number of iterations: Epochs
lr      = 0.00005  # Learning rate
wd      = 0.00      # weight decay
alg     = 2        # 

model = ANN.ANN(nI=trnData.nI, nH1=trnData.nH1, nH2=trnData.nH2, nH3=trnData.nH3, nH4=trnData.nH4, nO=trnData.nO)
#model = torch.load("model_02_Diamond.pt")
#model = torch.load("model_01_Regular.pt")

normDict = trnData.getNormDict()
model.xMin = trnData.xMinDict
model.xMax = trnData.xMaxDict
model.yMin = trnData.yMinDict
model.yMax = trnData.yMaxDict

if(alg == 1):
    model.optName = "Adagrad"
    optimizer1 = optim.Adagrad(model.parameters(), lr=lr, weight_decay=wd) # Initialize optimizer
elif(alg == 2):
    model.optName = "Adam"
    optimizer = optim.Adam    (model.parameters(), lr=lr, weight_decay=wd) # Initialize optimizer
elif(alg == 3):
    model.optName = "SGD"
    optimizer = optim.SGD     (model.parameters(), lr=lr, weight_decay=wd) # Initialize optimizer
elif(alg == 4):
    model.optName = "AdaDelta"
    optimizer = optim.Adadelta(model.parameters(), lr=lr, weight_decay=wd) # Initialize optimizer

# ---------------------------------------------
# Weights and biases minimization
# ---------------------------------------------
print("    Minimize using:", model.optName)
listEpoch   = []
listLossTrn = []
listLossTst = []

nNum0 = len(str(nEpochs))
#
#    listPath = []
for epoch in range(nEpochs):
    iIterTrain = 0
    
    # Update model
    lossValue = 0
    for dataTrain in trnSet:
        iIterTrain += 1
        xTrn, yTrn = dataTrain
        model.zero_grad()                 # Clear gradient from previous epoch
        output = model(xTrn)              # Propagate forward
        loss = fcn.mse_loss(output, yTrn) # Cost function: Loss
        loss.backward()                   # Propagate backwards
        optimizer.step()                  # Modify weights
        lossTrain = loss.item()           # Extract value of loss function

    # Calculate test error, for early stop
    for dataTest in tstSet:
        xTest, yTest = dataTest
        outputTest = model(xTest)
        lossTest = fcn.mse_loss(outputTest, yTest).item()

    #
    listEpoch.append(epoch)
    listLossTrn.append(lossTrain)
    listLossTst.append(lossTest)

    #
    if((epoch + 1) % 500 == 0 or epoch == 0):
        sEpoch = ANN.fillZerosStr(str(epoch + 1), nNum0)
        print("    Epoch: %s / %i - Loss Train: %0.5e - Loss Test: %0.5e" % (sEpoch, nEpochs, lossTrain, lossTest))

#
model.normDict = normDict
torch.save(model, "model_" + name + ".pt")

tFinish = time.time()
execution_time = tFinish - tStart
print("Time:", execution_time, "s")
#
pyplot.figure()
pyplot.semilogy(listEpoch, listLossTrn, 'b', label='Training data')
pyplot.semilogy(listEpoch, listLossTst, 'r', label='Test data')
pyplot.legend()
pyplot.title(f"Execution time: {execution_time}")

pyplot.savefig("loss_" + name + ".png")

pyplot.close()
#pyplot.show()



