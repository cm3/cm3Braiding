import torch
import classANN as ANN
import matplotlib.pyplot as plt

# ---------------------------------------------
print("Project: ViBra\nTest Neural Network")

# ---------------------------------------------
# Input Data
# ---------------------------------------------

'''
Procedure:
1. load testDataInputNorm(TstInp) and testDataOutputNorm(outTrue)
3. load model
3. model(TstInp) -> outANN
4. deNorm outANN and outTrue
5. delta = outTrue - outANN
'''

# 1 and 2

#name = "01_Regular"
name = "02_Diamond"

trnData = ANN.customDataset(fileIn="inp_" + name + "_001.dat", fileOut=str("out_" + name + "_001.dat"))
nrmData = trnData.getNormData()
tstData = ANN.customDataset(fileIn="inp_" + name + "_002.dat", fileOut=str("out_" + name + "_002.dat"), nrmData=nrmData)

#keys = list(trnData.yMinDict.keys())  # Nome dei componenti
#MinOut = trnData.yMinDict
#MaxOut = trnData.yMaxDict
#MinOut = list(MinOut.values())
#MaxOut = list(MaxOut.values())

model = torch.load("model_" + name + ".pt")
model.eval()

inpKeys = list(model.normDict["xMin"].keys())
outKeys = list(model.normDict["yMin"].keys())

outANN = []
outTrue = []
for dataTest in tstData:
    TstInp, singleOutTrue = dataTest
    singleOutANN = model(TstInp)
    
    outANN.append(singleOutANN.tolist())
    outTrue.append(singleOutTrue.tolist())

outANN = torch.tensor(outANN)
outTrue = torch.tensor(outTrue)
print(len(outANN))

normDict = model.normDict

for i in range(len(outANN)):
    for j in range(len(outANN[i])):
        key = outKeys[j]
    
        outANN[i][j] = outANN[i][j] * (normDict["yMax"][key] - normDict["yMin"][key]) + normDict["yMin"][key]
        outTrue[i][j] = outTrue[i][j] * (normDict["yMax"][key] - normDict["yMin"][key]) + normDict["yMin"][key]

print(outANN[1,1])
print(outTrue[1,1])
delta = torch.zeros_like(outTrue)
for i in range(len(outANN)):
    for j in range(len(outANN[i])):
        delta[i][j] = 100*(outTrue[i][j] - outANN[i][j])/outTrue[i][j]
print(len(delta[0]))
maxDelta = []
for j in range(len(delta[0])):
    maxDelta.append(max(abs(delta[:, j])))

print(maxDelta)

plt.bar(outKeys, maxDelta)  # Plot a bar chart
plt.xlabel('Component')  # Label x-axis
plt.ylabel('Max Delta %')  # Label y-axis
plt.title('Maximum Delta Percentage per Component')  # Set title
plt.xticks(rotation=45, ha='right')  # Rotate x-axis labels for better readability
plt.tight_layout()  # Adjust layout to prevent clipping of labels
plt.savefig("maxDelta_" + name + "_six.png")
plt.show()  # Show the plot
