#!/bin/bash
#submission script for Hercules2
#SBATCH --job-name=RUC
#SBATCH --array=1-1000
#SBATCH --time=3:00:00 # hh:mm:ss
#
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=35000 # 5GB
#SBATCH --partition=batch
#
#SBATCH --mail-user=ji.rothkegel@uliege.be
#SBATCH --mail-type=ALL

function load_module()
{
    module purge
    module load GCC/11.2.0
    module load CMake/3.21.1-GCCcore-11.2.0
    module load SWIG/4.0.2-GCCcore-11.2.0
    module load Python/3.9.6-GCCcore-11.2.0
    module load OpenMPI/4.1.2-GCC-11.2.0
    module load OpenBLAS/0.3.18-GCC-11.2.0
    module load freetype/2.11.0-GCCcore-11.2.0
    module load fontconfig/2.13.94-GCCcore-11.2.0
}
load_module

export LD_LIBRARY_PATH=$HOME/local/occt/lib/:$LD_LIBRARY_PATH
export CASROOT=$HOME/local/occt

export PETSC_DIR=$HOME/dev/petsc
export PETSC_ARCH=arch-linux-cxx-opt
export SLEPC_DIR=$HOME/dev/slepc
export SLEPC_ARCH=arch-linux-cxx-opt

export PYTHONPATH=/home/ulg/cmmm/jrothkeg/local/gmsh/lib64:$PYTHONPATH

echo "Task ID: $SLURM_ARRAY_TASK_ID"

N_OFFSET=0
ID_JOB=$(($SLURM_ARRAY_TASK_ID + $N_OFFSET))

srun python3 ../../../../source/mainRVE.py --name $ID_JOB -loadModel -removeMSH -dG3D
