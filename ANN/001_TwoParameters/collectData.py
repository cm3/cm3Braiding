import re
import os
import csv
import pickle

keysInp = ["sAxial", "theta"]
keysOut = ["Comp.0000", "Comp.0011", "Comp.0022", "Comp.1111", "Comp.1122", "Comp.2222", "Comp.1212", "Comp.2020", "Comp.0101"]

ptrns = ["01_Regular", "02_Diamond"]
cases = ["001", "002"]

for ptrn in ptrns:
    for case in cases:
        # Get index of valid data
        filePath = ptrn + "/" + case + "/res/"
        files = os.listdir(filePath)

        index = []
        for file in files:
            model = file.split('_')[0]
            index.append(int(re.findall(r'\d+', model)[0]))

        # Files list
        fileCSV = ["model"+str(i)+".csv" for i in index]
        fileRES = ["model" + str(i) + "_E_0_GP_0_tangent.csv" for i in index]

        # Read input files
        dataInp = {key:[] for key in keysInp}
        for file in fileCSV:
            with open(ptrn + "/" + case + "/csv/" + file, 'r') as dataCSV:
                for line in csv.DictReader(dataCSV, delimiter=';'):
                    for key in keysInp:
                        dataInp[key].append(float(line[key]))

        # Read output files
        dataOut = {key:[] for key in keysOut}
        for file in fileRES:
            #print(file)
            with open(ptrn + "/" + case + "/res/" + file, 'r') as dataRES:
                for line in csv.DictReader(dataRES, delimiter=';'):
                    for key in keysOut:
                        dataOut[key].append(float(line[key]))

        # Write files
        pickle.dump(dataInp, open("inp_" + ptrn + "_" + case + ".dat", 'wb'))
        pickle.dump(dataOut, open("out_" + ptrn + "_" + case + ".dat", 'wb'))

