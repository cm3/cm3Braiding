# Artificial Neural Network (ANN)
## Data Structure
For each pattern, regular and diamond, have each two folders, 001 and 002, where each is the train data and test data, respectively.

## Data Collection
The data collection is performed using the `collectData.py` script. The script has hardcoded the patterns and subdirectories, as
```python
ptrns = ["01_Regular", "02_Diamond"]
cases = ["001", "002"]
```

The input data for the ANN is located in each respective `csv` subdirectory in each case, the output data for the ANN is located in each respective `res` subdirectory.

The input and output data to be readed and stored is defined in the `collectData.py` as
```python
keysInp = ["sAxial", "theta", ...
keysOut = ["Comp.0000", ...
```

The script outputs the files needed for the training and testing of the ANN. The files are named as:
### Regular
<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
  </tr>
  <tr>
    <td>inp_01_Regular_001.dat</td>
    <td>Train</td>
  </tr>
  <tr>
    <td>out_01_Regular_001.dat</td>
    <td>Train</td>
  </tr>
  <tr>
    <td>inp_01_Regular_002.dat</td>
    <td>Test</td>
  </tr>
  <tr>
    <td>out_01_Regular_002.dat</td>
    <td>Test</td>
  </tr>
  </table>

### Diamond
  <table>
  <tr>
    <td>inp_02_Diamond_001.dat</td>
    <td>Train</td>
  </tr>
  <tr>
    <td>out_02_Diamond_001.dat</td>
    <td>Train</td>
  </tr>
  <tr>
    <td>inp_02_Diamond_002.dat</td>
    <td>Test</td>
  </tr>
  <tr>
    <td>out_02_Diamond_002.dat</td>
    <td>Test</td>
  </tr>
</table>

## ANN Training
The implementation of the `ANN` and `customDataset` used by Pytorch is done in file `ANN.py`. The training is handled by the script `trainANN.py`. The data to be loaded is hardcoded. As an example, we have
```python
trnData = ANN.customDataset(fileIn="inp_" + name + "_001.dat", fileOut=str("out_" + name + "_001.dat"))
nrmData = trnData.getNormData()
tstData = ANN.customDataset(fileIn="inp_" + name + "_002.dat", fileOut=str("out_" + name + "_002.dat"), nrmData=nrmData)
```

The test dataset `tstData` is normalized using the normalization data from the train dataset `trnData`.

The current implementation shows the following convergence, for the regular and diamond patterns, respectivelly:
<table>
    <tr>
        <td><img src="loss_lr_1e-05_alg_Adam_01_Regular.png" alt="1" alignment="center" width = 400px height = 350px><figcaption>Regular</figcaption></td>
        <td><img src="loss_lr_1e-05_alg_Adam_02_Diamond.png" alt="2" alignment="center" width = 400px height = 350px><figcaption>Diamond</figcaption></td>
     </tr> 
</table>

## Note: cm3Libraries Code Modification
The dG3D code used for the condensation outputs files with a fixed name. A modification has been made to handle the model index as part of the output name. In file `cm3Libraries/NonLinearSolver/nlsolver/homogenizedData.cpp`, in function `void allFirstOrderMechanicsFiles::openFiles` the following line has been modified:

### Original
```cpp
filename = solver->getFileSavingPrefix()+"tangent.csv";
```
### Modified
```cpp
filename = "res/model" + int2str(solver->getTag()) + "_" + solver->getFileSavingPrefix()+"tangent.csv";
```