# Generation of Results

## Files Structure
Four model folders are provided here:
- `01_Regular/001/`
- `01_Regular/002/`
- `02_Diamond/001/`
- `02_Diamond/002/`

En each model, the following subfolders are provided:
- `csv`: model dimensions.
- `brd`: braid model and volume model.

## Generation of Geometric and Mesh Files: `brd`
The geometric files in this data set, located in the `brd/` folder, `.brd` for the braid model and `.brep` for the volume model, are generated starting from the files located in the `csv` subfolder.

Once the model is loaded, the mesh file `.msh` is generated.

> If the `.brep` file does not exist, the command to generate the mesh file will load the `.brd` file and generate the `.brep` file.

For example, for a model with index `2`, in `01_Regular/001`, the following command can be used
```sh
cd 01_Regular/001
python3 ../../../../source/mainRVE.py --name 2 -loadModel
cd ../..
```

## Generation of `.brep` Files
in the case a braid model `.brd` exists but not the volume model `.brep`, a volume model can be generated. 

For example, for a model with index `2`, in `01_Regular/001`, the following command can be used

```sh
cd 01_Regular/001
python3 ../../../../source/mainRVE.py --name 2 --pattern reg
cd ../..
```

## Generation of Figures

The figures shown below can be generated using the following command
```sh
python3 postProcGenFigValidity.py
```
<table>
    <tr>
        <td><img src="2p_Diamond001.pdf" alt="1" alignment="center" width = 400px height = 350px></td>
        <td><img src="2p_Diamond002.pdf" alt="2" alignment="center" width = 400px height = 350px></td>
     </tr> 
     <tr>
        <td><img src="2p_Regular001.pdf" alt="3" alignment="center" width = 400px height = 350px></td>
        <td><img src="2p_Regular002.pdf" alt="4" alignment="center" width = 400px height = 350px></td>
    </tr>
</table>

The file `postProcGenFigValidity.py` is a script that reads the following folders:
- `01_Regular/001`
- `01_Regular/002`
- `02_Diamond/001`
- `02_Diamond/002`

In each folder, it first reads the content of folder `csv` for files `model<idx>.csv` to get a first list of models and then compares with the contents of folder `brd` for files `model<idx>.brd`. If the model is found in both folders, the model is considered as **valid**, where if it is not it is considered **invalid**.

The files that allow for display of the cover factor are:
- `xf.out`
- `yf.out`
- `cf.out`

These files are stored using the `pickle` package in Python and can be generated in each folder. For example, for the model in `01_Regular/001` as:
```sh
cd 01_Regular/001
python3 ../../../../source/mainRVE.py -refCF --pattern reg
cd ../..
```
