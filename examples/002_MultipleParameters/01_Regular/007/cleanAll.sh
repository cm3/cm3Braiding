#! /bin/bash

# Clean tree
rm -f brd/*
rm -f csv/*
rm -f dir/*
rm -f png/*
rm -f vtk/*
rm -f msh/*
rm -f res/*
rm -f stp/*
rm -f svg/*
rm -f *.csv
rm -f *.vtk
rm -f slurm*
rm -f *.out
