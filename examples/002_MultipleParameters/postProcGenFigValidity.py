import csv
import os
import pickle
import matplotlib.pyplot as pyplot

#
def getZeros(x, y):
    nX = len(x)
    nY = len(y)

    x0 = []
    if(nX == nY and nX >= 2):
        for i in range(nX - 1):
            xA = x[i]
            xB = x[i + 1]
            yA = y[i]
            yB = y[i + 1]

            if(yA == 0 and yB != 0):
                x0.append(xA)
            elif(yA != 0 and yB == 0):
                x0.append(xB)
            elif((yA < 0 and yB > 0) or (yA > 0 and yB < 0)):
                xC = xA - yA * (xB - xA) / (yB - yA)
                x0.append(xC)

    return x0

#
# folders = ["01_Regular/001", "01_Regular/002", "02_Diamond/001", "02_Diamond/002"]

folders = []
for i in range(9):
    folders.append("01_Regular/%03i" % (i+1))
for i in range(9):
    folders.append("02_Diamond/%03i" % (i+1))

cfFiles = ["xf.out", "yf.out", "cf.out"]

for folder in folders:
    figName = (folder.split('_')[1]).split('/')

    allFiles = os.listdir(folder+"/csv")
    files = []
    for file in allFiles:
        if(not "vf" in file and not "chamis" in file):
            files.append(file)

    #
    xClr = pickle.load(open(folder + "/" + cfFiles[0], 'rb'))
    yClr = pickle.load(open(folder + "/" + cfFiles[1], 'rb'))
    zClr = pickle.load(open(folder + "/" + cfFiles[2], 'rb'))

    xMin = min(min(xClr))
    xMax = max(max(xClr))
    yMin = min(min(yClr))
    yMax = max(max(yClr))

    #
    x0 = []
    y0 = []
    for i in range(len(xClr)):
        xL = []
        cL = []
        for j in range(len(xClr[i])):
            xL.append(xClr[j][i])
            cL.append(zClr[j][i] - 1.0 + 1e-10 )
        x0.append(getZeros(xL, cL)[0])
        y0.append(yClr[0][i])

    #
    x = []    
    y = []    
    b = []
    c = []
    index = []
    for file in files:
        model = file.split('.')[0]
        with open(folder + "/csv/" + model + '.csv', 'r') as data:
            for line in csv.DictReader(data, delimiter=';'):
                x.append(float(line['sAxial']))
                y.append(float(line['theta']))
                c.append(float(line['cFct']))

        idx = model
        while(not idx[0].isdigit()):
            idx = idx[1:]
        index.append(idx)

        #if(os.path.isfile(folder + "/brd/" + model + '.brep')):
        if(os.path.isfile(folder + "/brd/" + model + '.brd')):
            b.append(True)
        else:
            b.append(False)

    #
    pyplot.figure(figsize=(7, 6))
    pyplot.rcParams['font.size'] = 16.
    
    pyplot.pcolor(xClr, yClr, zClr, cmap='summer')
    pyplot.colorbar()

    pyplot.plot(x0, y0)

    nYes = 0
    nNot = 0
    for i in range(len(x)):
        if(not b[i]):
            if(nNot == 0):
                pyplot.plot(x[i], y[i], 'r+', label='Invalid Result')
                nNot += 1
            else:
                pyplot.plot(x[i], y[i], 'r+')
                if(c[i] < 1.0):
                    pyplot.text(x[i], y[i], index[i])

    for i in range(len(x)):
        if(b[i]):
            if(nYes == 0):
                pyplot.plot(x[i], y[i], 'b.', label='Valid Result')
                nYes += 1
            else:
                pyplot.plot(x[i], y[i], 'b.')

    ax = pyplot.gca()
    ax.set_aspect('equal')

    pyplot.xlim([xMin, xMax])
    pyplot.ylim([yMin, yMax])
    
    pyplot.legend(loc='upper center', bbox_to_anchor=(0.5, 1.15), ncol=2)

    pyplot.xlabel(r'$s_{axial}$ [mm]')
    pyplot.ylabel(r'$\theta$ [deg]')

    pyplot.savefig('6p_'+figName[0] + figName[1] + '.pdf',format="pdf", bbox_inches="tight")
